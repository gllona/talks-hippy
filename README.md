# Talks Hippy: Telegram and Matrix chatbot mini-framework

You want a chatbot but need a framework. You need a framework because you need more than a client library, more than a HTTP request/response solution, to express conversational interactions with a complete system.

Then you arrive to the conclusion is that you need more than a chatbot maker, you need something more specific than what the many cloud-based chatbot services provide with extreme easyness. You also want to host your system.

You are familiar with Java, and know what is a framework. Then you can use Talks Hippy for your own Telegram and Matrix chatbots.

# Status

Working version.

Can work paired with a Matrix server with the [talks-hippy-bridge](https://github.com/nightgreenwolf/talks-hippy-bridge) `maubot` bot.

Forked from https://gitlab.com/gllona/talks and enhanced.

# Advantages

* Supports both Telegram bots and Matrix.org bots using `maubot`
* More than an HTTP-based library. Talks Hippy is a framework that provides routing, context management and security.
* You can develop your own bots with all the power of Java. You are not restricted to the limits of cloud-based bot generators.
* You can develop complete applications without the hassle of front-end development. Telegram and Matrix clients manage it for you.

# Easy try

Clone [this example project](https://gitlab.com/gllona/talks-hippy-example) and give Talks Hippy a try. It comes with documentation and you will need less than 15 minutes for running it from scratch, honestly, if you already have the Java JDK and a MySql Server installed in your machine.  

Behind the scenes, all Telegram traffic is routed using Stas Parshin [Java Telegram Bot API](https://github.com/pengrad/java-telegram-bot-api).

The Matrix.org traffic is routed using the custom [maubot](https://docs.mau.fi/maubot/dev/getting-started.html) bridge bot
located at [talks-hippy-bridge](https://github.com/nightgreenwolf/talks-hippy-bridge).

# What Talks Hippy provides

As every framework, Talks Hippy provides specific functions that allows you to focus on the domain and not on the quirks of the user-to-app-to-user data transmission:

## Routing

In almost every REST application framework, you have a way to define what should be executed according to an HTTP phrase (HTTP method, URL, arguments). Some frameworks define the routes in specific files, while others rely on code annotations.

The main difference between REST applications and conversational ones is that with chatbots you have less information for routing. In most of the cases, you get no more than a single word. Consider the following scenario in which the *handler* represents a Java method:

```
#       USER INPUT      BOT HANDLER             BOT RESPONSE
--------------------------------------------------------------------------------------------
1.      /start          start()                 Do you want pizza?
2.      Yes             poorPizzaRestaurant()   We offer Napolitana only, do you want one?
3.      Yes             waitForYourPizza()      Great. Take a seat and wait a few minutes.     
``` 

The bot should recognize that a user input `Yes` means something different according to the interactional moment, so the correct handler is executed. This means that the architecture of the REST applications routing:

```
* HTTP phrase --> routing table --> Java handler
* Java handler execution --> HTTP response
``` 

is not applicable to chatbots, because there is no 1:1 relationship between user inputs and handlers. It's instead an 1:N relationship.

In order to provide a proper routing, Talks Hippy rely on the *context* (see below) for implementing the following architecture:

```
* [pre-condition] Context from interaction N-1 is stored at the backend
* (User input for interaction N) + (context) --> Java handler
* Java handler execution --> (bot response for interaction N) + (mutated context)
* [post-condition] Context from interaction N is stored at the backend
```  

## Context Management

The *context* is the *state* associated to a Telegram or Matrix.org bot user that is stored at the backend.

In Talks Hippy, the *context* is composed by two elements:

* The current *routing table*
* The *session variables*

The *context* is associated with a specific Telegram or Matrix.org user. Each user is identified to the bot with its `chat_id`.

A user can have a long-lived session with the bot (in some cases, years-long). Talks Hippy doesn't store the session history, only the information for handling inputs *at an specific moment - the current moment* of the user session.

The context is persistent. It's stored in a MySql table named `talk_contexts`. Each user *context* is represented by two rows in the table. 

### The Routing Table

One of the DB table's rows, with `type == 'handlers'`, stores the routing table. The routing table is dynamic -different than a REST application- and depends on the user session moment. The `content` column will contain something similar as:

```json
[
  {
    "triggers": [
      {
        "mediaType": "TEXT",
        "text": "Yes",
        "isRegex": false
      }
    ],
    "className": "WelcomeTalk",
    "methodName": "waitForYourPizza"
  }
]
```

This entry means that when a user input with the text `Yes` arrives, it will be routed to the `waitForYourPizza` method in the `WelcomeTalk` class.

The JSON above represents an array of *handlers*. There can be more than one handler for a specific moment of the user session. Each handler is associated with a class and method, and can be executed if one of its *triggers* matches with the user input. There can be more than one trigger for each handler. Also, triggers can be defined using regular expressions. And, of course, Talks Hippy supports the rich interaction provided by Telegram, meaning that you can define handlers that answer to *Photos, Audios, Videos,* etc.

For Matrix.org traffic, the only type of traffic supported are text messages. 

### The Session Variables

As with every system, backend state can be stored in a RDBMS or cache. But in conversational applications there are lots of pieces of information that should be stored. This is because chatbots lack client-side state.

In a mobile app, the screen is made by assembling components that store the pieces of the user input. After all pieces have been fulfilled by the user, an HTTP request is made to the backend. But a Telegram or Matrix.org chatbot can't handle them, because its memory is short-lived: a request, a response.

Suppose that you want to acquire some parts of the user input and then execute a domain-specific handler:

```
#       USER INPUT      BOT HANDLER                     BOT RESPONSE
--------------------------------------------------------------------------------------------
4.      Napolitana      offerNapolitanaToppings()       Do you want toppings?
                                                        * Anchovies
                                                        * Onions
                                                        * No
5.      Onions          offerNapolitanaToppings()       An additional topping?
                                                        * Anchovies
                                                        * No
6.      Anchovies       offerNapolitanaToppings()       An additional topping?
                                                        * No
7.      No              waitForYourPizza()              Great. Take a seat and wait a few minutes.     
``` 

In this scenario, you want to gather the pizza toppings before calling the `waitForYourPizza` method. You could implement a DB table for that, but imagine doing that for every set of multi-step domain actions. You end with a lot of intermediate tables.

The *session variables* offered by Talks Hippy allows you to collect that information before calling the domain method. You can store *strings*, *integer numbers* and *floating point numbers*. The *session variables* are modeled after a (name, value) map. 

In the `talk_contexts` table, this is represented by a row with `type == 'vars'`, which has a `content` column with a value as:

```json
{
  "PIZZA": "Napolitana",
  "TOPPINGS": "Onions,Anchovies"
}
```

Then, when the method `waitForYourPizza` is executed, it can recover the values of the *variables* `PIZZA` and `TOPPINGS`, and then proceed to the kitchen.

### User Privacy

The `TalkContext` object available in every `Talk` subclass provides a method `forgetPersonalData`
to remove the user's personal data stored in the `talk_contexts` table.

## Limits Management

Telegram imposes some limits to bots:

* 30 messages per second sent to all chats
* 1 second between consecutive messages sent to the same chat
* 4096 characters per text message

Talks Hippy manages those limits for you.

Note, however, that the text message length limit is not enforced for HTML responses, only for plain text messages. Sending a very long HTML message could fail silently.

Note: the same limits are applied to interactions with Matrix bots.

## Security

The *dynamic routing table* implemented by Talks Hippy offer an entry-level security layer. Every domain handler -a Java method- can be executed only when it's in the user's *routing table* and the user input matches an associated *trigger*. In no other case a handler will be executed by Talks Hippy.

When a user starts a chatbot session, there are no handlers available for the user. But as every conversational segment -represented as a `Talk` subclass- has an optional *default handler*, you can define, let's say, a `WelcomeTalk` class which inherits from `Talk` and provides a default handler, a method `doStart()`, that will answer when the trigger `/start` matches the user input.

In this way, a *handler* will be executed only if:

* It has been defined as the method to be called after a specific user input matches the handler's *trigger*
* That definition has been set:
  * In the *default handler* definition of a `Talk` subclass, or
  * In the user session *routing table*, as a product of an explicit, code-level definition done when a previously executed *handler* returned its *response* to the user 
 
This differs from REST applications, where the backend endpoints are openly available at every moment of the user session, ready to be exploited using HTTP client automation.

## Easiness

This is the code for a `Talk` subclass that implements a `hello world` bot:

```java
public class WelcomeTalk extends Talk {

    public WelcomeTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("WelcomeTalk::doStart", InputTrigger.ofString("/start"))
        );
    }

    public TalkResponse doStart(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "hello world!"
        );
    }
}
```

If you want to implement a simple flow, you can attach *triggers* to the `TalkResponse` that is returned by `doStart()`, and implement the required *handlers*. Each *handler* comes with a very specific signature, they receive a `TalkRequest` and return a `TalkResponse`:

```java
    public TalkResponse doStart(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "hello world!\n" 
                + "To continue tap /here"
        ).withString("WelcomeTalk::continueAfterStart", "/here");
    }
    
    public TalkResponse continueAfterStart(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "you made a two step interaction!"
        );
    }
```

The Talks Hippy approach means that every *handler* has to be prepared to process only the specific user input type that is defined by the user session *triggers*. For example, if you define:

```
#       EXECUTED METHOD                 RESPONSE.HANDLER.TRIGGER    RESPONSE.HANDLER.METHOD      
--------------------------------------------------------------------------------------------
8.      offerNapolitanaToppings()       * regex: [A-Za-z]+          offerNapolitanaToppings() 
                                        * No                        waitForYourPizza()
```

then the `waitForYourPizza()` method should be prepared to receive a text input only, and in this particular case that text will be a `No`. No need to prepare that *handler* to cope with inputs of type *Photo*, *Audio*, *Video*, etc. Note, however, that you could define other *triggers* that will make the `waitForYourPizza()` to be executed as the product of other user input types.  

## Multiple Responses

If you want to send multiple responses to an incoming interaction, use the following syntax:

```java
public TalkResponse cookPizza(TalkRequest request) {
    String photo = request.getChatId() % 2 == 0 ? 
        "cat.jpg" : "dog.jpg";
    request.getInputAdapter().sendResponse(
        TalkResponse.ofPhoto(
            className(this), request.getChatId(),
            Utils.fileFrom(photo)
        )
    );
    return TalkResponse.ofText(
        className(this), request.getChatId(),
        "Your pizza is cooking. Enjoy our patron's picture."
    );
}
```

**Note**: messages sent using `request.getInputAdapter().sendResponse(....)` can not set *handlers* to be used in next interactions. They, however, can be used to set how messages are shown by Telegram. A message sent with `.withPreserveHandlers()` will make Telegram don't change existing displayed buttons or keyboard in a chat.

A better way to send multiple responses to the same chat is by using the `Talkresponse.next()` method. Multiple calls to
`next()` can be chained:

```java
public TalkResponse start(TalkRequest request) {
    return TalkResponse.ofHtml(
        className(this), request.getChatId(),
        "<code>" + status.getAsterisk() + "</code>"
    ).next(
        TalkResponse.ofText(
            className(this), request.getChatId(),
            status.getMessage()
        ).withRegex("hangman.HangmanTalk::guess", GUESS_REGEX)
    );
}
```

## Low priority messages

Sometimes you want to send broadcast notifications to all your bot users. A massive amount of notifications, in the form of Telegram or Matrix.org messages, can block the normal flow of interactions with users currently using your application. This is because Telegram imposes a limit of 30 messages per second sent to all chats. If you want to send 1800 broadcast messages, you are delaying future users' interactions for 1 complete minute, during which the users will not receive feedback from your application.

The solution is to send the broadcast notifications as *low priority* messages, using the following syntax:

```java
public TalkResponse notifyFreePizza(TalkRequest request) {
    for (int i = 0; i < 1000; i++) {
        request.getInputAdapter().sendResponse(
            TalkResponse.ofText(
                className(this), getChatIdFromYourUsersDatabase(i),
                "Hey come for a free pizza tonight!"
            ).withPreserveHandlers()
             .withLowPriority()
        );
    }
    
    return TalkResponse.ofText(
        className(this), request.getChatId(),
        "Notifications sent."
    );
}
```

Submitted low priority messages are scheduled to be sent at 1 per second speed.

## Flying messages

Flying messages or "disappearing messages" if a feature for which you can make messages sent or received by the bot to disappear after
some specific time. The Telegram bot API allows bots to delete messages in the first 48 hours after they are generated
(by the bot or by bot users).

In order to make a received message "to fly", you need to use this syntax:

```java
public TalkResponse deleteReceivedMessage(TalkRequest request) {
    request.fly(); // will delete the received message after a predefined duration
    // or
    request.fly(5, TimeUnit.MINUTES); // make the message fly after 5 minutes
    // ...    
}
```

In order to make a message sent "to fly":

```java
public TalkResponse sendMessageWithAutoFly(TalkRequest request) {
    // create a TalkResponse that will fly after a predefined duration
    TalkResponse flyingMessage1 = TalkResponse.ofText(
        className(this), request.getChatId(),
        "Flying message 1"
    ).withFlying();
    
    // create a TalkResponse that will fly after 1 hour
    TalkResponse flyingMessage2 = TalkResponse.ofText(
        className(this), request.getChatId(),
        "Flying message 2"
    ).withFlying(1, TimeUnit.HOURS);
    
    return flyingMessage1; // or flyingMessage2
}
```

The effective duration will be capped to the value of the environment variable `FLYING_MESSAGES_MAX_DURATION_IN_MINUTES`. To be sure that Talks Hippy has enough time
to process the deletion queue, communicate with the Telegram servers, cope with intermittent network failures, and have some space for
bot re-starts, it is recommended to use a maximum duration of 47 hours (or less).

If a duration is not specified, the duration will be taken from the environment variable `FLYING_MESSAGES_DURATION_IN_MINUTES`.
If this value is `-1` then the flying messages feature will be ignored and messages won't fly.

There is an additional environment variable you must set, `FLYING_MESSAGES_STEALTH_FACTOR`. Normally its value should be `1`.
But for testing purposes, you may want to check if the messages scheduled for hours in advance fly. If you set this value to `60`, then
a message scheduled to fly in 1 minute will fly in 1 second, and a message scheduled to fly in 1 hour
will fly in 1 minute. There is no upper limit for this value.

**Note**: Flying messages are available only if `PERSISTENT_MESSAGE_SENDER_VERSION` is `3`.

## Asynchronous mode

Sometimes generating the contents to be displayed is an expensive operation, like image generation or
text generation using AI models. Those operations avoid sending a fast answer to Telegram servers or the `maubot` bridge bot. It is better to
delay the content generation to the message sender, which is asynchronous regarding the main event loop.

You can specify a `textSupplier` and a `fileSupplier` in each `TalkResponse`. Pass lambda functions to them and
at the same time pass `null` in its factory methods for arguments of type `String` (for the text or HTML message) and
`File`.

If a supplier fails its execution, you can resort to the `exceptionMapper`, which provides a fallback response to
each Exception thrown at asynchronous response content building.

You can also specify a chain of responses to be sent. Use the `next` method to build the response chain.

The following code excerpt shows how to apply this feature:

```java
public TalkResponse showFunImage(TalkRequest request) {
    TalkResponse funDescription = TalkResponse.ofText(
        className(this), request.getChatId(),
        "That was fun!"
    );
    TalkResponse funImage = TalkResponse.ofPhoto(
        className(this), request.getChatId(),
        null   // pass null here instead of a File object
    ).withFileSupplier(() -> funProvider())
     .withExceptionMapper(e -> cantCreateImage(request, e))
     .withButton("fun.FunTalk::continue", "Continue")
     .next(funDescription);
    return funImage;
}

private File funProvider() {
    File funImage = plotter.plot();   // expensive operation that can throw Exception
    return funImage;
}

private TalkResponse cantCreateImage(TalkRequest request, Exception e) {
    return TalkResponse.ofText(
        className(this), request.getChatId(),
        "System error. Unable to paint image."
    ).withButton("fun.FunTalk::menu", "🏕 Back");
}
```

**Note 1**: Asynchronous responses and response chaining are available only if `PERSISTENT_MESSAGE_SENDER_VERSION` is `3`.

**Note 2**: Asynchronous responses are not persisted in the database.

# HOW_TO

You can follow these steps or base your project on one of these projects:

* [animal-welfare-telegram-bot](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/build.gradle)
* [talks-example](https://gitlab.com/gllona/talks-example)

## Setup

### 1. Gradle

Create a Gradle project in your IDE.

Copy the `build.gradle` from [here](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/build.gradle). Take note that this a module Gradle configuration file. You could also need a `settings.gradle` similar to [this](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/settings.gradle) if you opt for a multi-module project. 

Edit `build.gradle`:

```groovy
application {
    mainClassName = 'MAIN_CLASS_FULL_QUALIFIED_NAME'
    executableDir = '.'
}
```

### 2. Main class

Create your main class. Copy it from [here](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/src/main/java/org/animal/App.java).

Remove the reference to `AppConfig` inside the constructor.

### 3. Model class

You can use the *model class* as an interceptor for Telegram and Matrix.org requests and responses. Copy it from [here](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/src/main/java/org/animal/Model.java).

### 4. Talks enum

`Talk` subclasses are registered in the `Talks` enum. Copy it from [here](https://gitlab.com/gllona/animal-welfare-telegram-bot/-/blob/master/app/src/main/java/org/animal/Talks.java) and remove all elements except `WelcomeTalk`.

### 5. Database

Create a  database with the following schema:

```sql
create table talks_contexts
(
    id                 int auto_increment
        primary key,
    session_id         varchar(255)      not null,
    talk_name          varchar(255)      not null,
    type               varchar(255)      not null,
    content            text              not null,
    points_to          int               null,
    level              tinyint default 0 not null,
    code               varchar(255)      null,
    code_set_at        timestamp(3)      null,
    next_talk_response mediumtext        null,
    constraint session_id
        unique (session_id, talk_name, type),
    constraint talks_contexts_points_to_fk
        foreign key (points_to) references talks_contexts (id)
            on delete cascade
)
    charset = utf8mb4;

create index talks_contexts_code
    on talks_contexts (code);

create index talks_contexts_code_sent_at
    on talks_contexts (code_set_at);

create table talks_interactions
(
    id         bigint auto_increment
        primary key,
    type       char(8)      not null,
    chat_id    varchar(255) not null,
    message_id varchar(255) not null,
    created_at timestamp(3) null,
    fly_at     timestamp(3) null
)
    charset = utf8mb4;

create index interactions_fly_at_idx
    on talks_interactions (fly_at);

create table talks_matrix_messages
(
    id      bigint auto_increment
        primary key,
    type    char(8)      not null,
    chat_id varchar(255) not null,
    actions text         null
)
    charset = utf8mb4;

create index matrix_messages_chat_id
    on talks_matrix_messages (chat_id);

create index matrix_messages_type
    on talks_matrix_messages (type);

create table talks_media_resources
(
    id            bigint auto_increment
        primary key,
    resource_id   varchar(255)                              not null,
    input_adapter varchar(255)                              not null,
    file_id       mediumtext                                null,
    updated_at    timestamp(3) default current_timestamp(3) not null
)
    charset = utf8mb4;

create index talks_media_resources_input_adapter
    on talks_media_resources (input_adapter);

create index talks_media_resources_resource_id
    on talks_media_resources (resource_id);

create table talks_message_sender_queue
(
    id            bigint auto_increment
        primary key,
    talk_response text         not null,
    sent_at       timestamp(3) null
)
    charset = utf8mb4;
```

## Interactions

Create a `Talk` subclass with some interactions in it. The name will be `WelcomeTalk` as it was already registered in the `Talks` enum:

```java
public class WelcomeTalk extends Talk {

    public WelcomeTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("WelcomeTalk::start", InputTrigger.ofString("/start"))
        );
    }

    public TalkResponse start(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Like?"
        ).withButton("WelcomeTalk::yes", "Yes")
         .withButton("WelcomeTalk::no", "No");
    }
    
    public TalkResponse yes(TalkRequest request) {
        return thanks(request);
    }

    public TalkResponse no(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "I will delete your vote."
        ).withButton("WelcomeTalk::thanks", "Continue");
    }

    private TalkResponse thanks(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Thanks!\n" 
                + "Vote /again"
        ).withString("WelcomeTalk::start", "/again");
    }
}
```

## Configuration

Create a `config.properties` file. Copy it from [here](https://gitlab.com/gllona/talks-example/-/blob/master/config.properties.EXAMPLE).

Create an `.env` file. Copy it from [here](https://gitlab.com/gllona/talks-example/-/blob/master/.env.EXAMPLE)

Edit both files following the indications provided in the example project [Readme](https://gitlab.com/gllona/talks-example/-/tree/master/#configure-the-bot) for database settings, Telegram and Matrix.org settings.

Note also that `TALK_PACKAGE_NAME` should tell the package name where your `WelcomeTalk` class (or other Talk subclasses) are placed. 
If this value is `org.app`...

* ... And you generate a `TalkResponse` with `.withString("Menu::menu", "/menu")`, then Talks Hippy
  will make the bot to execute the method `menu` in the `org.app.Menu` class if the user enters the `/menu` command
* ... And you generate a `TalkResponse` with `.withString("intro.Menu::menu", "/menu")`, then Talks Hippy
  will make the bot to execute the method `menu` in the `org.app.intro.Menu` class if the user enters the `/menu` command

**Note**: You should use `USE_PERSISTENT_MESSAGE_SENDER=1` and `PERSISTENT_MESSAGE_SENDER_VERSION=3` in
every case. Every other configuration is not guaranteed to work with the current version of the framework.

### Environment variables

Description of the values which must be supplied in the `.env` file:

```
DB_SERVER=mariadb  # Can use also MySQL
DB_PORT_EXTERNAL=3307  # Used with docker compose only. This is the DB port exposed to your host
DB_PORT_INTERNAL=3306  # DB port
DB_DATABASE=talks_hippy_example  # The DB name
DB_USERNAME=talks_hippy_example  # The DB user
DB_PASSWORD=set_to_some_secure_password  # The DB password
DB_CONNECTION_POOL_SIZE=8  # DB connection pool size
DB_CONNECTION_POOL_TIMEOUT_IN_MILLIS=14000  # Timeout after which DB connections will be renewed
DB_CONNECTION_TTL_IN_MILLIS=600000  # Max TTL for DB connections in the DB pool
FILE_DOWNLOAD_PATH=/tmp/bots  # Temporary directory to download media. Make sure this directory exists 
UPDATES_LISTENER_NUM_THREADS=4  # Unused in the current framework version
MESSAGES_SENDER_NUM_THREADS=4  # Unused in the current framework version
USE_SINGLE_THREAD=0  # Set to 1 to use a single thread only
SHUTDOWN_TIMEOUT_IN_MILLIS=10000  # Timeout for the bot shutdown
TALKS_GROUP=one_of_the_TalkGroups_enum_values_like_ALL_or_empty_for_all_Talks  # Use this to activate a sublist of Talks
MAIN_TALK=one_of_the_Talks_enum_value_or_empty_for_default_main_Talk_for_the_TalkGroup  # Set to the class name of your main Talk
BOT_NAME=your_BotFather_bot_name_bot  # Telegram bot name, without `@`
TELEGRAM_API_KEY=your_BotFather_API_key  # Telegram bot API key
TELEGRAM_GET_UPDATES_LIMIT=16  # Maximum number of updated to be fetched from Telegram in each polling cycle
SESSIONS_TO_LOG=*  # If set to `*`, all sessions interactions will be logged in the console. You can set this to a comma-separated list of chat IDs
LOG_NULL_TALK_RESPONSES=1  # Set to `0` for a cleaner log history
PERSONAL_DATA_REMOVAL_DELAY_IN_MILLIS=25000  # Delay for personal data removal. It is recommended not to decrease this value
FALLBACK_RESPONSE="???"  # Text that will be sent to the user in case of an unrecognized command or entry
USE_PERSISTENT_MESSAGE_SENDER=1  # Must be set to `1`
PERSISTENT_MESSAGE_SENDER_VERSION=3  # Must be set to `3`
ALLOW_CONCURRENT_REQUESTS=0  # Allow concurrently receiving multiple messages for the same session 
SAFE_METHOD_NAME_REGEX=".*Safe$"  # `TalkResponse method(TalkRequest request)` methods matching this regular expression will be applied `ALLOW_CONCURRENT_REQUESTS=1`
FLYING_MESSAGES_DURATION_IN_MINUTES=5  # Default flying messages duration
FLYING_MESSAGES_MAX_DURATION_IN_MINUTES=2820  # Maximum flying messages duration
FLYING_MESSAGES_STEALTH_FACTOR=1  # To test flying messages, set this duration multiplier to a number greater than `1` 
MESSAGE_ACTIONS_COMPONENTS=COMMANDS_BUTTONS  # Hints sent to the Matrix bridge, can combine `COMMANDS`, `BUTTONS` and `GLOBALCOMMANDS` separated by `_` 
MESSAGE_ACTIONS_USE_ALPHABETIC_KEYS=0  # Set to 0, alphabetic keys are not implemented yet
RESPONSES_MESSAGE_ID_CACHE_TTL_IN_SECONDS=1800  # It is recommended not to change this value
PROCESS_ECHO_MESSAGES=0  # Set to `1` if you have a Matrix bridge confugured to send Matrix bot messages to the Talks Hippy bot and an Interceptor class in place
ENCODE_EMOJI=0  # Set to `1` if emoji are not correctly displayed
REST_CONTROLLER_PORT=8080  # HTTP port for the bot endpoints
TELEGRAM_ENABLED=1  # Set to `1` to enable the Telegram bot, else set to `0`
MATRIX_ENABLED=1  # Set to `1` to enable the Matrix bot, else set to `0`
WHATSAPP_PHONE_NUMBER="+country_code_then_space_then_mobile_phone_number"  # Needed if the WhatsApp `mautrix` Matrix bridge is enabled
TELEGRAM_PHONE_NUMBER="+country_code_then_space_then_mobile_phone_number"  # Needed if the Telegram `mautrix` Matrix bridge is enabled
SIGNAL_PHONE_NUMBER="+country_code_then_space_then_mobile_phone_number"  # Needed if the Signal `mautrix` Matrix bridge is enabled
MATRIX_SERVER=your_matrix_server_base_domain_name  # Matrix homserver base domain name
MATRIX_USER=your_matrix_bot_username_without_the_server_part  # Matrix user, without `@`, `:` or characters after `:`
MATRIX_ENDPOINTS_API_KEY=your_matrix_endpoints_API_key  # API key that must be used by the Matrix bridge to call the Talks Hippy bot
HEALTH_ENDPOINTS_API_KEY=your_heathcheck_endpoints_API_key
MAGIC_CODE=1234  # In the example project, the first user to enter this code will become the bot administrator
```

## Run

In local environment:

```
./gradlew :run
```

Or run the App class in your favorite IDE.

In production environment:

```
./gradlew clean shadowJar
java -add-opens java.base/java.lang=ALL-UNNAMED -jar build/libs/app-all.jar
```

Note that your generated fat JAR could have a different name.

# Disclaimer

Code is not unit tested, yet. Use it at your own risk.

# Author

Idea and development by [Gorka Llona](http://desarrolladores.logicos.org/gorka/).
