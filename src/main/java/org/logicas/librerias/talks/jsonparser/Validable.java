package org.logicas.librerias.talks.jsonparser;

public interface Validable {

    boolean isValid();
}
