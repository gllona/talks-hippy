package org.logicas.librerias.talks.jsonparser;


import java.util.function.Predicate;

public interface PolymorphicJsonParseSpec<T extends Validable> {

    Predicate<String> getMatcher();
    Class<T> getClazz();
}
