package org.logicas.librerias.talks.jsonparser;

import com.google.gson.*;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
public class PolymorphicJsonParseHelper {

    private final static Gson gson = new GsonBuilder()
        .disableHtmlEscaping()
        //.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
        //.setPrettyPrinting()
        .setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE)
        .serializeNulls()
        .create();

    public static <T extends Validable> T extract(
        String json,
        List<? extends PolymorphicJsonParseSpec<T>> specs
    ) {
        return doExtract(json, specs, null);
    }

    public static <T extends Validable, U extends T> U extract(
        String json,
        List<? extends PolymorphicJsonParseSpec<T>> specs,
        Class<U> baseClass
    ) {
        return (U) doExtract(json, specs, baseClass);
    }

    private static <T extends Validable, U extends T> T doExtract(
        String json,
        List<? extends PolymorphicJsonParseSpec<T>> specs,
        Class<U> baseClass
    ) {
        try {
            gson.fromJson(json, JsonObject.class);
        } catch (JsonSyntaxException e) {
            throw new PolymorphicJsonParseException(PolymorphicJsonParseException.Type.INVALID_JSON, json);
        }

        PolymorphicJsonParseSpec<T> spec = specs.stream()
            .filter(possibleSpec -> possibleSpec.getMatcher().test(json))
            .findFirst()
            .orElse(null);
        if (spec == null) {
            throw new PolymorphicJsonParseException(PolymorphicJsonParseException.Type.MATCHING_ERROR, json);
        }

        T result = marshall(json, spec.getClazz());

        if (result == null) {
            throw new PolymorphicJsonParseException(PolymorphicJsonParseException.Type.MARSHALL_ERROR, json);
        }

        if (! result.isValid()) {
            throw new PolymorphicJsonParseException(PolymorphicJsonParseException.Type.NOT_VALID, json);
        }

        if (baseClass != null && ! baseClass.isAssignableFrom(result.getClass())) {
            throw new PolymorphicJsonParseException(PolymorphicJsonParseException.Type.NOT_ASSIGNABLE, json);
        }

        return result;
    }

    private static <T extends Validable> T marshall(String json, Class<T> clazz) {
        try {
            return gson.fromJson(json, clazz);
        } catch (JsonSyntaxException e) {
            return null;
        }
    }

    public static boolean matches(String json, String field, String value) {
        return doMatches(json, field, value, false);
    }

    public static boolean matchesOrNull(String json, String field, String value) {
        return doMatches(json, field, value, true);
    }

    private static boolean doMatches(String json, String field, String value, boolean allowNulls) {
        JsonObject jsonObject;
        try {
            jsonObject = gson.fromJson(json, JsonObject.class);
        } catch (JsonSyntaxException e) {
            return false;
        }

        JsonElement actualValue = jsonObject.get(field);
        if (actualValue == null) {
            return allowNulls;
        }

        String actualValueString = actualValue.toString().replaceAll("^\"|\"$", "");

        return Objects.equals(value, actualValueString);
    }
}
