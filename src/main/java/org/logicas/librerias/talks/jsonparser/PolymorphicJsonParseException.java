package org.logicas.librerias.talks.jsonparser;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PolymorphicJsonParseException extends RuntimeException {

    public enum Type {
        INVALID_JSON,
        MATCHING_ERROR,
        MARSHALL_ERROR,
        NOT_VALID,
        NOT_ASSIGNABLE
    }

    private final Type type;
    private final String message;

    @Override
    public String getMessage() {
        return String.format("[error=%s] JSON parse exception: %s", type, message);
    }
}
