package org.logicas.librerias.talks.telegram;

import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.model.request.ReplyKeyboardRemove;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.InputTrigger.Button;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ButtonDecorator {

//    private static final int MAX_EQUIVALENT_TEXT_LENGTH = 36;
//    private static final String OUTER_PAD = "8";
//    private static final String INNER_PAD = "8";

    private static final int LENGTH_PAD_INNER = 3;
    private static final int LENGTH_ALL_MAX = 40;

    private static ButtonDecorator instance;

    public static synchronized ButtonDecorator getInstance() {
        if (instance == null) {
            instance = new ButtonDecorator();
        }
        return instance;
    }

    public void decorate(TalkResponse response) {
        List<Button> buttons = extractButtons(response);
        if (buttons.size() == 0) {
            if (! response.getHandlers().equals(Talk.preserveHandlers())) {
                response.addDecoration(abstractSendRequest -> abstractSendRequest.replyMarkup(new ReplyKeyboardRemove()));
            }
            return;
        }
        layoutButtons(response, buttons);
    }

    private List<Button> extractButtons(TalkResponse response) {
        List<InputTrigger.Button> buttons = response.getHandlers()
            .stream()
            .flatMap(handler -> handler.getTriggers().stream())
            .map(InputTrigger::getButton)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
        return buttons;
    }

    private void layoutButtons(TalkResponse response, List<Button> buttons) {
        List<Button> positioned = new ArrayList<>();
        List<Button> unpositioned = new ArrayList<>();
        int maxRow = 0;
        int maxColumn = 0;

        for (Button button : buttons) {
            if (button.row != null && button.column != null) {
                positioned.add(button);
                if (button.row > maxRow) {
                    maxRow = button.row;
                }
                if (button.column > maxColumn) {
                    maxColumn = button.column;
                }
            } else {
                unpositioned.add(button);
            }
        }

        int columns = maxColumn + 1;
        List<Button[]> layout;
        List<Button[]> lastLayout = null;
        while (true) {
            layout = doLayoutButtons(columns, positioned, unpositioned);
            if (unpositioned.size() == 0) {
                break;
            }
            if (columns > maxColumn + 1 && (! layoutFits(layout) || columnIsEmpty(layout, columns - 1))) {
                layout = lastLayout;
                columns--;
                break;
            }
            lastLayout = layout;
            columns++;
        }

        List<Button[]> finalLayout = new ArrayList<>(layout);
        int finalColumns = columns;
        response.addDecoration(abstractSendRequest ->
            abstractSendRequest.replyMarkup(new ReplyKeyboardMarkup(keyboardMarkup(finalLayout, finalColumns))
                .resizeKeyboard(true)
                //.oneTimeKeyboard(true)
                //.selective(true)
            )
        );
    }

    private List<Button[]> doLayoutButtons(int columns, List<Button> positioned, List<Button> unpositioned) {
        List<Button[]> rows = new ArrayList<>();
        int maxColumn = 0;
        for (Button button : positioned) {
            maxColumn = Math.max(maxColumn,
                                 addToRows(rows, Math.max(columns, maxColumn + 1), button, button.row, button.column));
        }

        int row = 0;
        int column = -1;

        for (Button button : unpositioned) {
            do {
                column++;
                if (column >= columns) {
                    column = 0;
                    row++;
                }
            } while (row < rows.size() && rows.get(row).length > column && rows.get(row)[column] != null);

            addToRows(rows, maxColumn + 1, button, row, column);
        }

        return rows;
    }

    private int addToRows(List<Button[]> rows, int columns, Button button, int row, int column) {
        int maxColumn = Math.max(columns - 1, column);
        for (int i = 0; i < rows.size(); i++) {
            Button[] oldRow = rows.get(i);
            if (oldRow.length < maxColumn + 1) {
                Button[] newRow = new Button[maxColumn + 1];
                System.arraycopy(oldRow, 0, newRow, 0, oldRow.length);
                rows.set(i, newRow);
            }
        }
        for (int r = rows.size() - 1; r < row; r++) {
            rows.add(new Button[maxColumn + 1]);
        }
        rows.get(row)[column] = button;
        return maxColumn;
    }

    private boolean columnIsEmpty(List<Button[]> layout, int column) {
        for (Button[] row : layout) {
            if (row.length > column && row[column] != null) {
                return false;
            }
        }
        return true;
    }

    private boolean layoutFits(List<Button[]> layout) {
        if (layout.size() == 0) {
            return true;
        }

        int maxLength = layout.stream()
            .flatMap(Arrays::stream)
            .filter(Objects::nonNull)
            .mapToInt(b -> buttonLength(b.getText()))
            .max()
            .orElse(0);
        int columns = layout.get(0).length;

        boolean fits = LENGTH_PAD_INNER * (columns - 1) + maxLength * columns <= LENGTH_ALL_MAX;

        return fits;
    }

    private int buttonLength(String text) {
        return text == null ? 0 : text.length();
    }

    private String[][] keyboardMarkup(List<Button[]> layout, int columns) {
        String[][] markup = new String[layout.size()][columns];

        for (int row = 0; row < layout.size(); row++) {
            Button[] rowButtons = layout.get(row);
            for (int column = 0; column < rowButtons.length; column++) {
                markup[row][column] = rowButtons[column] != null ? rowButtons[column].getText() : " ";
            }
            for (int column = rowButtons.length; column < columns; column++) {
                markup[row][column] = " ";
            }
        }

        return markup;
    }
}
