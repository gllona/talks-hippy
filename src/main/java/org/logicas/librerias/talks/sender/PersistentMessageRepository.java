package org.logicas.librerias.talks.sender;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;
import org.logicas.librerias.talks.config.InputAdapters;

import java.util.List;
import java.util.stream.Collectors;

public class PersistentMessageRepository extends SingleKeyEntityRepository<PersistentMessage> {

    private static PersistentMessageRepository instance;

    private Dao dao;

    private PersistentMessageRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized PersistentMessageRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new PersistentMessageRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "talks_message_sender_queue";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return PersistentMessage.FIELDS;
    }

    @Override
    public String idFieldName() {
        return PersistentMessage.FIELD_ID;
    }

    public List<PersistentMessage> readPendingMessages() {
        return readByCondition("sent_at IS NULL");
    }

    public List<PersistentMessage> readMatrixMessages() {
        return readAll().stream()
            .filter(message -> InputAdapters.forTalkResponse(message.getUnencodedTalkResponse()) == InputAdapters.MATRIX)
            .collect(Collectors.toList());
    }
}
