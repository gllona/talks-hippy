package org.logicas.librerias.talks.sender.v2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.LibSetting;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.concurrent.*;

class LowPrioritySender {

    private class Sender implements Runnable {
        private boolean doFinish = false;

        @Override
        public void run() {
            while (! doFinish && ! Thread.currentThread().isInterrupted()) {
                sendOneMessage();
            }
        }

        public synchronized void cancel() {
            doFinish = true;
        }

        private void sendOneMessage() {
            TalkResponse talkResponse = null;
            try {
                talkResponse = sendQueue.take();
                sendMessage(talkResponse);
            } catch (InterruptedException e) {
                cancel();
            }
        }

        private void sendMessage(TalkResponse talkResponse) throws InterruptedException {
            boolean accepted = rendevouzQueue.acceptLowPriority(talkResponse);

            if (! accepted) {
                sendQueue.put(talkResponse);
            }

            Thread.sleep(interMessageDelay);
        }
    }


    private static final String THREADS_ID = "LPST";
    private static final int interMessageDelay = LibSetting.getInteger(LibSetting.CHAT_INTER_MESSAGE_DELAY_IN_MILLIS);
    private static LowPrioritySender instance = null;
    private static final Logger logger = LogManager.getLogger(LowPrioritySender.class);

    private final TalksConfiguration talksConfig;
    private final RendevouzQueue rendevouzQueue;
    private final CustomThreadFactory threadFactory = CustomThreadFactory.getInstance();
    private ExecutorService executor;
    private final Sender sender;
    private final BlockingQueue<TalkResponse> sendQueue = new LinkedBlockingQueue<>();

    public static synchronized LowPrioritySender getInstance(TalksConfiguration config) {
        if (instance == null) {
            instance = new LowPrioritySender(config);
        }
        return instance;
    }

    public LowPrioritySender(TalksConfiguration config) {
        this.talksConfig = config;
        rendevouzQueue = RendevouzQueue.getInstance(talksConfig);
        createExecutor();
        sender = new Sender();
        executor.execute(sender);
    }

    private void createExecutor() {
        executor = Executors.newSingleThreadExecutor(
            threadFactory.create(THREADS_ID)
        );
    }

    public void send(TalkResponse response) {
        try {
            sendQueue.put(response);
        } catch (InterruptedException ignored) {
            logger.error("Dropping TalkResponse, queue is full");
        }
    }

    public void shutdown() {
        logger.info("Stopping low priority sender...");
        threadFactory.interruptThreads(THREADS_ID);
        logger.info("Shutting down executor...");
        executor.shutdown();
        try {
            executor.awaitTermination(LibSetting.getInteger(LibSetting.SHUTDOWN_TIMEOUT_IN_MILLIS), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            logger.warn("Interrupted while waiting for LowPrioritySender to finish");
        }
        logger.info("Low priority sender stopped.");
    }
}
