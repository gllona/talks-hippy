package org.logicas.librerias.talks.sender;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@Setter
@Getter
@ToString
public class PersistentMessage extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_TALK_RESPONSE = "talk_response";
    public static final String FIELD_SENT_AT = "sent_at";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_TALK_RESPONSE,
        FIELD_SENT_AT
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private String talkResponse;
    private LocalDateTime sentAt;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getTalkResponse() {
        return talkResponse;
    }

    public LocalDateTime getSentAt() {
        return sentAt;
    }

    public void setSentAt(LocalDateTime sentAt) {
        this.sentAt = sentAt;
    }

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(talkResponse, FIELD_TALK_RESPONSE, params);
        conditionalAddToParams(sentAt, FIELD_SENT_AT, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }

    public void setUnencodedTalkResponse(TalkResponse response) {
        talkResponse = gson.toJson(response);
    }

    public TalkResponse getUnencodedTalkResponse() {
        TalkResponse response = gson.fromJson(talkResponse, TalkResponse.class);
        return response;
    }
}
