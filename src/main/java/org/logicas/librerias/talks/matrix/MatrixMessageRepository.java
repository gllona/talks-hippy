package org.logicas.librerias.talks.matrix;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.ToNumberPolicy;
import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.QueryBuilder;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

import java.util.List;
import java.util.Map;

public class MatrixMessageRepository extends SingleKeyEntityRepository<MatrixMessage> {

    private static MatrixMessageRepository instance;

    private static Gson gson = new GsonBuilder()
        .disableHtmlEscaping()
        //.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
        //.setPrettyPrinting()
        .setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE)
        .serializeNulls()
        .create();

    private Dao dao;

    private MatrixMessageRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized MatrixMessageRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new MatrixMessageRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "talks_matrix_messages";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return MatrixMessage.FIELDS;
    }

    @Override
    public String idFieldName() {
        return MatrixMessage.FIELD_ID;
    }

    public void saveOutgoingActions(String roomId, Map<String, String> messageActions) {
        if (messageActions == null) {
            return;
        }

        deleteByCondition("chat_id = '" + QueryBuilder.escapeSql(roomId) + "'");

        MatrixMessage matrixMessage = new MatrixMessage();
        matrixMessage.setChatId(roomId);
        matrixMessage.setType(MatrixMessage.TYPE_OUTGOING);
        matrixMessage.setActions(gson.toJson(messageActions));
        save(matrixMessage);
    }

    public MatrixMessage readByRoomId(String roomId) {
        List<MatrixMessage> matrixMessages = readByCondition("chat_id = '" + QueryBuilder.escapeSql(roomId) + "'");
        return matrixMessages.stream().findFirst().orElse(null);
    }

    public void deleteByRoomId(String roomId) {
        deleteByCondition("chat_id = '" + QueryBuilder.escapeSql(roomId) + "'");
    }

    //TODO delete personal data implementation for this talks_matrix_messages table
}
