package org.logicas.librerias.talks.matrix;

import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.config.InputAdapters;
import org.logicas.librerias.talks.config.LibSetting;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Data
public class MatrixResponse {

    @Data
    public static class Message {
        private final String roomId;
        private final InputAdapters channel;
        private final String messageType;
        private final MatrixBodyType bodyType;
        private final String mimeType;
        private final String body;
        private final String mxcUri;
        private final String filename;
        private final Map<String, String> actions;
        private final long id;

        public long getId() {
            return id;
        }

        public static Message ofText(String roomId, InputAdapters channel, String text, List<String> commands, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            return new Message(roomId, channel, MatrixBodyType.TEXT.getMediaType(), MatrixBodyType.TEXT, null, text, null, null, buildActions(commands, globalCommands, triggers, showActions), dbId);
        }

        public static Message ofHtml(String roomId, InputAdapters channel, String html, List<String> commands, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            return new Message(roomId, channel, MatrixBodyType.HTML.getMediaType(), MatrixBodyType.HTML, null, html, null, null, buildActions(commands, globalCommands, triggers, showActions), dbId);
        }

        public static Message ofLocation(String roomId, InputAdapters channel, float latitude, float longitude, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            String geoUri = "geo:" + latitude + "," + longitude;
            return new Message(roomId, channel, MatrixBodyType.GEO_URI.getMediaType(), MatrixBodyType.GEO_URI, null, geoUri, null, null, buildActions(null, globalCommands, triggers, showActions), dbId);
        }

        public static Message ofAudio(String roomId, InputAdapters channel, File file, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            return ofMedia(roomId, channel, MatrixBodyType.AUDIO, file, globalCommands, triggers, showActions, dbId);
        }

        public static Message ofImage(String roomId, InputAdapters channel, File file, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            return ofMedia(roomId, channel, MatrixBodyType.IMAGE, file, globalCommands, triggers, showActions, dbId);
        }

        public static Message ofVideo(String roomId, InputAdapters channel, File file, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            return ofMedia(roomId, channel, MatrixBodyType.VIDEO, file, globalCommands, triggers, showActions, dbId);
        }

        public static Message ofFile(String roomId, InputAdapters channel, File file, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            return ofMedia(roomId, channel, MatrixBodyType.FILE, file, globalCommands, triggers, showActions, dbId);
        }

        public static Message ofAudioId(String roomId, InputAdapters channel, String fileId, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            return ofMediaId(roomId, channel, MatrixBodyType.AUDIO, fileId, globalCommands, triggers, showActions, dbId);
        }

        public static Message ofImageId(String roomId, InputAdapters channel, String fileId, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            return ofMediaId(roomId, channel, MatrixBodyType.IMAGE, fileId, globalCommands, triggers, showActions, dbId);
        }

        public static Message ofVideoId(String roomId, InputAdapters channel, String fileId, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            return ofMediaId(roomId, channel, MatrixBodyType.VIDEO, fileId, globalCommands, triggers, showActions, dbId);
        }

        public static Message ofFileId(String roomId, InputAdapters channel, String fileId, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            return ofMediaId(roomId, channel, MatrixBodyType.FILE, fileId, globalCommands, triggers, showActions, dbId);
        }

        private static Message ofMedia(String roomId, InputAdapters channel, MatrixBodyType bodyType, File file, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            if (file == null) {
                logger.warn(String.format("Can not create MatrixResponse from null File for dbId [%d]", dbId));
                return new Message(roomId, channel, bodyType.getMediaType(), bodyType, null, null, null, null, buildActions(null, globalCommands, triggers, showActions), dbId);
            }

            Path path;
            String filename;
            try {
                path = file.toPath();
                filename = path.getFileName().toString();
            } catch (Exception e) {
                logger.warn(String.format("Can not create MatrixResponse from invalid File for dbId [%d]", dbId));
                return new Message(roomId, channel, bodyType.getMediaType(), bodyType, null, null, null, null, buildActions(null, globalCommands, triggers, showActions), dbId);
            }

            try {
                byte[] bytes = Files.readAllBytes(path);
                String base64bytes = new String(Base64.getEncoder().encode(bytes));
                String mimeType = Files.probeContentType(path);
                return new Message(roomId, channel, bodyType.getMediaType(), bodyType, mimeType, base64bytes, null, filename, buildActions(null, globalCommands, triggers, showActions), dbId);
            } catch (Exception e) {
                logger.warn(String.format("Can not read bytes from file [%s]", path), e);
                return new Message(roomId, channel, bodyType.getMediaType(), bodyType, null, null, null, null, buildActions(null, globalCommands, triggers, showActions), dbId);
            }
        }

        private static Message ofMediaId(String roomId, InputAdapters channel, MatrixBodyType bodyType, String fileId, List<String> globalCommands, List<String> triggers, Boolean showActions, long dbId) {
            if (fileId == null) {
                logger.warn(String.format("Can not create MatrixResponse from null fileId for dbId [%d]", dbId));
                return new Message(roomId, channel, bodyType.getMediaType(), bodyType, null, null, null, null, buildActions(null, globalCommands, triggers, showActions), dbId);
            }

            return new Message(roomId, channel, bodyType.getMediaType(), bodyType, null, null, fileId, null, buildActions(null, globalCommands, triggers, showActions), dbId);
        }

        public static Message ofDeleteMessage(String roomId, InputAdapters channel, String messageId, long dbId) {
            return new Message(roomId, channel, MatrixBodyType.DELETE_MESSAGE.getMediaType(), MatrixBodyType.DELETE_MESSAGE, null, messageId, null, null, null, dbId);
        }

        private static Map<String, String> buildActions(List<String> commands, List<String> globalCommands, List<String> triggers, Boolean showActions) {
            Map<String, String> actions = new TreeMap<>(Comparator.comparing(Message::padKey));

            if (! shouldProvideActions(showActions)) {
                return actions;
            }

            String components = LibSetting.get(LibSetting.MESSAGE_ACTIONS_COMPONENTS);
            boolean useAlphabeticKeys = LibSetting.getInteger(LibSetting.MESSAGE_ACTIONS_USE_ALPHABETIC_KEYS) == 1;
            String key = null;

            if (isActionsComponentIncluded("COMMANDS", components) && commands != null) {
                for (String command : commands) {
                    key = addAction(actions, key, command, useAlphabeticKeys);
                }
            }
            if (isActionsComponentIncluded("BUTTONS", components) && triggers != null) {
                for (String button : triggers) {
                    key = addAction(actions, key, button, useAlphabeticKeys);
                }
            }
            if (isActionsComponentIncluded("GLOBALCOMMANDS", components) && globalCommands != null) {
                for (String command : globalCommands) {
                    key = incrementKey(key, useAlphabeticKeys);
                    addAction(actions, key, command, useAlphabeticKeys);
                }
            }

            return actions;
        }

        private static String padKey(String key) {
            return key.length() == 1 ? "0" + key : key;
        }

        private static boolean shouldProvideActions(Boolean showActions) {
            if ("NONE".equals(LibSetting.get(LibSetting.MESSAGE_ACTIONS_COMPONENTS))) {
                return false;
            } else if (showActions != null && ! showActions) {
                return false;
            } else {
                return true;
            }
        }

        private static boolean isActionsComponentIncluded(String component, String components) {
            return ("_" + components + "_").contains("_" + component + "_");
        }

        private static String addAction(Map<String, String> actions, String key, String action, boolean useAlphabeticKeys) {
            if (! actions.containsValue(action)) {
                key = incrementKey(key, useAlphabeticKeys);
                actions.put(key, action);
            }
            return key;
        }

        private static String incrementKey(String key, boolean useAlphabeticKeys) {
            if (useAlphabeticKeys) {
                if (key == null) {
                    key = "@";
                }
                int i = key.charAt(0);
                i++;
                return Character.toString((char) i);
            } else {
                if (key == null) {
                    key = "0";
                }
                int i = Integer.parseInt(key);
                i++;
                return Integer.toString(i);
            }
        }

        public Message copyWithoutActions() {
            return new Message(roomId, channel, messageType, bodyType, mimeType, body, mxcUri, filename, null, id);
        }
    }

    public static MatrixResponse accepted() {
        return new MatrixResponse("OK", null);
    }

    public static MatrixResponse error(String message) {
        return new MatrixResponse(message, null);
    }

    private static Logger logger = LogManager.getLogger(MatrixResponse.class);

    private final String description;
    private final List<Message> messages;
}
