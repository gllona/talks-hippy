package org.logicas.librerias.talks.matrix;

import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@Setter
@Getter
@ToString
public class MatrixMessage extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_CHAT_ID = "chat_id";
    public static final String FIELD_ACTIONS = "actions";

    public static final String TYPE_INCOMING = "incoming";
    public static final String TYPE_OUTGOING = "outgoing";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_TYPE,
        FIELD_CHAT_ID,
        FIELD_ACTIONS
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private static final Type actionsMapType = new TypeToken<Map<String, String>>() {}.getType();

    private Long id;
    private String type;
    private String chatId;
    private String actions;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getActions() {
        return actions;
    }

    public Map<String, String> getActionsAsMap() {
        return gson.fromJson(actions, actionsMapType);
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(type, FIELD_TYPE, params);
        conditionalAddToParams(chatId, FIELD_CHAT_ID, params);
        conditionalAddToParams(actions, FIELD_ACTIONS, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }
}
