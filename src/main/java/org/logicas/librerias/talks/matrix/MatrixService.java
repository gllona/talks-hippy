package org.logicas.librerias.talks.matrix;

import kotlin.Triple;
import org.logicas.librerias.talks.api.InputAdapterApi;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.InputAdapters;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.sender.v3.MessageSender;

import java.util.*;

public class MatrixService {

    private static MatrixService instance = null;

    private final TalksConfiguration talksConfig;
    private final MessageSender messageSender;
    private final InputAdapterApi matrixInputAdapter;
    private final MatrixMessageRepository matrixMessageRepository;

    public static synchronized MatrixService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new MatrixService(talksConfig);
        }
        return instance;
    }

    public MatrixService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        messageSender = MessageSender.Companion.getInstance(talksConfig);
        matrixInputAdapter = talksConfig.getInputAdaptersManager().get(InputAdapters.MATRIX);
        matrixMessageRepository = MatrixMessageRepository.getInstance(talksConfig.getDao());
    }

    public void receiveMessage(MatrixReceiveMessageRequest matrixRequest) {
        processActions(matrixRequest);
        TalkRequest request = TalkRequest.ofMatrix(matrixInputAdapter, matrixRequest);
        matrixMessageRepository.deleteByRoomId(matrixRequest.getRoomId());
        matrixInputAdapter.receiveRequest(request);
    }

    private void processActions(MatrixReceiveMessageRequest matrixRequest) {
        String roomId = matrixRequest.getRoomId();
        String text = matrixRequest.getMessage().text();

        if (text == null) {
            return;
        }
        text = text.toUpperCase();

        MatrixMessage matrixMessage = matrixMessageRepository.readByRoomId(roomId);
        if (matrixMessage == null) {
            return;
        }

        Map<String, String> actions = matrixMessage.getActionsAsMap();
        if (actions != null) {
            for (Map.Entry<String, String> action : actions.entrySet()) {
                if (action.getKey().equals(text)) {
                    matrixRequest.getMessage().setText(action.getValue());
                    return;
                }
            }
        }
    }

    public MatrixResponse getMessages() {
        List<MatrixResponse.Message> matrixMessages = messageSender.getMatrixMessages();
        List<MatrixResponse.Message> finalMatrixMessages = new LinkedList<>();
        Set<String> roomIds = new HashSet<>();

        for (int i = matrixMessages.size() - 1; i >= 0; i--) {
            MatrixResponse.Message matrixMessage = matrixMessages.get(i);
            String roomId = matrixMessage.getRoomId();
            MatrixResponse.Message finalMatrixMessage;
            if (! roomIds.contains(roomId)) {
                roomIds.add(roomId);
                Map<String, String> messageActions = matrixMessage.getActions();
                matrixMessageRepository.saveOutgoingActions(roomId, messageActions);
                finalMatrixMessage = matrixMessage;
            } else {
                finalMatrixMessage = matrixMessage.copyWithoutActions();
            }
            finalMatrixMessages.add(0, finalMatrixMessage);
        }

        return new MatrixResponse("OK", finalMatrixMessages);
    }

    public void confirmMessages(MatrixConfirmMessagesRequest matrixRequest) {
        List<Triple<Long, String, String>> messageIdsAndMxcUris = matrixRequest.getMessages().stream()
            .map(message -> new Triple<>(message.getSourceId(), message.getMatrixId(), message.getMxcUri()))
            .toList();
        messageSender.confirmSentMessagesReception(messageIdsAndMxcUris, InputAdapters.MATRIX);
    }
}
