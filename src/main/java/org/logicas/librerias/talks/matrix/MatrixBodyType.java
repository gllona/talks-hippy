package org.logicas.librerias.talks.matrix;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
@Getter
public enum MatrixBodyType {
    TEXT("m.text"),
    HTML("m.text"),
    GEO_URI("m.location"),
    IMAGE("m.image"),
    AUDIO("m.audio"),
    VIDEO("m.video"),
    FILE("m.file"),
    DELETE_MESSAGE("m.room.redaction");

    public static final List<MatrixBodyType> TEXT_BASED = List.of(TEXT, HTML);
    public static final List<MatrixBodyType> BYTES_BASED = List.of(IMAGE, AUDIO, VIDEO, FILE);

    public static MatrixBodyType from(String mediaType) { // never returns HTML
        return Arrays.stream(values())
            .filter(bodyType -> bodyType.mediaType.equals(mediaType))
            .findFirst()
            .orElse(null);
    }

    private final String mediaType;
}
