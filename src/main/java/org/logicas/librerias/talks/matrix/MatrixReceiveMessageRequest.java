package org.logicas.librerias.talks.matrix;

import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.config.LibSetting;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

@Data
public class MatrixReceiveMessageRequest {

    public class Message {
        public String type() {
            return messageType;
        }

        public String messageId() {
            return eventId;
        }

        public String format() {
            return format;
        }

        public String text() {
            return body == null ? formattedBody : body;
        }

        public void setText(String text) {
            body = text;
        }

        public String geoUri() {
            return geoUri;
        }

        public String getBytes() {
            return bytes;
        }

        public String getMxcUri() {
            return mxcUri;
        }

        public File getFile() {
            if (bytes == null) {
                return null;
            }
            byte[] decodedBytes = Base64.getDecoder().decode(bytes);
            return writeTmpFile(decodedBytes);
        }

        private File writeTmpFile(byte[] bytes) {
            Path dir = Path.of(LibSetting.get(LibSetting.FILE_DOWNLOAD_PATH));
            if (! assureDirectoryExists(dir)) {
                return null;
            }
            String prefix = "incoming_";
            String suffix = ".file";
            Path tempFile = null;
            try {
                tempFile = Files.createTempFile(dir, prefix, suffix);
                Files.write(tempFile, bytes);
                return tempFile.toFile();
            } catch (IOException e) {
                logger.error(String.format("Could not write bytes to file %s", tempFile), e);
                return null;
            }
        }

        private boolean assureDirectoryExists(Path dir) {
            try {
                Files.createDirectories(dir);
                return true;
            } catch (IOException e) {
                return false;
            }
        }
    }

    private static Logger logger = LogManager.getLogger(MatrixReceiveMessageRequest.class);

    private Long timestamp;
    private String roomId;
    private String eventId;
    private String senderId; // can start with @telegram or @signal or @whatsapp, any other comes from matrix
    private String eventType; // m.room.message
    private String body;
    private String messageType;
    private String format;
    private String formattedBody;
    private String geoUri;
    private String mimeType;
    private String mxcUri;
    private String bytes; // base64 encoded

    private transient final Message message = new Message();

    public String getRoomId() {
        return roomId;
    }

    public String getEventId() {
        return eventId;
    }

    public String getSenderId() {
        return senderId;
    }

    public String getBody() {
        return body;
    }

    public boolean shouldProcess() {
        return MatrixBodyType.from(messageType) != null;
    }

    public boolean isValid() {
        MatrixBodyType bodyType = MatrixBodyType.from(messageType);
        return roomId != null &&
            eventId != null &&
            senderId != null &&
            "m.room.message".equals(eventType) &&
            bodyType != null &&
            (
                (body == null || MatrixBodyType.TEXT_BASED.contains(bodyType)) ||
                (geoUri == null && MatrixBodyType.GEO_URI == bodyType) ||
                (bytes == null || MatrixBodyType.BYTES_BASED.contains(bodyType))
            ) &&
            (body != null || geoUri != null || bytes != null);
    }
}
