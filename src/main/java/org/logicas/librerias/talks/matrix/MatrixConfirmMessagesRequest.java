package org.logicas.librerias.talks.matrix;

import lombok.Data;

import java.util.List;

@Data
public class MatrixConfirmMessagesRequest {

    @Data
    public class Message {
        private Long sourceId;
        private String matrixId;
        private String mxcUri;
    }

    private List<Message> messages;

    public boolean isValid() {
        return messages != null &&
            messages.stream().allMatch(message -> message.sourceId != null);
    }
}
