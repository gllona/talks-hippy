package org.logicas.librerias.talks.config;

public class Constants {

    public static final String PROPERTIES_DIRECTORY = "./config";
    public static final String PROPERTIES_FILES_REGEX = "^config(-[a-zA-Z0-9]+)?\\.properties$";
}
