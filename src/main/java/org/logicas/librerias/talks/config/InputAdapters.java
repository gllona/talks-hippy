package org.logicas.librerias.talks.config;

import lombok.AllArgsConstructor;
import org.logicas.librerias.talks.api.InputAdapterApi;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.logicas.librerias.talks.health.HealthInputAdapter;
import org.logicas.librerias.talks.matrix.MatrixInputAdapter;
import org.logicas.librerias.talks.telegram.TelegramInputAdapter;

import java.util.function.Supplier;

@AllArgsConstructor
public enum InputAdapters {

    TELEGRAM(TelegramInputAdapter.class, () -> LibSetting.isEnabled(LibSetting.TELEGRAM_ENABLED)),
    MATRIX(MatrixInputAdapter.class, () -> LibSetting.isEnabled(LibSetting.MATRIX_ENABLED)),
    HEALTH(HealthInputAdapter.class, () -> true);

    public Class<? extends InputAdapterApi> adapter;
    private Supplier<Boolean> isActiveSupplier;

    public Class<? extends InputAdapterApi> getAdapter() {
        return adapter;
    }

    public boolean isActive() {
        return isActiveSupplier.get();
    }

    public static InputAdapters forTalkRequest(TalkRequest request) {
        if (request.getUpdate() != null) {
            return InputAdapters.TELEGRAM;
        } else if (request.getMatrixRequest() != null) {
            return InputAdapters.MATRIX;
        } else {
            return null;
        }
    }

    public static InputAdapters forTalkResponse(TalkResponse response) {
        return forChatId(response.getChatId());
    }

    public static InputAdapters forChatId(String chatId) {
        if (chatId.startsWith("!") && chatId.contains(":")) {
            return InputAdapters.MATRIX;
        } else {
            try {
                Long.parseLong(chatId);
            } catch (Exception e) {
                return null;
            }
            return InputAdapters.TELEGRAM;
        }
    }
}
