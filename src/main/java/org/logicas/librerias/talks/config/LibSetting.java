package org.logicas.librerias.talks.config;

import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;

import static org.logicas.librerias.talks.config.LibSetting.Source.*;

@RequiredArgsConstructor
public enum LibSetting {
    MESSAGES_SENDER_NUM_THREADS(APP), // ignored for RatedPersistentMessageSenderV3
    USE_SINGLE_THREAD(APP),
    SHUTDOWN_TIMEOUT_IN_MILLIS(APP),
    TALK_PACKAGE_NAME(APP),
    START_MESSAGE(APP),
    TELEGRAM_GET_UPDATES_LIMIT(TELEGRAM),
    CHAT_INTER_MESSAGE_DELAY_IN_MILLIS(APP),
    ALL_CHATS_MAX_MESSAGES_PER_SECOND(APP),
    BOT_NAME(TELEGRAM),
    TELEGRAM_API_KEY(TELEGRAM),
    FILE_DOWNLOAD_PATH(APP),
    DB_RDBMS(APP),
    DB_HOST(APP),
    DB_PORT(APP),
    DB_DATABASE(APP),
    DB_USERNAME(APP),
    DB_PASSWORD(APP),
    DB_OPTIONS(APP),
    DB_CONNECTION_POOL_SIZE(APP),
    DB_CONNECTION_POOL_TIMEOUT_IN_MILLIS(APP),
    DB_CONNECTION_TTL_IN_MILLIS(APP),
    DB_KEEPALIVE_QUERY(APP),
    SESSIONS_TO_LOG(APP),
    LOG_NULL_TALK_RESPONSES(APP),
    FALLBACK_RESPONSE(APP),
    PERSONAL_DATA_REMOVAL_DELAY_IN_MILLIS(APP),
    USE_PERSISTENT_MESSAGE_SENDER(APP),
    PERSISTENT_MESSAGE_SENDER_VERSION(APP),
    DUPLICATES_CACHE_TTL_IN_SECONDS(APP),
    ALLOW_CONCURRENT_REQUESTS(APP),
    CONCURRENT_REQUESTS_CACHE_TTL_IN_SECONDS(APP),
    SAFE_METHOD_NAME_REGEX(APP),
    FLYING_MESSAGES_DURATION_IN_MINUTES(APP),
    FLYING_MESSAGES_MAX_DURATION_IN_MINUTES(APP),
    FLYING_MESSAGES_STEALTH_FACTOR(APP),
    RESPONSES_MESSAGE_ID_CACHE_TTL_IN_SECONDS(APP),
    MESSAGE_ACTIONS_COMPONENTS(APP),
    MESSAGE_ACTIONS_USE_ALPHABETIC_KEYS(APP),
    SWITCH_SESSION_CODE_LENGTH(APP),
    SWITCH_SESSION_TIMEOUT_IN_SECONDS(APP),
    PROCESS_ECHO_MESSAGES(APP),
    TELEGRAM_ENABLED(APP),
    MATRIX_ENABLED(APP),
    MATRIX_BOT_USERNAME(MATRIX),
    WHATSAPP_PHONE_NUMBER(MATRIX),
    TELEGRAM_PHONE_NUMBER(MATRIX),
    SIGNAL_PHONE_NUMBER(MATRIX),
    MATRIX_SERVER(MATRIX),
    ENCODE_EMOJI(APP);

    private final Source source;

    @RequiredArgsConstructor
    enum Source {
        APP("app"),
        TELEGRAM("telegram"),
        MATRIX("matrix");

        private final String propertiesFileSuffix;
    }

    public static boolean isSourceEnabled(Source source) {
        return switch (source) {
            case APP -> true;
            case TELEGRAM -> "1".equals(LibConfig.getInstance().getString(LibSetting.TELEGRAM_ENABLED));
            case MATRIX -> "1".equals(LibConfig.getInstance().getString(LibSetting.MATRIX_ENABLED));
        };
    }

    public static String get(LibSetting setting) {
        return isSourceEnabled(setting.source) ?
            LibConfig.getInstance().getString(setting) :
            null;
    }

    public static Integer getInteger(LibSetting setting) {
        try {
            return Integer.parseInt(get(setting));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static boolean isEnabled(LibSetting setting) {
        return 1 == getInteger(setting);
    }

    public static List<LibSetting> getEnabledSettings() {
        return Arrays.stream(values())
            .filter(setting -> isSourceEnabled(setting.source))
            .toList();
    }

    public static String getPropertiesFileSuffix(LibSetting setting) {
        return setting.source.propertiesFileSuffix;
    }
}
