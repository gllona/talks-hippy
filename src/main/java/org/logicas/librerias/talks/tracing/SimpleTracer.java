package org.logicas.librerias.talks.tracing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TracerApi;
import org.logicas.librerias.talks.config.LibSetting;
import org.logicas.librerias.talks.engine.Usernames;
import org.logicas.librerias.talks.health.HealthService;

import java.util.Arrays;
import java.util.List;

public class SimpleTracer implements TracerApi {

    private static SimpleTracer instance;
    private static Logger logger = LogManager.getLogger(SimpleTracer.class);

    public static synchronized TracerApi getInstance() {
        if (instance == null) {
            instance = new SimpleTracer();
        }
        return instance;
    }

    @Override
    public void info(String session, String username, String messageId, String message) {
        if (shouldLog(session, username)) {
            logger.info(messageFor(session, username, messageId, message));
        }
    }

    @Override
    public void warn(String session, String username, String messageId, String message) {
        if (shouldLog(session, username)) {
            logger.warn(messageFor(session, username, messageId, message));
        }
    }

    @Override
    public void error(String session, String username, String messageId, String message) {
        if (shouldLog(session, username)) {
            logger.error(messageFor(session, username, messageId, message));
        }
    }

    @Override
    public void error(String session, String username, String messageId, String message, Throwable t) {
        if (shouldLog(session, username)) {
            logger.error(messageFor(session, username, messageId, message), t);
        }
    }

    private boolean shouldLog(String session, String username) {
        if (Usernames.isBot(username)) {
            return false;
        }
        List<String> sessionsToLog = getSessionsToLog();
        return ! HealthService.CHAT_ID.equals(session) && (
            session == null ||
            sessionsToLog.contains("*") ||
            sessionsToLog.contains(session)
        );
    }

    private String messageFor(String session, String username, String messageId, String message) {
        return String.format("[session=%s] [username=%s] [message=%s] %s", session, username, messageId, message);
    }

    private static List<String> getSessionsToLog() {
        return Arrays.asList(
            LibSetting.get(LibSetting.SESSIONS_TO_LOG).split(",")
        );
    }
}
