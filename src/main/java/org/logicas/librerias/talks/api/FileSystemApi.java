package org.logicas.librerias.talks.api;

import java.io.File;
import java.util.concurrent.TimeUnit;

public interface FileSystemApi {

    void deleteAfter(File file, int count, TimeUnit timeUnit);

    void deleteDownloadedAfter(File file, int count, TimeUnit timeUnit);
}
