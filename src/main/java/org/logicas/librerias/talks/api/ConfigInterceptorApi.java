package org.logicas.librerias.talks.api;

import java.util.function.Supplier;

public interface ConfigInterceptorApi {

    String getProperty(String settingName, Supplier<String> defaultSupplier);
}
