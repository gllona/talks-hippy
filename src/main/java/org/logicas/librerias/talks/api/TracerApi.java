package org.logicas.librerias.talks.api;

public interface TracerApi {

    void info(String session, String username, String messageId, String message);

    void warn(String session, String username, String messageId, String message);

    void error(String session, String username, String messageId, String message);

    void error(String session, String username, String messageId, String message, Throwable e);
}
