package org.logicas.librerias.talks.api;

import com.pengrad.telegrambot.TelegramBot;
import org.logicas.librerias.copersistance.CopersistenceConfig;
import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.DaoFactory;
import org.logicas.librerias.talks.config.LibSetting;
import org.logicas.librerias.talks.engine.*;
import org.logicas.librerias.talks.filesystem.DeleteQueue;
import org.logicas.librerias.talks.filesystem.FileSystem;
import org.logicas.librerias.talks.sender.v0.RatedMessageSender;
import org.logicas.librerias.talks.sender.v1.RatedPersistentMessageSender;
import org.logicas.librerias.talks.sender.v2.RatedPersistentMessageSenderV2Facade;
import org.logicas.librerias.talks.sender.v3.RatedPersistentMessageSenderV3Facade;
import org.logicas.librerias.talks.telegram.MessageSenderApi;
import org.logicas.librerias.talks.tracing.SimpleTracer;

import java.util.List;

public abstract class TalksConfiguration {

    private TelegramBot bot;
    private Dao dao;
    private MessageSenderApi messageSender;

    public abstract AppApi getApp();

    public abstract ModelApi getModel();

    public abstract List<TalkType> getTalkTypes();

    public abstract Integer getMatrixPort();

    public void setTelegramBot(TelegramBot bot) {
        this.bot = bot;
    }

    public TelegramBot getTelegramBot() {
        return bot;
    }

    public InputAdaptersManagerApi getInputAdaptersManager() {
        return InputAdaptersManager.getInstance(this);
    }

    public InteractionsManagerApi getInteractionsManager() {
        return InteractionsManager.getInstance(this);
    }

    public synchronized MessageSenderApi getMessageSender() {
        if (messageSender == null) {
            //messageSender = new DirectMessageSender(this);
            if (usePersistentMessageSender()) {
                int version = getPersistentMessageSenderVersion();
                if (version == 1) {
                    messageSender = new RatedPersistentMessageSender(this);
                } else if (version == 2) {
                    messageSender = new RatedPersistentMessageSenderV2Facade(this);
                } else if (version == 3) {
                    messageSender = new RatedPersistentMessageSenderV3Facade(this);
                }
                else {
                    throw new RuntimeException("Invalid persistent message sender version. Supported versions: 1, 2");
                }
            } else {
                messageSender = new RatedMessageSender(this);
            }
        }
        return messageSender;
    }

    public TracerApi getTracer() {
        return SimpleTracer.getInstance();
    }

    public TalkManagerApi getTalkManager() {
        return TalkManager.getInstance(this);
    }

    public TalkContext buildTalkContext(Talk talk) {
        //return new InMemoryTalkContext();
        return new PersistentTalkContext(getDao(), talk);
    }

    public FileSystemApi getFileSystem() {
        return FileSystem.Companion.getInstance(this);
    }

    public DeleteQueueApi getFileSystemDeleteQueue() {
        return DeleteQueue.INSTANCE;
    }

    public synchronized Dao getDao() {
        if (dao == null) {
            dao = DaoFactory.getInstance().createDao(
                buildCopersistanceConfig()
            );
        }
        return dao;
    }

    public CopersistenceConfig buildCopersistanceConfig() {
        return new CopersistenceConfig() {
            @Override
            public String getRdbms() {
                return LibSetting.get(LibSetting.DB_RDBMS);
            }
            @Override
            public String getHost() {
                return LibSetting.get(LibSetting.DB_HOST);
            }
            @Override
            public int getPort() {
                return Integer.parseInt(LibSetting.get(LibSetting.DB_PORT));
            }
            @Override
            public String getDatabase() {
                return LibSetting.get(LibSetting.DB_DATABASE);
            }
            @Override
            public String getUsername() {
                return LibSetting.get(LibSetting.DB_USERNAME);
            }
            @Override
            public String getPassword() {
                return LibSetting.get(LibSetting.DB_PASSWORD);
            }
            @Override
            public String getOptions() {
                return LibSetting.get(LibSetting.DB_OPTIONS);
            }
            @Override
            public int getConnectionPoolSize() {
                return Integer.parseInt(LibSetting.get(LibSetting.DB_CONNECTION_POOL_SIZE));
            }
            @Override
            public int getGetTimeoutInMillis() {
                return Integer.parseInt(LibSetting.get(LibSetting.DB_CONNECTION_POOL_TIMEOUT_IN_MILLIS));
            }
            @Override
            public int getConnTtlInMillis() {
                return Integer.parseInt(LibSetting.get(LibSetting.DB_CONNECTION_TTL_IN_MILLIS));
            }
            @Override
            public String getKeepAliveQuery() {
                return LibSetting.get(LibSetting.DB_KEEPALIVE_QUERY);
            }
        };
    }

    private boolean usePersistentMessageSender() {
        return LibSetting.getInteger(LibSetting.USE_PERSISTENT_MESSAGE_SENDER) != 0;
    }

    private int getPersistentMessageSenderVersion() {
        return LibSetting.getInteger(LibSetting.PERSISTENT_MESSAGE_SENDER_VERSION);
    }
}
