package org.logicas.librerias.talks.api;

import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public interface InteractionsManagerApi {

    void handle(TalkRequest request);

    void handle(TalkResponse response, String incomingMessageId);

    int getFlyingMessagesCount();

    void shutdown();
}
