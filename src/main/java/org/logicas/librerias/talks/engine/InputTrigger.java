package org.logicas.librerias.talks.engine;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.config.LibSetting;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

@AllArgsConstructor
@EqualsAndHashCode
public class InputTrigger {

    public enum GroupAction {
        JOIN,
        LEAVE
    }

    private enum ButtonType {
        REGULAR,
        CONTACT,
        LOCATION
    }

    @RequiredArgsConstructor
    @EqualsAndHashCode
    public class Button {
        public final Integer row;
        public final Integer column;
        public final ButtonType type;
        public String buttonText;
        public String getText() {
            if (buttonText == null) {
                return TextEncoder.unencode(text);
            } else {
                return TextEncoder.unencode(buttonText);
            }
        }
        public void setText(String text) {
            this.buttonText = text;
        }
    }

    private static Logger logger = LogManager.getLogger(InputTrigger.class);

    private MediaType mediaType;
    @Getter private String text;
    @Getter private boolean isRegex;
    private transient Pattern pattern;
    @Getter private GroupAction groupAction;
    @Getter private Button button;

    public static InputTrigger ofString(MediaType mediaType) {
        return new InputTrigger(
            mediaType,
            mediaType != MediaType.TEXT ? null : ".*",
            mediaType == MediaType.TEXT,
            mediaType != MediaType.TEXT ? null : createPattern(".*"),
            null,
            null
        );
    }

    public static InputTrigger ofString(String text) {
        return new InputTrigger(MediaType.TEXT, TextEncoder.normalizeAndEncode(text), false, null, null, null);
    }

    public static InputTrigger ofRegex(String pattern) {
        try {
            String normalizedPattern = TextEncoder.normalize(pattern);
            return new InputTrigger(MediaType.TEXT, normalizedPattern, true, createPattern(normalizedPattern), null, null);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Can not compile regex pattern [%s]", pattern), e);
        }
    }

    public static InputTrigger ofMedia(MediaType mediaType) {
        return new InputTrigger(mediaType, null, false, null, null, null);
    }

    private static Pattern createPattern(String text) {
        String fullPattern = "^(" + text + ")$";
        return Pattern.compile(fullPattern, Pattern.CASE_INSENSITIVE);
    }

    public static InputTrigger ofGroupAction(GroupAction action) {
        return new InputTrigger(null, null, false, null, action, null);
    }

    public InputTrigger asButton() {
        this.button = new Button(null, null, ButtonType.REGULAR);
        this.button.setText(text);
        return this;
    }

    public InputTrigger asButton(int row, int column) {
        this.button = new Button(row, column, ButtonType.REGULAR);
        return this;
    }

    public Optional<InputMatch> matches(TalkRequest call) {
        if (call.isJoinGroup() || call.isLeaveGroup()) {
            if (call.isJoinGroup() && groupAction == GroupAction.JOIN || call.isLeaveGroup() && groupAction == GroupAction.LEAVE) {
                return Optional.of(new InputMatch(null, null, null, groupAction));
            }
            return Optional.empty();
        }

        if (mediaType == null) {
            return Optional.empty();
        }

        if (mediaType != MediaType.TEXT) {
            return mediaType.extract(call.getMessage()) != null ?
                Optional.of(new InputMatch(mediaType, mediaType.extract(call.getMessage()), emptyList(), null)) :
                Optional.empty();
        }

        if (call.getText() == null) {
            return Optional.empty();
        }

        if (isRegex && pattern == null) {
            pattern = createPattern(text);
        }
        String messageText = (String) mediaType.extract(call.getMessage());
        messageText = removeGroupBotSuffix(messageText);
        if (! isRegex) {
            return text.equalsIgnoreCase(messageText) ?
                Optional.of(new InputMatch(MediaType.TEXT, null, singletonList(messageText), null)) :
                Optional.empty();
        }
        Matcher matcher = pattern.matcher(messageText);
        if (! matcher.find()) {
            return Optional.empty();
        }
        List<String> groupMatches = new ArrayList<>();
        for (int i = 1; i <= matcher.groupCount(); i++) {
            try {
                groupMatches.add(matcher.group(i));
            } catch (Exception e) {
                logger.warn(String.format(
                    "Could not match group %d in regex pattern [%s] for text [%s]",
                    i, text, call.getMessage()
                ));
                return Optional.empty();
            }
        }
        return Optional.of(new InputMatch(MediaType.TEXT, null, groupMatches, null));
    }

    private String removeGroupBotSuffix(String text) {
        String suffix = "@" + getBotName();
        if (! text.endsWith(suffix)) {
            return text;
        }
        return text.substring(0, text.lastIndexOf(suffix));
    }

    private String getBotName() {
        return LibSetting.get(LibSetting.BOT_NAME);
    }
}
