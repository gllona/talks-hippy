package org.logicas.librerias.talks.engine;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class Holder<T> {

    private T value;

    public static <U> Holder<U> of(U value) {
        return new Holder<>(value);
    }

    public T get() {
        return value;
    }

    public void set(T value) {
        this.value = value;
    }
}
