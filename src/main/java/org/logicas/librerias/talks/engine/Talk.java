package org.logicas.librerias.talks.engine;

import lombok.EqualsAndHashCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.InputAdapterApi;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.api.TracerApi;
import org.logicas.librerias.talks.config.InputAdapters;
import org.logicas.librerias.talks.config.LibSetting;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

@EqualsAndHashCode
public abstract class Talk implements Comparable<Talk> {

    protected static Logger logger = LogManager.getLogger(Talk.class);

    private static final List<InputHandler> PRESERVE_HANDLERS = singletonList(new InputHandler(null, emptyList(), "", ""));
    private static final Pattern SAFE_METHOD_REGEX_PATTERN = Pattern.compile(LibSetting.get(LibSetting.SAFE_METHOD_NAME_REGEX));
    private static List<String> talksClassNames = null;

    protected TalksConfiguration talksConfig;
    protected TracerApi tracer;
    protected TalkType type;
    protected TalkContext context;

    protected Talk(TalksConfiguration talksConfig, TalkType talkType) {
        this.talksConfig = talksConfig;
        this.type = talkType;
        tracer = talksConfig.getTracer();
        if (talksClassNames == null) {
            talksClassNames = talksConfig.getTalkTypes().stream()
                .map(TalkType::getTalkClass)
                .map(Class::getSimpleName)
                .collect(Collectors.toList());
        }
    }

    protected abstract Optional<InputHandler> getDefaultHandler();

    public TalkType getType() {
        return type;
    }

    public TalkContext getContext() {
        return context;
    }

    public void setContext(TalkContext context) {
        this.context = context;
    }

    protected synchronized void createContext() {
        if (context == null) {
            context = TalkContext.create(talksConfig, this);
        }
    }

    protected synchronized void createContext(String useContextFromThisClassName) {
        if (context == null) {
            context = TalkContext.create(talksConfig, useContextFromThisClassName, this);
        }
    }

    protected synchronized void createContext(Class<? extends Talk> useContextFromThisClass) {
        if (context == null) {
            context = TalkContext.create(talksConfig, useContextFromThisClass, this);
        }
    }

    public Boolean shouldLockDispatch(TalkRequest call) {
        Optional<InputMatch> match = context.match(call);
        if (match.isEmpty()) {
            return null;
        }
        String methodName = match.get().getHandler().getMethodName();
        return ! (isSafeMethod(methodName) || Objects.equals(LibSetting.getInteger(LibSetting.ALLOW_CONCURRENT_REQUESTS), 1));
    }

    private boolean isSafeMethod(String methodName) {
        return SAFE_METHOD_REGEX_PATTERN.matcher(methodName).matches();
    }

    public Optional<TalkResponse> dispatch(TalkRequest call) {
        if (call.getMessage() == null) {
            return dispatchWithNullMessage(call);
        }

        Optional<InputMatch> match = context.match(call);

        if (match.isEmpty()) {
            return Optional.empty();
            //TODO Telegram / process message :: new_chat_members=null, left_chat_member=null, new_chat_title='null', new_chat_photo=null, delete_chat_photo=null, group_chat_created=null, supergroup_chat_created=null, channel_chat_created=null, migrate_to_chat_id=null, migrate_from_chat_id=null, pinned_message=null, invoice=null, successful_payment=null, connected_website='null', passport_data=null, reply_markup=null
        }

        InterceptorResponse interceptorResponse = isStartCommand(call) ? null : tryIntercept(call);
        boolean followChain = interceptorResponse == null || interceptorResponse.isFollowChain();
        TalkResponse firstResponse = interceptorResponse == null ? null : interceptorResponse.getResponse();
        TalkResponse lastResponse = null;

        if (followChain) {
            long startedAt = System.currentTimeMillis();
            String chatId = call.getChatId();
            String username = call.getUsername();
            String messageId = call.getMessageId();
            String className = match.get().getHandler().getClassName();
            String methodName = match.get().getHandler().getMethodName();
            tracer.info(chatId, username, messageId, String.format("To dispatch TalkRequest to method %s::%s....", className, methodName));
            lastResponse = invokeMethod(match.get(), call);
            if (lastResponse == null) {
                if (Objects.equals(LibSetting.getInteger(LibSetting.LOG_NULL_TALK_RESPONSES), 1)) {
                    tracer.warn(chatId, username, messageId, String.format("A [null] response was received from method %s::%s....", className, methodName));
                }
            } else {
                long endedAt = System.currentTimeMillis();
                lastResponse.setTelemetry(new TalkResponse.Telemetry(startedAt, endedAt, className, methodName));
            }
            tracer.info(chatId, username, messageId, String.format("Finished invocation of method %s::%s....", className, methodName));
        }

        if (firstResponse != null && lastResponse != null) {
            sendResponse(firstResponse);
        }

        return Optional.ofNullable(lastResponse != null ? lastResponse : firstResponse);
    }

    private boolean isStartCommand(TalkRequest call) {
        return Objects.equals(call.getText(), LibSetting.get(LibSetting.START_MESSAGE));
    }

    private InterceptorResponse tryIntercept(TalkRequest call) {
        long startedAt = System.currentTimeMillis();
        InterceptorResponse interceptorResponse = doTryIntercept(call);

        if (interceptorResponse != null) {
            String chatId = call.getChatId();
            String username = call.getUsername();
            String messageId = call.getMessageId();
            TalkResponse response = interceptorResponse.getResponse();
            boolean followChain = interceptorResponse.isFollowChain();
            if (response != null) {
                long endedAt = System.currentTimeMillis();
                response.setTelemetry(new TalkResponse.Telemetry(startedAt, endedAt, interceptorResponse.getClassName(), interceptorResponse.getMethodName()));
                tracer.info(chatId, username, messageId, String.format("Intercepted TalkRequest [followChain=%s] and generated response [%s]", followChain, response.getFastText()));
            } else {
                tracer.info(chatId, username, messageId, String.format("Intercepted TalkRequest [followChain=%s] and generated no response", followChain));
            }
        }

        return interceptorResponse;
    }

    private InterceptorResponse doTryIntercept(TalkRequest call) {
        InterceptorResponse lastInterceptorResponse = null;
        List<Interceptor> interceptors = talksConfig.getTalkManager().talksForClass(Interceptor.class);
        for (Interceptor interceptor : interceptors) {
            if (! shouldIntercept(interceptor)) {
                continue;
            }
            InterceptorResponse interceptorResponse = interceptor.intercept(call);
            if (lastInterceptorResponse != null && interceptorResponse != null &&
                    interceptorResponse.getResponse() == null && interceptorResponse.isUseLastResponseIfResponseIsNull()) {
                interceptorResponse.setResponse(lastInterceptorResponse.getResponse());
            }
            lastInterceptorResponse = interceptorResponse;
            if (lastInterceptorResponse != null && ! lastInterceptorResponse.isFollowChain()) {
                break;
            }
        }
        return lastInterceptorResponse;
    }

    private boolean shouldIntercept(Interceptor interceptor) {
        return interceptor.interceptsAllTalks() || interceptor.interceptedTalks().contains(context.getTalk());
    }

    private TalkResponse invokeMethod(InputMatch match, TalkRequest call) {
        call.setMatch(match);
        try {
            Class<?> clazz = getTalkClassForMatch(match);
            if (clazz == null) {
                return null;
            }
            Method m = clazz.getDeclaredMethod(match.getHandler().getMethodName(), TalkRequest.class);
            Talk target = talksConfig.getTalkManager().talkForName(clazz.getSimpleName());
            TalkResponse response = (TalkResponse) m.invoke(target, call);
            return response;
        } catch (Exception e) {
            throw new RuntimeException(String.format(
                "Unable to invoke method [%s::%s] successfully", match.getHandler().getClassName(), match.getHandler().getMethodName()
            ), e);
        }
    }

    @SuppressWarnings("unchecked")
    private static Class<? extends Talk> getTalkClassForMatch(InputMatch match) {
        String className = match.getHandler().getClassName();
        try {
            Class<? extends Talk> clazz = (Class<? extends Talk>) Class.forName(className);
            return clazz;
        } catch (ClassCastException | ClassNotFoundException e) {
            return null;
        }
    }

    public void saveHandlers(String chatId, String username, String messageId, TalkResponse response) {
        if (response.getHandlers() == null) {
            tracer.warn(chatId, username, messageId, "A response with [null] handlers was received.");
        } else if (! response.getHandlers().equals(Talk.preserveHandlers())) {
            context.setHandlers(chatId, response.getHandlers());
        }
    }

    private Optional<TalkResponse> dispatchWithNullMessage(TalkRequest call) {
        if (call.getType() == InputAdapters.TELEGRAM) {
            //TODO process inline query and :: channel_post=null, edited_channel_post=null, inline_query=null, chosen_inline_result=null, callback_query=null, shipping_query=null, pre_checkout_query=null, poll=null, poll_answer=null
            logger.info(String.format("Received an update [%d] with null message: ", call.getUpdate().updateId()));
        } else {
            //TODO add cases for other input adapters
        }
        return Optional.empty();
    }

    public static List<InputHandler> preserveHandlers() {
        return PRESERVE_HANDLERS;
    }

    public static boolean hasPreserveHandlers(List<InputHandler> handlers) {
        return PRESERVE_HANDLERS.equals(handlers);
    }

    public String switchSessionInitiate(String session, boolean switchMainSession, TalkResponse nextTalkResponse) {
        return talksConfig.getTalkManager().switchSessionInitiate(session, switchMainSession, nextTalkResponse);
    }

    public Optional<TalkResponse> switchSessionComplete(String session, String code) {
        return talksConfig.getTalkManager().switchSessionComplete(session, code, context);
    }

    @SuppressWarnings("unchecked")
    protected <T extends Talk> T talkForClass(Class<T> clazz) {
        return (T) talksConfig.getTalkManager().talkForName(clazz.getSimpleName());
    }

    protected String className(Talk talk) {
        return talk == null ? null : talk.getClass().getCanonicalName();
    }

    private void sendResponse(TalkResponse response) {
        InputAdapters inputAdapterType = InputAdapters.forTalkResponse(response);
        InputAdapterApi inputAdapter = InputAdaptersManager.getInstance(talksConfig).get(inputAdapterType);
        inputAdapter.sendResponse(response);
    }

    @Override
    public int compareTo(Talk other) {   //TODO improve
        String thisClassName = this.getClass().getSimpleName();
        String otherClassName = other.getClass().getSimpleName();
        int thisClassIndex = talksClassNames.indexOf(thisClassName);
        int otherClassIndex = talksClassNames.indexOf(otherClassName);
        return thisClassIndex - otherClassIndex;
    }
}
