package org.logicas.librerias.talks.engine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.LibSetting;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public abstract class TalkContext {

    private static Logger logger = LogManager.getLogger(TalkContext.class);

    private static Map<String, TalkContext> contexts = new ConcurrentHashMap<>();

    protected TalkContext finalContext;
    protected Set<Talk> supportedTalks = new HashSet<>();

    public static TalkContext create(TalksConfiguration talksConfig, Talk talk) {
        TalkContext context = talksConfig.buildTalkContext(talk);
        context.supportedTalks.add(talk);
        contexts.put(talk.getClass().getSimpleName(), context);
        return context;
    }

    public static TalkContext create(TalksConfiguration talksConfig, String useContextFromThisClassName, Talk talk) {
        TalkContext context = create(talksConfig, talk);
        TalkContext finalContext = contexts.get(useContextFromThisClassName);
        if (finalContext == null) {
            throw new RuntimeException(String.format(
                "TalkContext for [%s] must be created before using it for [%s]",
                useContextFromThisClassName, talk.getClass().getSimpleName()
            ));
        }
        finalContext = finalContext.defer();
        finalContext.supportedTalks.add(talk);
        context.finalContext = finalContext;
        return context;
    }

    public static TalkContext create(TalksConfiguration talksConfig, Class<? extends Talk> useContextFromThisClass, Talk talk) {
        TalkContext context = create(talksConfig, talk);
        TalkContext finalContext = contexts.get(useContextFromThisClass.getSimpleName());
        if (finalContext == null) {
            throw new RuntimeException(String.format(
                "TalkContext for [%s] must be created before using it for [%s]",
                useContextFromThisClass.getSimpleName(), talk.getClass().getSimpleName()
            ));
        }
        finalContext = finalContext.defer();
        finalContext.supportedTalks.add(talk);
        context.finalContext = finalContext;
        return context;
    }

    protected abstract Talk getTalk();

    protected TalkContext defer() {
        return finalContext == null ? this : finalContext.defer();
    }

    public abstract void setGlobalHandlers(List<InputHandler> handlers);

    public abstract void setHandlers(String session, List<InputHandler> handlers);

    public abstract void setGlobal(String name, Object value);

    public abstract Object getGlobal(String name);

    public abstract void set(String session, String name, Object value);

    public abstract void remove(String session, String name);

    public abstract Object get(String session, String name);

    public abstract Map<String, Object> getAll(String session);

    public String getString(String session, String name) {
        Object value = get(session, name);
        return value == null ? null : value.toString();
    }

    public Long getLong(String session, String name) {
        Object value = get(session, name);
        return value == null ? null : Double.valueOf(value.toString()).longValue();
    }

    public Integer getInt(String session, String name) {
        Object value = get(session, name);
        return value == null ? null : Double.valueOf(value.toString()).intValue();
    }

    public Double getDouble(String session, String name) {
        Object value = get(session, name);
        return value == null ? null : Double.parseDouble(value.toString());
    }

    public abstract void reset(String session);

    public abstract void flush(String session);

    public void forgetPersonalData(String session) {
        reset(session);
        forget(session);
    }

    protected abstract void forget(String session);

    public abstract List<InputHandler> getGlobalHandlers();

    protected abstract List<InputHandler> getBoundedHandlers(String session);

    protected abstract void deleteBoundedHandlers(String session);

    public Optional<InputMatch> match(TalkRequest call) {
        List<InputHandler> globalHandlers = getGlobalHandlers();
        List<InputHandler> boundedHandlers = getBoundedHandlers(call.getChatId());

        boolean isStartMessage = call.getMessage() != null && call.getText() != null &&
            getStartMessage().equalsIgnoreCase(call.getText());

        Optional<InputMatch> match = match(call, globalHandlers, isStartMessage);
        if (match.isPresent()) {
            //deleteBoundedHandlers(call.getChatId()); //TODO check
            return match;
        }
        if (! isStartMessage && boundedHandlers != null) {
            match = match(call, boundedHandlers, true);
        }
        if (match.isPresent()) {
            return match;
        }
        match = match(call, globalHandlers, true);
        return match;
    }

    private Optional<InputMatch> match(TalkRequest call, Collection<InputHandler> handlers, boolean includeRegexTriggers) {
        for (InputHandler handler : handlers) {
            if (handler == null) {
                logger.warn(String.format(
                    "A [null] handler was found: [message=%s] [includeRegexTriggers=%b] [handlers=%s]",
                    call.getMessage(), includeRegexTriggers, handlers));
                continue;
            }
            if (! supportsHandler(handler)) {
                continue;
            }
            Optional<InputMatch> match = handler.matches(call, includeRegexTriggers);
            if (match.isPresent()) {
                return match;
            }
        }
        return Optional.empty();
    }

    private boolean supportsHandler(InputHandler handler) {
        String callerClassName = handler.getCallerClassName();
        if (callerClassName == null || getTalk() == null) {
            return supportsHandlerFallback(handler);
        }
        if (callerClassName.startsWith("/")) {
            callerClassName = callerClassName.substring(1);
        } else {
            callerClassName = LibSetting.get(LibSetting.TALK_PACKAGE_NAME) + "." + callerClassName;
        }
        return Objects.equals(getTalk().getClass().getCanonicalName(), callerClassName);
    }

    private boolean supportsHandlerFallback(InputHandler handler) {
        for (Talk talk : supportedTalks) {
            if (talk.getClass().getCanonicalName().endsWith(handler.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private String getStartMessage() {
        return LibSetting.get(LibSetting.START_MESSAGE);
    }

    public abstract String follow(String session);

    public abstract void switchSessionInitiate(String session, String code, boolean switchMainSession, TalkResponse nextTalkResponse);

    public abstract String generateSwitchSessionCode();

    public abstract boolean matchesSwitchSessionCode(String session, String code);

    public abstract List<String> getSessionIdsMatchingSwitchSessionCode(String code);

    public abstract boolean hasHandlersEntry(String session);

    public abstract void createHandlersEntry(String session);

    public abstract boolean isSwitchSessionCodeValid(String session, int timeoutInSeconds);

    public abstract void invalidateSwitchSessionCode(String session);

    public abstract TalkType getTalkType();

    public abstract TalkResponse getNextTalkResponse(String session);

    public abstract void clearNextTalkResponse(String session);

    public abstract void copyVarsFromSession(String session, String sourceSession);

    public abstract boolean isMainSession(String session);

    public abstract void pointTo(String session, String targetSession);

    public abstract void deleteCode(String session);
}
