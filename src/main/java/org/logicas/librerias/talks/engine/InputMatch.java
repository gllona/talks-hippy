package org.logicas.librerias.talks.engine;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.logicas.librerias.talks.engine.InputTrigger.GroupAction;

import java.util.List;

@Getter
@ToString
public class InputMatch {

    @Setter private InputHandler handler;
    private final MediaType mediaType;
    private final Object mediaMatch;
    @Setter private List<String> regexMatches;
    private final GroupAction groupAction;

    public InputMatch(MediaType mediaType, Object mediaMatch, List<String> regexMatches, GroupAction groupAction) {
        this.handler = null;
        this.mediaType = mediaType;
        this.mediaMatch = mediaMatch;
        this.regexMatches = regexMatches;
        this.groupAction = groupAction;
    }

    public boolean hasHandlerClass(Talk talk) {
        String talkClassName = talk.getClass().getCanonicalName();
        return talkClassName != null && talkClassName.endsWith(handler.getClassName());
    }
}
