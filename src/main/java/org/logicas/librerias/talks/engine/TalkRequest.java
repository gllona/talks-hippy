package org.logicas.librerias.talks.engine;

import com.pengrad.telegrambot.model.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.InputAdapterApi;
import org.logicas.librerias.talks.config.InputAdapters;
import org.logicas.librerias.talks.config.LibSetting;
import org.logicas.librerias.talks.matrix.MatrixReceiveMessageRequest;

import java.io.File;
import java.util.concurrent.TimeUnit;

@EqualsAndHashCode
@ToString
@Getter
public class TalkRequest {

    Logger logger = LogManager.getLogger(TalkRequest.class);

    private final InputAdapterApi inputAdapter;
    private final Update update;
    private final MatrixReceiveMessageRequest matrixRequest;
    private final Object rocketChatMessage; // Enterprise feature
    @Setter @EqualsAndHashCode.Exclude private InputMatch match;
    @EqualsAndHashCode.Exclude private Long flyTime;
    private boolean isJoinGroup;
    private boolean isLeaveGroup;
    private String overriddenText = null;

    public static TalkRequest ofMatrix(InputAdapterApi inputAdapter, MatrixReceiveMessageRequest matrixReceiveMessageRequest) {
        return new TalkRequest(
            inputAdapter,
            null,
            matrixReceiveMessageRequest,
            null
        );
    }

    public static TalkRequest ofHealth(InputAdapterApi inputAdapter, String chatId, String senderId, String text) {
        MatrixReceiveMessageRequest matrixReceiveMessageRequest = new MatrixReceiveMessageRequest();
        matrixReceiveMessageRequest.setRoomId(chatId);
        matrixReceiveMessageRequest.setSenderId(senderId);
        matrixReceiveMessageRequest.setEventType("m.room.message");
        matrixReceiveMessageRequest.setBody(text);
        matrixReceiveMessageRequest.setMessageType("m.text");

        return new TalkRequest(
            inputAdapter,
            null,
            matrixReceiveMessageRequest,
            null
        );
    }

    public TalkRequest(InputAdapterApi inputAdapter, Update update, MatrixReceiveMessageRequest matrixRequest, Object rocketChatMessage) {
        this.inputAdapter = inputAdapter;
        this.update = update;
        this.matrixRequest = matrixRequest;
        this.rocketChatMessage = rocketChatMessage; // Enterprise feature
    }

    public InputAdapters getType() {
        return InputAdapters.forTalkRequest(this);
    }

    public Update getUpdate() {
        return update;
    }

    public MatrixReceiveMessageRequest getMatrixRequest() {
        return matrixRequest;
    }

    public String getChatId() {
        if (getType() == InputAdapters.TELEGRAM) {
            return getTelegramChatId();
        } else if (getType() == InputAdapters.MATRIX) {
            return getMatrixChatId();
        } else {
            return null;
        }
    }

    public InputAdapters getChannel() {
        return InputAdapters.forTalkRequest(this);
    }

    public String getTelegramChatId() {
        if (update.message() != null) {
            return update.message().chat().id().toString();
        } else if (update.editedMessage() != null) {
            return update.editedMessage().chat().id().toString();
        } else if (update.channelPost() != null) {
            return update.channelPost().chat().id().toString();
        } else if (update.editedChannelPost() != null) {
            return update.editedChannelPost().chat().id().toString();
        }
        return null;
    }

    private String getMatrixChatId() {
        return matrixRequest.getRoomId();
    }

    public Object getMessage() {
        if (update != null) {
            return getTelegramMessage();
        } else if (matrixRequest != null) {
            return getMatrixMessage();
        } else {
            return null;
        }
    }

    public Message getTelegramMessage() {
        return update.message() != null ? update.message() : (
            update.editedMessage() != null ? update.editedMessage() : (
                update.channelPost() != null ? update.channelPost() :
                    update.editedChannelPost()
            ));
    }

    private MatrixReceiveMessageRequest.Message getMatrixMessage() {
        return matrixRequest.getMessage();
    }

    public String getMessageId() {
        if (getType() == InputAdapters.TELEGRAM) {
            Integer messageId = update.message().messageId();
            return messageId == null ? null : String.valueOf(messageId);
        } else if (getType() == InputAdapters.MATRIX) {
            return matrixRequest.getMessage().messageId();
        } else {
            return null;
        }
    }

    public String getUsername() {
        if (getType() == InputAdapters.TELEGRAM) {
            return "@" + update.message().chat().username();
        } else if (getType() == InputAdapters.MATRIX) {
            return matrixRequest.getSenderId();
        } else {
            return null;
        }
    }

    public String getUserFullName() {
        if (getType() == InputAdapters.TELEGRAM) {
            Chat chat = getTelegramMessage().chat();
            String fullName = ((chat.firstName() == null ? "" : chat.firstName().trim()) + " " +
                (chat.lastName() == null ? "" : chat.lastName().trim())).trim();
            return "".equals(fullName) ? null : fullName;
        } else if (getType() == InputAdapters.MATRIX) {
            return null;
        } else {
            return null;
        }
    }

    public String getUserShortName() {
        if (getType() == InputAdapters.TELEGRAM) {
            Chat chat = getTelegramMessage().chat();
            String shortName = chat.firstName() == null ? "" : chat.firstName().trim();
            return "".equals(shortName) ? null : shortName;
        } else if (getType() == InputAdapters.MATRIX) {
            return null;
        } else {
            return null;
        }
    }

    public String getText() {
        if (overriddenText != null) {
            return overriddenText;
        }

        if (getType() == InputAdapters.TELEGRAM) {
            return getTelegramMessage().text();
        } else if (getType() == InputAdapters.MATRIX) {
            return getMatrixMessage().text();
        } else {
            return null;
        }
    }

    public void overrideText(String text) {
        overriddenText = text;
    }

    public void setJoinGroup(boolean joinGroup) {
        isJoinGroup = joinGroup;
    }

    public void setLeaveGroup(boolean leaveGroup) {
        isLeaveGroup = leaveGroup;
    }

    public File getFile() {
        return getFile(null);
    }

    public File getFile(Holder<String> fileIdHolder) {
        if (getType() == InputAdapters.TELEGRAM) {
            return getTelegramFile(fileIdHolder);
        } else if (getType() == InputAdapters.MATRIX) {
            if (fileIdHolder != null) {
                fileIdHolder.set(getMatrixRequest().getMxcUri());
            }
            return getMatrixRequest().getMessage().getFile();
        } else {
            return null;
        }
    }

    public String getFileId() {
        if (getType() == InputAdapters.TELEGRAM) {
            return getTelegramFileId();
        } else if (getType() == InputAdapters.MATRIX) {
            return getMatrixRequest().getMessage().getMxcUri();
        } else {
            return null;
        }
    }

    private File getTelegramFile(Holder<String> fileIdHolder) {
        return switch (getMatch().getMediaType()) {
            case PHOTO -> getTelegramPhoto(fileIdHolder, true);
            case AUDIO -> getTelegramAudio(fileIdHolder, true);
            case VOICE -> getTelegramVoice(fileIdHolder, true);
            case VIDEO -> getTelegramVideo(fileIdHolder, true);
            case VIDEO_NOTE -> getTelegramVideoNote(fileIdHolder, true);
            case DOCUMENT -> getTelegramDocument(fileIdHolder, true);
            default -> null;
        };
    }

    private String getTelegramFileId() {
        Holder<String> fileIdHolder = new Holder<>();
        File nullFile = switch (getMatch().getMediaType()) {
            case PHOTO -> getTelegramPhoto(fileIdHolder, false);
            case AUDIO -> getTelegramAudio(fileIdHolder, false);
            case VOICE -> getTelegramVoice(fileIdHolder, false);
            case VIDEO -> getTelegramVideo(fileIdHolder, false);
            case VIDEO_NOTE -> getTelegramVideoNote(fileIdHolder, false);
            case DOCUMENT -> getTelegramDocument(fileIdHolder, false);
            default -> null;
        };
        return fileIdHolder.get();
    }

    private File getTelegramPhoto(Holder<String> fileIdHolder, boolean downloadFile) {
        PhotoSize[] photos = (PhotoSize[]) getMatch().getMediaMatch();
        //String fileId = photos[0].fileId();   // most lo-res photo
        String fileId = photos[photos.length - 1].fileId();   // most hi-res photo
        if (fileIdHolder != null) {
            fileIdHolder.set(fileId);
        }
        return downloadFile ? getInputAdapter().getFile(fileId) : null;
    }

    private File getTelegramAudio(Holder<String> fileIdHolder, boolean downloadFile) {
        Audio audio = (Audio) getMatch().getMediaMatch();
        String fileId = audio.fileId();
        if (fileIdHolder != null) {
            fileIdHolder.set(fileId);
        }
        return downloadFile ? getInputAdapter().getFile(fileId) : null;
    }

    private File getTelegramVoice(Holder<String> fileIdHolder, boolean downloadFile) {
        Voice voice = (Voice) getMatch().getMediaMatch();
        String fileId = voice.fileId();
        if (fileIdHolder != null) {
            fileIdHolder.set(fileId);
        }
        return downloadFile ? getInputAdapter().getFile(fileId) : null;
    }

    private File getTelegramVideo(Holder<String> fileIdHolder, boolean downloadFile) {
        Video video = (Video) getMatch().getMediaMatch();
        String fileId = video.fileId();
        if (fileIdHolder != null) {
            fileIdHolder.set(fileId);
        }
        return downloadFile ? getInputAdapter().getFile(fileId) : null;
    }

    private File getTelegramVideoNote(Holder<String> fileIdHolder, boolean downloadFile) {
        VideoNote videoNote = (VideoNote) getMatch().getMediaMatch();
        String fileId = videoNote.fileId();
        if (fileIdHolder != null) {
            fileIdHolder.set(fileId);
        }
        return downloadFile ? getInputAdapter().getFile(fileId) : null;
    }

    private File getTelegramDocument(Holder<String> fileIdHolder, boolean downloadFile) {
        Document document = (Document) getMatch().getMediaMatch();
        String fileId = document.fileId();
        if (fileIdHolder != null) {
            fileIdHolder.set(fileId);
        }
        return downloadFile ? getInputAdapter().getFile(fileId) : null;
    }

    public boolean isFromBot() {
        return Usernames.isBot(getUsername());
    }

    public Long getFlyTime() {
        return flyTime;
    }

    public void fly(int duration, TimeUnit timeUnit) {
        if (isFlyingEnabled()) {
            this.flyTime = Math.min(TimeUnit.MILLISECONDS.convert(duration, timeUnit), getFlyingMessagesMaxTTL());
        }
    }

    public void fly() {
        if (isFlyingEnabled()) {
            this.flyTime = Math.min(getFlyingMessagesDefaultTTL(), getFlyingMessagesMaxTTL());
        }
    }

    private static long getFlyingMessagesDefaultTTL() {
        return TimeUnit.MILLISECONDS.convert(LibSetting.getInteger(LibSetting.FLYING_MESSAGES_DURATION_IN_MINUTES), TimeUnit.MINUTES);
    }

    private static long getFlyingMessagesMaxTTL() {
        return TimeUnit.MILLISECONDS.convert(LibSetting.getInteger(LibSetting.FLYING_MESSAGES_MAX_DURATION_IN_MINUTES), TimeUnit.MINUTES);
    }

    private static boolean isFlyingEnabled() {
        return getFlyingMessagesDefaultTTL() != -1;
    }
}
