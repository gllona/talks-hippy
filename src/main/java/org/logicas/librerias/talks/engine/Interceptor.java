package org.logicas.librerias.talks.engine;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;

import java.util.LinkedList;
import java.util.List;

public abstract class Interceptor extends Talk {

    public static final String METHOD_INTERCEPT = "intercept";

    protected static final List<Talk> TALKS_ALL = new LinkedList<>();

    protected Interceptor(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
    }

    public abstract List<Talk> interceptedTalks();

    public abstract InterceptorResponse intercept(TalkRequest request);

    public boolean interceptsAllTalks() {
        return interceptedTalks() == TALKS_ALL;
    }
}
