package org.logicas.librerias.talks.engine;

import lombok.Data;

@Data
public class TalkLocation {

    private final float latitude;
    private final float longitude;
}
