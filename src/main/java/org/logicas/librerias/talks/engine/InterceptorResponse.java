package org.logicas.librerias.talks.engine;

import lombok.*;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString
public class InterceptorResponse {

    @Setter TalkResponse response;
    boolean followChain;
    boolean useLastResponseIfResponseIsNull;
    String className;
    String methodName;

    public static InterceptorResponse of(Talk talk, TalkResponse response, boolean followChain) {
        return new InterceptorResponse(
            response,
            followChain,
            false,
            talk.getClass().getSimpleName(),
            Interceptor.METHOD_INTERCEPT
        );
    }

    public static InterceptorResponse ofLastResponse(Talk talk, boolean followChain) {
        return new InterceptorResponse(
            null,
            followChain,
            true,
            talk.getClass().getSimpleName(),
            Interceptor.METHOD_INTERCEPT
        );
    }
}
