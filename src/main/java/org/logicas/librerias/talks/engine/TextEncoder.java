package org.logicas.librerias.talks.engine;

import com.vdurmont.emoji.EmojiParser;
import org.logicas.librerias.talks.config.LibSetting;

import java.text.Normalizer;
import java.text.Normalizer.Form;

public class TextEncoder {

    public static String normalize(String text) {
        if (text == null) {
            return null;
        }
        return Normalizer.normalize(text, Form.NFC);
    }

    public static String normalizeAndEncode(String text) {
        if (text == null) {
            return null;
        }

        String normalizedText = normalize(text);

        return shouldEncodeEmoji() ?
            EmojiParser.parseToHtmlHexadecimal(text) :
            normalizedText;
    }

    public static String unencode(String text) {
        if (text == null) {
            return null;
        }

        return EmojiParser.parseToUnicode(text);
    }

    private static boolean shouldEncodeEmoji() {
        return LibSetting.getInteger(LibSetting.ENCODE_EMOJI) != 0;
    }
}
