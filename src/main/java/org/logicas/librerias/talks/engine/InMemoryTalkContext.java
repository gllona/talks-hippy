package org.logicas.librerias.talks.engine;

import org.logicas.librerias.talks.api.TalkType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.util.Collections.emptyList;

public class InMemoryTalkContext extends TalkContext {

    private static class SessionContext {
        private static Object NULL = new Object();
        private Map<String, Object> vars = new ConcurrentHashMap<>();
        public void set(String name, Object value) {
            vars.put(name, value == null ? NULL : value);
        }
        public void remove(String name) {
            vars.remove(name);
        }
        public Object get(String name) {
            Object value = vars.get(name);
            return value == NULL ? null : value;
        }
        public Map<String, Object> getAll() { return vars; }
        public void clear() {
            vars.clear();
        }
    }

    private SessionContext globalVars = new SessionContext();
    private Map<String, SessionContext> boundedVars = new ConcurrentHashMap<>();
    private Collection<InputHandler> globalHandlers = new ConcurrentLinkedQueue<>();
    private Map<String, Collection<InputHandler>> boundedHandlers = new ConcurrentHashMap<>();

    @Override
    protected Talk getTalk() {
        return null;
    }

    protected InMemoryTalkContext defer() {
        return finalContext == null ? this : (InMemoryTalkContext) finalContext.defer();
    }

    @Override
    public List<InputHandler> getGlobalHandlers() {
        return new ArrayList<>(defer().globalHandlers);
    }

    @Override
    protected List<InputHandler> getBoundedHandlers(String session) {
        Collection<InputHandler> handlers = defer().boundedHandlers.get(session);
        return new ArrayList<>(handlers == null ?
            emptyList() :
            new ArrayList<>(handlers)
        );
    }

    @Override
    protected void deleteBoundedHandlers(String session) {
        defer().boundedHandlers.get(session).clear();
    }

    @Override
    public void setGlobalHandlers(List<InputHandler> handlers) {
        InMemoryTalkContext finalContext = defer();
        finalContext.globalHandlers.clear();
        finalContext.globalHandlers.addAll(handlers);
    }

    @Override
    public void setHandlers(String session, List<InputHandler> handlers) {
        if (handlers == null || Talk.preserveHandlers().equals(handlers)) {
            return;
        }
        InMemoryTalkContext finalContext = defer();
        finalContext.boundedHandlers.putIfAbsent(session, new ConcurrentLinkedQueue<>());
        finalContext.boundedHandlers.get(session).clear();
        finalContext.boundedHandlers.get(session).addAll(handlers);
    }

    @Override
    public void setGlobal(String name, Object value) {
        defer().globalVars.set(name, value);
    }

    @Override
    public Object getGlobal(String name) {
        return defer().globalVars.get(name);
    }

    @Override
    public void set(String session, String name, Object value) {
        InMemoryTalkContext finalContext = defer();
        finalContext.boundedVars.putIfAbsent(session, new SessionContext());
        finalContext.boundedVars.get(session).set(name, value);
    }

    @Override
    public void remove(String session, String name) {
        InMemoryTalkContext finalContext = defer();
        finalContext.boundedVars.putIfAbsent(session, new SessionContext());
        finalContext.boundedVars.get(session).remove(name);
    }

    @Override
    public Object get(String session, String name) {
        InMemoryTalkContext finalContext = defer();
        finalContext.boundedVars.putIfAbsent(session, new SessionContext());
        return finalContext.boundedVars.get(session).get(name);
    }

    @Override
    public Map<String, Object> getAll(String session) {
        InMemoryTalkContext finalContext = defer();
        finalContext.boundedVars.putIfAbsent(session, new SessionContext());
        return finalContext.boundedVars.get(session).getAll();
    }

    @Override
    public void reset(String session) {
        InMemoryTalkContext finalContext = defer();
        if (finalContext.boundedVars.get(session) != null) {
            finalContext.boundedVars.get(session).clear();
        }
        if (finalContext.boundedHandlers.get(session) != null) {
            finalContext.boundedHandlers.get(session).clear();
        }
    }

    @Override
    public void flush(String session) {
        // do nothing
    }

    @Override
    protected void forget(String session) {
        throw new UnsupportedOperationException("Forgetting the user context is not implemented for in memory context");
    }

    @Override
    public String follow(String session) {
        throw new UnsupportedOperationException("follow session is not available for InMemoryTalkContext");
    }

    @Override
    public void switchSessionInitiate(String session, String code, boolean switchMainSession, TalkResponse nextTalkResponse) {
        throw new UnsupportedOperationException("switchSession is not available for InMemoryTalkContext");
    }

    @Override
    public String generateSwitchSessionCode() {
        throw new UnsupportedOperationException("generateSwitchSessionCode is not available for InMemoryTalkContext");
    }

    @Override
    public boolean matchesSwitchSessionCode(String session, String code) {
        throw new UnsupportedOperationException("matchesSwitchSessionCode is not available for InMemoryTalkContext");
    }

    @Override
    public List<String> getSessionIdsMatchingSwitchSessionCode(String code) {
        throw new UnsupportedOperationException("getSessionIdsMatchingCode is not available for InMemoryTalkContext");
    }

    public boolean hasHandlersEntry(String session) {
        throw new UnsupportedOperationException("hasHandlersEntry is not available for InMemoryTalkContext");
    }

    public void createHandlersEntry(String session) {
        throw new UnsupportedOperationException("createHandlersEntry is not available for InMemoryTalkContext");
    }

    @Override
    public boolean isSwitchSessionCodeValid(String session, int timeoutInSeconds) {
        throw new UnsupportedOperationException("isSwitchSessionCodeValid is not available for InMemoryTalkContext");
    }

    @Override
    public void invalidateSwitchSessionCode(String session) {
        throw new UnsupportedOperationException("invalidateSwitchSessionCode is not available for InMemoryTalkContext");
    }

    @Override
    public TalkType getTalkType() {
        throw new UnsupportedOperationException("getTalkType is not available for InMemoryTalkContext");
    }

    @Override
    public TalkResponse getNextTalkResponse(String session) {
        throw new UnsupportedOperationException("getNextTalkResponse is not available for InMemoryTalkContext");
    }

    @Override
    public void clearNextTalkResponse(String session) {
        throw new UnsupportedOperationException("clearNextTalkResponse is not available for InMemoryTalkContext");
    }

    @Override
    public void copyVarsFromSession(String session, String sourceSession) {
        throw new UnsupportedOperationException("copyVarsFromSession is not available for InMemoryTalkContext");
    }

    @Override
    public boolean isMainSession(String session) {
        throw new UnsupportedOperationException("isMainSession is not available for InMemoryTalkContext");
    }

    @Override
    public void pointTo(String session, String targetSession) {
        throw new UnsupportedOperationException("pointTo is not available for InMemoryTalkContext");
    }

    @Override
    public void deleteCode(String session) {
        throw new UnsupportedOperationException("deleteCode is not available for InMemoryTalkContext");
    }
}
