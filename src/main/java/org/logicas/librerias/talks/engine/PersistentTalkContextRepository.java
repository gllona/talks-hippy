package org.logicas.librerias.talks.engine;

import com.google.common.collect.Streams;
import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.QueryBuilder;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.config.LibSetting;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;

public class PersistentTalkContextRepository extends SingleKeyEntityRepository<PersistentTalkContextEntry> {

    private static PersistentTalkContextRepository instance;

    private Dao dao;

    private PersistentTalkContextRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized PersistentTalkContextRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new PersistentTalkContextRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "talks_contexts";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return PersistentTalkContextEntry.FIELDS;
    }

    @Override
    public String idFieldName() {
        return PersistentTalkContextEntry.FIELD_ID;
    }

    public PersistentTalkContextEntry readHandlersEntry(TalkType talkType, String session) {
        PersistentTalkContextEntry entry = readOne(String.format(
            "session_id = '%s' AND talk_name = '%s' AND type = '%s'",
            QueryBuilder.escapeSql(session), talkType.getName(), PersistentTalkContextEntry.TYPE_INPUT_HANDLERS
        ));
        return entry;
    }

    public List<InputHandler> readHandlers(TalkType talkType, String session) {
        PersistentTalkContextEntry entry = readHandlersEntry(talkType, session);
        if (entry == null) {
            return emptyList();
        }
        List<InputHandler> handlers = entry.getInputHandlers();
        return handlers == null ? emptyList() : handlers;
    }

    public void deleteHandlersEntry(TalkType talkType, String session) {
        deleteByCondition(String.format(
            "session_id = '%s' AND talk_name = '%s' AND type = '%s'",
            QueryBuilder.escapeSql(session), talkType.getName(), PersistentTalkContextEntry.TYPE_INPUT_HANDLERS
        ));
    }

    public void deleteHandlersEntry(String session) {
        deleteByCondition(String.format(
            "session_id = '%s' AND type = '%s'",
            QueryBuilder.escapeSql(session), PersistentTalkContextEntry.TYPE_INPUT_HANDLERS
        ));
    }

    public PersistentTalkContextEntry readVarsEntry(TalkType talkType, String session) {
        PersistentTalkContextEntry entry = readOne(String.format(
            "session_id = '%s' AND talk_name = '%s' AND type = '%s'",
            QueryBuilder.escapeSql(session), talkType.getName(), PersistentTalkContextEntry.TYPE_VARS
        ));
        return entry;
    }

    public void deleteVarsEntry(TalkType talkType, String session) {
        deleteByCondition(String.format(
            "session_id = '%s' AND talk_name = '%s' AND type = '%s'",
            QueryBuilder.escapeSql(session), talkType.getName(), PersistentTalkContextEntry.TYPE_VARS
        ));
    }

    public void deleteVarsEntry(String session) {
        deleteByCondition(String.format(
            "session_id = '%s' AND type = '%s'",
            QueryBuilder.escapeSql(session), PersistentTalkContextEntry.TYPE_VARS
        ));
    }

    public List<String> readPointingSessionIds(String targetSessionId, boolean includeTargetSessionId) {
        List<Long> contextEntryIds = readByCondition(String.format("%s = '%s' and %s = '%s'",
                PersistentTalkContextEntry.FIELD_SESSION_ID, QueryBuilder.escapeSql(targetSessionId),
                PersistentTalkContextEntry.FIELD_TYPE, PersistentTalkContextEntry.TYPE_INPUT_HANDLERS)).stream()
            .map(PersistentTalkContextEntry::getId)
            .toList();

        if (contextEntryIds.isEmpty()) {
            return includeTargetSessionId ?
                List.of(targetSessionId) :
                List.of();
        }

        String contextEntryIdsStr = contextEntryIds.stream().map(Object::toString).collect(Collectors.joining(", "));

        List<String> pointingSessionIds = readByCondition(String.format("%s IN (%s)", PersistentTalkContextEntry.FIELD_POINTS_TO, contextEntryIdsStr)).stream()
            .map(PersistentTalkContextEntry::getSessionId)
            .filter(Objects::nonNull)
            .distinct()
            .toList();

        return includeTargetSessionId ?
            Streams.concat(pointingSessionIds.stream(), Stream.of(targetSessionId)).toList() :
            pointingSessionIds;
    }

    public List<String> readPointedSessionIds(String sourceSessionId, boolean includeSourceSessionId) {
        List<Long> contextEntryIds = readByCondition(String.format("%s = '%s' AND %s = '%s'",
                PersistentTalkContextEntry.FIELD_SESSION_ID, QueryBuilder.escapeSql(sourceSessionId),
                PersistentTalkContextEntry.FIELD_TYPE, PersistentTalkContextEntry.TYPE_INPUT_HANDLERS)).stream()
            .map(PersistentTalkContextEntry::getPointsTo)
            .filter(Objects::nonNull)
            .toList();

        if (contextEntryIds.isEmpty()) {
            return includeSourceSessionId ?
                List.of(sourceSessionId) :
                List.of();
        }

        List<String> pointedSessionIds = readByIds(contextEntryIds).stream()
            .map(PersistentTalkContextEntry::getSessionId)
            .distinct()
            .toList();

        return includeSourceSessionId ?
            Streams.concat(pointedSessionIds.stream(), Stream.of(sourceSessionId)).toList() :
            pointedSessionIds;
    }

    public List<String> readRelatedSessionIds(String sessionId) {
        return Streams.concat(
            readPointingSessionIds(sessionId, true).stream(),
            readPointedSessionIds(sessionId, true).stream()
        ).distinct()
         .toList();
    }

    public List<String> readSessionIdsBySwitchSessionCode(String code) {
        return readByCondition("code = '" + code + "'").stream()
            .map(PersistentTalkContextEntry::getSessionId)
            .distinct()
            .toList();
    }

    public boolean codeIsPresent(String code) {
        return countAll("code = '" + code + "'") > 0;
    }

    public List<PersistentTalkContextEntry> getEntriesWithOldCodes() {
        int timeoutInSeconds = LibSetting.getInteger(LibSetting.SWITCH_SESSION_TIMEOUT_IN_SECONDS);
        return readByCondition("code IS NOT NULL AND COALESCE(code_set_at, NOW()) < DATE_SUB(NOW(), INTERVAL " + timeoutInSeconds + " SECOND)");
    }
}
