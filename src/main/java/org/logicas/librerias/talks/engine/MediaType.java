package org.logicas.librerias.talks.engine;

import com.pengrad.telegrambot.model.Message;
import lombok.RequiredArgsConstructor;
import org.logicas.librerias.talks.matrix.MatrixReceiveMessageRequest;

import java.io.File;
import java.util.function.Supplier;

@RequiredArgsConstructor
public enum MediaType {

    ANIMATION(Message::animation, m -> null),
    AUDIO(Message::audio, MediaType::extractMatrixFile),
    CONTACT(Message::contact, m -> null),
    DOCUMENT(Message::document, MediaType::extractMatrixFile),
    LOCATION(MediaType::extractTelegramLocation, MediaType::extractMatrixLocation),
    PHOTO(Message::photo, MediaType::extractMatrixFile),
    TEXT(MediaType::extractTelegramText, MediaType::extractMatrixText),
    VIDEO(Message::video, MediaType::extractMatrixFile),
    VIDEO_NOTE(Message::videoNote, MediaType::extractMatrixFile),
    VOICE(Message::voice, MediaType::extractMatrixFile),
    ACTION_DELETE_MESSAGE(null, null);
    //GAME(Message::game),
    //STICKER(Message::sticker),
    //CAPTION(Message::caption),
    //VENUE(Message::venue),
    //POLL(Message::poll),

    @FunctionalInterface
    public interface TelegramTypeExtractor {
        Object extract(Message message);
    }

    @FunctionalInterface
    public interface MatrixTypeExtractor {
        Object extract(MatrixReceiveMessageRequest.Message message);
    }

    private final TelegramTypeExtractor telegramExtractor;
    private final MatrixTypeExtractor matrixExtractor;

    public Object extract(Object message) {
        if (message instanceof Message) {
            return telegramExtractor.extract((Message) message);
        } else if (message instanceof MatrixReceiveMessageRequest.Message) {
            return matrixExtractor.extract((MatrixReceiveMessageRequest.Message) message);
        } else {
            return null;
        }
    }

    //TODO get editedText() if text() not available
    private static String extractTelegramText(Message message) {
        return TextEncoder.normalizeAndEncode(message.text());
    }

    private static String extractMatrixText(MatrixReceiveMessageRequest.Message message) {
        return TextEncoder.normalizeAndEncode(message.text());
    }

    private static TalkLocation extractTelegramLocation(Message message) {
        return message.location() == null ?
            null :
            new TalkLocation(
                message.location().latitude(),
                message.location().longitude()
            );
    }

    private static TalkLocation extractMatrixLocation(MatrixReceiveMessageRequest.Message message) {
        String geoUri = message.geoUri(); // geo:<latitude>,<longitude>
        if (geoUri == null) {
            return null;
        }
        String coordinates = geoUri.substring(geoUri.indexOf(":"));
        int separatorPos = coordinates.indexOf(",");
        int suffixPos = coordinates.indexOf(";");
        if (suffixPos != -1) {
            coordinates = coordinates.substring(0, suffixPos);
        }
        String latitude = coordinates.substring(1, separatorPos);
        String longitude = coordinates.substring(separatorPos + 1);
        return new TalkLocation(
            Float.parseFloat(latitude),
            Float.parseFloat(longitude)
        );
    }

    private static Supplier<File> extractMatrixFile(MatrixReceiveMessageRequest.Message message) {
        return message.getBytes() == null ? null : message::getFile;
    }
}
