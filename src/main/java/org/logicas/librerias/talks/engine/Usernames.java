package org.logicas.librerias.talks.engine;

import org.logicas.librerias.talks.config.LibSetting;
import spark.utils.StringUtils;

import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Usernames {

    private static final String WHATSAPP_PHONE_BOT_USERNAME_REGEX = buildPhoneNumberRegex(LibSetting.WHATSAPP_PHONE_NUMBER, "whatsapp");

    public static final String TELEGRAM_PHONE_BOT_USERNAME_REGEX = buildPhoneNumberRegex(LibSetting.TELEGRAM_PHONE_NUMBER, "telegram");

    private static final String TELEGRAM_BOT_USERNAME_REGEX = joinRegexs(
        TELEGRAM_PHONE_BOT_USERNAME_REGEX,
        getTelegramBotName()
    );

    private static final String SIGNAL_PHONE_BOT_USERNAME_REGEX = buildPhoneNumberRegex(LibSetting.SIGNAL_PHONE_NUMBER, "signal");

    private static final String MATRIX_BOT_USERNAME_REGEX = patternQuote(LibSetting.get(LibSetting.MATRIX_BOT_USERNAME));

    private static final String MATRIX_BOT_USERNAMES_REGEX = String.format("^(%s)$",
        joinRegexs(
            WHATSAPP_PHONE_BOT_USERNAME_REGEX,
            TELEGRAM_PHONE_BOT_USERNAME_REGEX,
            SIGNAL_PHONE_BOT_USERNAME_REGEX,
            MATRIX_BOT_USERNAME_REGEX
        )
    );

    private static final String BOT_USERNAME_REGEX = String.format("^(%s)$",
        joinRegexs(
            WHATSAPP_PHONE_BOT_USERNAME_REGEX,
            TELEGRAM_BOT_USERNAME_REGEX,
            SIGNAL_PHONE_BOT_USERNAME_REGEX,
            MATRIX_BOT_USERNAMES_REGEX
        )
    );

    public static boolean isBot(String username) {
        return username != null && username.matches(BOT_USERNAME_REGEX);
    }

    public static boolean isMatrixBot(String username) {
        return username != null && username.matches(MATRIX_BOT_USERNAME_REGEX);
    }

    private static String buildPhoneNumberRegex(LibSetting phoneNumberSetting, String channel) {
        String phoneNumber = LibSetting.get(phoneNumberSetting);
        if (StringUtils.isEmpty(phoneNumber)) {
            return null;
        }
        phoneNumber = phoneNumber
            .substring(1)
            .replace(" ", "[0-9]?");
        return String.format("@%s_%s:%s",
            channel,
            phoneNumber,
            patternQuote(LibSetting.get(LibSetting.MATRIX_SERVER))
        );
    }

    private static String getTelegramBotName() {
        String botName = LibSetting.get(LibSetting.BOT_NAME);
        return botName == null ? null : "@" + botName;
    }

    private static String joinRegexs(String... parts) {
        return Arrays.stream(parts)
            .filter(Objects::nonNull)
            .collect(Collectors.joining("|"));
    }

    private static String patternQuote(String text) {
        return text == null ? null : Pattern.quote(text);
    }
}
