package org.logicas.librerias.talks.engine;

import com.google.gson.reflect.TypeToken;
import lombok.*;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Setter
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class PersistentTalkContextEntry extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_SESSION_ID = "session_id";
    public static final String FIELD_TALK_NAME = "talk_name";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_CONTENT = "content";
    public static final String FIELD_POINTS_TO = "points_to";
    public static final String FIELD_LEVEL = "level";
    public static final String FIELD_CODE = "code";
    public static final String FIELD_CODE_SET_AT = "code_set_at";
    public static final String FIELD_NEXT_TALK_RESPONSE = "next_talk_response";

    public static final String TYPE_INPUT_HANDLERS = "handlers";
    public static final String TYPE_VARS = "vars";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_SESSION_ID,
        FIELD_TALK_NAME,
        FIELD_TYPE,
        FIELD_CONTENT,
        FIELD_POINTS_TO,
        FIELD_LEVEL,
        FIELD_CODE,
        FIELD_CODE_SET_AT,
        FIELD_NEXT_TALK_RESPONSE
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    String sessionId;
    private String talkName;
    String type;
    private String content;
    private Long pointsTo;
    private Integer level;
    private String code;
    private LocalDateTime codeSetAt;
    private String nextTalkResponse;

    public PersistentTalkContextEntry(String sessionId, String talkName, String type) {
        this.sessionId = sessionId;
        this.talkName = talkName;
        this.type = type;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getTalkName() {
        return talkName;
    }

    public String getType() {
        return type;
    }

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(sessionId, FIELD_SESSION_ID, params);
        conditionalAddToParams(talkName, FIELD_TALK_NAME, params);
        conditionalAddToParams(type, FIELD_TYPE, params);
        conditionalAddToParams(content, FIELD_CONTENT, params);
        conditionalAddToParams(pointsTo == null ? NULL_TAG : pointsTo, FIELD_POINTS_TO, params);
        conditionalAddToParams(level, FIELD_LEVEL, params);
        conditionalAddToParams(code == null ? NULL_TAG : code, FIELD_CODE, params);
        conditionalAddToParams(codeSetAt == null ? NULL_TAG : codeSetAt, FIELD_CODE_SET_AT, params);
        conditionalAddToParams(nextTalkResponse == null ? NULL_TAG : nextTalkResponse, FIELD_NEXT_TALK_RESPONSE, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }

    public void setInputHandlers(Collection<InputHandler> inputHandlers) {
        type = TYPE_INPUT_HANDLERS;
        content = gson.toJson(inputHandlers);
    }

    public void setVars(Map<String, Object> vars) {
        type = TYPE_VARS;
        content = gson.toJson(vars);
    }

    public List<InputHandler> getInputHandlers() {
        if (! TYPE_INPUT_HANDLERS.equals(type)) {
            throw new RuntimeException("getInputHandlers() can not be invoked for PersistentTalkContextEntry with type " + type);
        }
        Type type = new TypeToken<List<InputHandler>>(){}.getType();
        List<InputHandler> handlers = gson.fromJson(content, type);
        return handlers;
    }

    public Map<String, Object> getVars() {
        if (! TYPE_VARS.equals(type)) {
            throw new RuntimeException("getVars() can not be invoked for PersistentTalkContextEntry with type " + type);
        }
        if (content == null) {
            return new HashMap<>();
        }
        Type type = new TypeToken<HashMap<String, Object>>(){}.getType();
        Map<String, Object> vars = gson.fromJson(content, type);
        return vars;
    }

    public boolean isNew() {
        return id == null;
    }

    public Long getPointsTo() {
        return pointsTo;
    }

    public void setPointsTo(Long pointsTo) {
        this.pointsTo = pointsTo;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getCodeSetAt() {
        return codeSetAt;
    }

    public void setCodeSetAt(LocalDateTime codeSetAt) {
        this.codeSetAt = codeSetAt;
    }

    public TalkResponse getNextTalkResponseTyped() {
        return gson.fromJson(nextTalkResponse, TalkResponse.class);
    }

    public void setNextTalkResponseTyped(TalkResponse talkResponse) {
        nextTalkResponse = talkResponse == null ? null : gson.toJson(talkResponse);
    }

    public PersistentTalkContextEntry withTalkClassName(String talkClassName) {
        return new PersistentTalkContextEntry(
            id,
            sessionId,
            talkClassName,
            type,
            content,
            pointsTo,
            level,
            code,
            codeSetAt,
            nextTalkResponse
        );
    }
}
