package org.logicas.librerias.talks.engine;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.logicas.librerias.talks.config.LibSetting;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class InputHandler {

    // not more used after changing handlers definitions in Talks from method to string
    @FunctionalInterface
    public interface TalkMethod {
        TalkResponse work(TalkRequest call, List<String> matches);
    }

    @Getter private String callerClassName;
    @Getter private List<InputTrigger> triggers;
    private String className;
    @Getter private String methodName;

    public static InputHandler of(String callerTalkClassName, String classAndMethod, InputTrigger... triggers) {
        String callerClassName = stripTalkClassName(callerTalkClassName);
        int separatorIndex = classAndMethod.indexOf("::");
        if (separatorIndex == -1) {
            throw new RuntimeException("Class&Method separator not found in " + classAndMethod);
        }
        String className = classAndMethod.substring(0, separatorIndex);
        String methodName = classAndMethod.substring(separatorIndex + 2);
        if (className.isEmpty() || methodName.isEmpty()) {
            throw new RuntimeException("Invalid Class&Method: " + classAndMethod);
        }
        return new InputHandler(callerClassName, asList(triggers), className, methodName);
    }

    public Optional<InputMatch> matches(TalkRequest call, boolean includeRegexTriggers) {
        for (InputTrigger trigger : triggers) {
            if (! includeRegexTriggers && trigger.isRegex()) {
                continue;
            }
            Optional<InputMatch> match = trigger.matches(call);
            if (match.isPresent()) {
                match.get().setHandler(this);
                return match;
            }
        }
        return Optional.empty();
    }

    public String getClassName() {
        return className.startsWith("/") ?
            className.substring(1) :
            getTalkPackageName() + "." + className;
    }

    private static String stripTalkClassName(String className) {
        if (className == null) {
            return null;
        }
        String basePackage = getTalkPackageName() + ".";
        if (className.startsWith(basePackage)) {
            className = className.substring(basePackage.length());
        } else {
            className = "/" + className;
        }
        return className;
    }

    private static String getTalkPackageName() {
        return LibSetting.get(LibSetting.TALK_PACKAGE_NAME);
    }
}
