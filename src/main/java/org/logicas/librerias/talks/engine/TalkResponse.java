package org.logicas.librerias.talks.engine;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.*;
import lombok.*;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.InputAdapters;
import org.logicas.librerias.talks.config.LibSetting;
import org.logicas.librerias.talks.matrix.MatrixResponse;
import org.logicas.librerias.talks.sender.v3.RatedPersistentMessageSenderV3Facade;
import org.logicas.librerias.talks.telegram.ButtonDecorator;

import java.io.File;
import java.net.URI;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.logicas.librerias.talks.engine.SendPriority.LOW;
import static org.logicas.librerias.talks.engine.SendPriority.NORMAL;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Getter
public class TalkResponse {

    @EqualsAndHashCode
    static class FileBasedPayload {
        private URI fileURI;
        private String fileId;
        public FileBasedPayload(File file) {
            this.fileURI = file.toURI();
            this.fileId = null;
        }
        public FileBasedPayload(String fileId) {
            this.fileId = fileId;
            this.fileURI = null;
        }
        public File file() {
            if (fileURI == null) {
                return null;
            }
            try {
                return Paths.get(fileURI).toFile();
            } catch (Exception e) {
                return null;
            }
        }
        public String fileId() {
            return fileId;
        }
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    static class ContactPayload {
        public String phoneNumber;
        public String firstName;
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    static class LocationPayload {
        public float latitude;
        public float longitude;
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    @Getter
    public static class Telemetry {
        private long startedAtInMillis;
        private long endedAtInMillis;
        private String className;
        private String methodName;
        public long getDurationInMillis() {
            return endedAtInMillis - startedAtInMillis;
        }
        public String getClassAndMethod() {
            return className + "::" + methodName;
        }
    }

    public static final TalkResponse EMPTY = TalkResponse.ofText("IMPOSSIBLE::CALLER::CLASS::NAME", "", "");

    private static final int MAX_TEXT_MESSAGE_LENGTH = 4000;   // 4096 by Telegram specs
    private static final Pattern REGEX_COMMAND = Pattern.compile("(?<!(<|/|[a-zA-Z0-9]))/[a-zA-Z0-9_]+\\b");

    private static final Cache<String, TalkResponse> messageIdResponsesCache = CacheBuilder.newBuilder().expireAfterWrite(LibSetting.getInteger(LibSetting.RESPONSES_MESSAGE_ID_CACHE_TTL_IN_SECONDS), TimeUnit.SECONDS).build();

    private String uuid;
    private transient String callerTalkClassName;
    private MediaType mediaType;
    private String text;
    private boolean isHtml;
    private FileBasedPayload fileBasedPayload;
    @Getter(value = AccessLevel.NONE) private ContactPayload contactPayload;
    @Getter(value = AccessLevel.NONE) private LocationPayload locationPayload;
    private String chatId;
    private InputAdapters channel;
    private String mediaResourceId;
    @EqualsAndHashCode.Exclude @ToString.Exclude private String messageId; // used for actions on messages
    private List<InputHandler> handlers;
    private SendPriority priority = NORMAL;
    private Long flyTime;
    private Boolean showActions;
    private transient AbstractSendRequest<?> telegramSendRequest;
    private final transient List<Consumer<AbstractSendRequest<?>>> decorators = new ArrayList<>();
    private transient MatrixResponse.Message matrixMessage;
    private transient Object rocketChatMessage; // Enterprise feature
    private transient Supplier<String> textSupplier;
    private transient Supplier<File> fileSupplier;
    private transient Function<Exception, TalkResponse> exceptionMapper;
    private transient CompletableFuture<String> messageIdGetter;
    private transient SynchronousQueue<String> messageIdGetterHolder;
    @EqualsAndHashCode.Exclude private TalkResponse nextResponse;
    @Setter private transient Telemetry telemetry;

    public String getUuid() {
        return uuid;
    }

    public InputAdapters getType() {
        return InputAdapters.forTalkResponse(this);
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public Long getFlyTime() {
        return flyTime;
    }

    public SendPriority getPriority() {
        return priority;
    }

    public Function<Exception, TalkResponse> getExceptionMapper() {
        return exceptionMapper;
    }

    public SynchronousQueue<String> getMessageIdGetterHolder() {
        return messageIdGetterHolder;
    }

    public TalkResponse getNextResponse() {
        return nextResponse;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public String getText() {
        if (text == null && textSupplier != null) {
            try {
                text = textSupplier.get();
            } catch (Exception e) { // receiver will get null and react to it
            }
        }
        return TextEncoder.unencode(text);
    }

    public String getFastText() {
        return text == null && textSupplier != null ?
            "[text-not-yet-calculated]" :
            TextEncoder.unencode(text);
    }

    public String getMediaResourceId() {
        return mediaResourceId;
    }

    private File getFile() {
        if (fileBasedPayload == null && fileSupplier != null) {
            try {
                File file = fileSupplier.get();
                fileBasedPayload = new FileBasedPayload(file);
            } catch (Exception ignored) { // receiver will get null and react to it
            }
        }
        return fileBasedPayload == null ? null : fileBasedPayload.file();
    }

    private String getFileId() {
        return fileBasedPayload == null ? null : fileBasedPayload.fileId();
    }

    private static FileBasedPayload buildFileBasedPayload(File file) {
        return file == null ? null : new FileBasedPayload(file);
    }

    private static FileBasedPayload buildFileBasedPayload(String fileId) {
        return fileId == null ? null : new FileBasedPayload(fileId);
    }

    public static TalkResponse ofAnimation(String callerTalkClassName, String chatId, File file) {
        return ofAnimation(callerTalkClassName, chatId, null, file);
    }

    public static TalkResponse ofAudio(String callerTalkClassName, String chatId, File file) {
        return ofAudio(callerTalkClassName, chatId, null, file);
    }

    public static TalkResponse ofVoice(String callerTalkClassName, String chatId, File file) {
        return ofVoice(callerTalkClassName, chatId, null, file);
    }

    public static TalkResponse ofPhoto(String callerTalkClassName, String chatId, File file) {
        return ofPhoto(callerTalkClassName, chatId, null, file);
    }

    public static TalkResponse ofVideo(String callerTalkClassName, String chatId, File file) {
        return ofVideo(callerTalkClassName, chatId, null, file);
    }

    public static TalkResponse ofVideoNote(String callerTalkClassName, String chatId, File file) {
        return ofVideoNote(callerTalkClassName, chatId, null, file);
    }

    public static TalkResponse ofDocument(String callerTalkClassName, String chatId, File file) {
        return ofDocument(callerTalkClassName, chatId, null, file);
    }

    public static TalkResponse ofAnimationId(String callerTalkClassName, String chatId, String fileId) {
        return ofAnimationId(callerTalkClassName, chatId, null, fileId);
    }

    public static TalkResponse ofAudioId(String callerTalkClassName, String chatId, String fileId) {
        return ofAudioId(callerTalkClassName, chatId, null, fileId);
    }

    public static TalkResponse ofVoiceId(String callerTalkClassName, String chatId, String fileId) {
        return ofVoiceId(callerTalkClassName, chatId, null, fileId);
    }

    public static TalkResponse ofPhotoId(String callerTalkClassName, String chatId, String fileId) {
        return ofPhotoId(callerTalkClassName, chatId, null, fileId);
    }

    public static TalkResponse ofVideoId(String callerTalkClassName, String chatId, String fileId) {
        return ofVideoId(callerTalkClassName, chatId, null, fileId);
    }

    public static TalkResponse ofVideoNoteId(String callerTalkClassName, String chatId, String fileId) {
        return ofVideoNoteId(callerTalkClassName, chatId, null, fileId);
    }

    public static TalkResponse ofDocumentId(String callerTalkClassName, String chatId, String fileId) {
        return ofDocumentId(callerTalkClassName, chatId, null, fileId);
    }

    public static TalkResponse ofContact(String callerTalkClassName, String chatId, String phoneNumber, String firstName) {
        return ofContact(callerTalkClassName, chatId, null, phoneNumber, firstName);
    }

    public static TalkResponse ofLocation(String callerTalkClassName, String chatId, float latitude, float longitude) {
        return ofLocation(callerTalkClassName, chatId, null, latitude, longitude);
    }

    public static TalkResponse ofText(String callerTalkClassName, String chatId, String text) {
        return ofText(callerTalkClassName, chatId, null, text);
    }

    public static TalkResponse ofHtml(String callerTalkClassName, String chatId, String text) {
        return ofHtml(callerTalkClassName, chatId, null, text);
    }

    public static TalkResponse ofFlying(String chatId, String messageId) {
        return ofFlying(chatId, null, messageId);
    }

    public static TalkResponse ofAnimation(String callerTalkClassName, String chatId, InputAdapters channel, File file) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.ANIMATION, null, false,
            buildFileBasedPayload(file), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofAudio(String callerTalkClassName, String chatId, InputAdapters channel, File file) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.AUDIO, null, false,
            buildFileBasedPayload(file), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofVoice(String callerTalkClassName, String chatId, InputAdapters channel, File file) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.VOICE, null, false,
            buildFileBasedPayload(file), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofPhoto(String callerTalkClassName, String chatId, InputAdapters channel, File file) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.PHOTO, null, false,
            buildFileBasedPayload(file), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofVideo(String callerTalkClassName, String chatId, InputAdapters channel, File file) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.VIDEO, null, false,
            buildFileBasedPayload(file), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofVideoNote(String callerTalkClassName, String chatId, InputAdapters channel, File file) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.VIDEO_NOTE, null, false,
            buildFileBasedPayload(file), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofDocument(String callerTalkClassName, String chatId, InputAdapters channel, File file) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.DOCUMENT, null, false,
            buildFileBasedPayload(file), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofAnimationId(String callerTalkClassName, String chatId, InputAdapters channel, String fileId) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.ANIMATION, null, false,
            buildFileBasedPayload(fileId), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofAudioId(String callerTalkClassName, String chatId, InputAdapters channel, String fileId) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.AUDIO, null, false,
            buildFileBasedPayload(fileId), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofVoiceId(String callerTalkClassName, String chatId, InputAdapters channel, String fileId) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.VOICE, null, false,
            buildFileBasedPayload(fileId), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofPhotoId(String callerTalkClassName, String chatId, InputAdapters channel, String fileId) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.PHOTO, null, false,
            buildFileBasedPayload(fileId), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofVideoId(String callerTalkClassName, String chatId, InputAdapters channel, String fileId) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.VIDEO, null, false,
            buildFileBasedPayload(fileId), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofVideoNoteId(String callerTalkClassName, String chatId, InputAdapters channel, String fileId) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.VIDEO_NOTE, null, false,
            buildFileBasedPayload(fileId), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofDocumentId(String callerTalkClassName, String chatId, InputAdapters channel, String fileId) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.DOCUMENT, null, false,
            buildFileBasedPayload(fileId), null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofContact(String callerTalkClassName, String chatId, InputAdapters channel, String phoneNumber, String firstName) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.CONTACT, null, false,
            null, new ContactPayload(phoneNumber, firstName), null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofLocation(String callerTalkClassName, String chatId, InputAdapters channel, float latitude, float longitude) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.LOCATION, null, false,
            null, null, new LocationPayload(latitude, longitude),
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofText(String callerTalkClassName, String chatId, InputAdapters channel, String text) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.TEXT, TextEncoder.normalizeAndEncode(text), false,
            null, null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofHtml(String callerTalkClassName, String chatId, InputAdapters channel, String text) {
        return new TalkResponse(newUuid(), callerTalkClassName, MediaType.TEXT, TextEncoder.normalize(text), true,
            null, null, null,
            chatId, channel, null, null, new ArrayList<>(), NORMAL, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public static TalkResponse ofFlying(String chatId, InputAdapters channel, String messageId) {
        return new TalkResponse(newUuid(), null, MediaType.ACTION_DELETE_MESSAGE, null, false,
            null, null, null,
            chatId, channel, null, messageId, new ArrayList<>(), LOW, null, null, null,
            null, null, null, null, null,
            null, null, null, null);
    }

    public TalkResponse with(InputHandler handler) {
        return withIf(handler, true);
    }

    public TalkResponse withIf(InputHandler handler, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(handler);
        }
        return this;
    }

    public TalkResponse withMediaResourceId(String mediaResourceId) {
        return withMediaResourceIdIf(mediaResourceId, true);
    }

    public TalkResponse withMediaResourceIdIf(String mediaResourceId, boolean doAddResourceId) {
        this.mediaResourceId = mediaResourceId;
        return this;
    }

    public TalkResponse withString(String classAndMethod, String trigger) {
        return withStringIf(classAndMethod, trigger, true);
    }

    public TalkResponse withStringIf(String classAndMethod, String trigger, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(InputHandler.of(callerTalkClassName, classAndMethod, InputTrigger.ofString(trigger)));
        }
        return this;
    }

    public TalkResponse withButton(String classAndMethod, String trigger) {
        return withButtonIf(classAndMethod, trigger, true);
    }

    public TalkResponse withButton(String classAndMethod, String trigger, int row, int column) {
        return withButtonIf(classAndMethod, trigger, row, column, true);
    }

    public TalkResponse withButtonIf(String classAndMethod, String trigger, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(InputHandler.of(callerTalkClassName, classAndMethod, InputTrigger.ofString(trigger).asButton()));
        }
        return this;
    }

    public TalkResponse withButtonIf(String classAndMethod, String trigger, int row, int column, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(InputHandler.of(callerTalkClassName, classAndMethod, InputTrigger.ofString(trigger).asButton(row, column)));
        }
        return this;
    }

    public TalkResponse withRegex(String classAndMethod, String trigger) {
        return withRegexIf(classAndMethod, trigger, true);
    }

    public TalkResponse withRegexIf(String classAndMethod, String trigger, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(InputHandler.of(callerTalkClassName, classAndMethod, InputTrigger.ofRegex(trigger)));
        }
        return this;
    }

    public TalkResponse withMedia(String classAndMethod, MediaType mediaType) {
        return withMediaIf(classAndMethod, mediaType, true);
    }

    public TalkResponse withMediaIf(String classAndMethod, MediaType mediaType, boolean doAddHandler) {
        if (doAddHandler) {
            handlers.add(InputHandler.of(callerTalkClassName, classAndMethod, InputTrigger.ofMedia(mediaType)));
        }
        return this;
    }

    public TalkResponse withHandlers(List<InputHandler> handlers) {
        this.handlers.addAll(handlers);
        return this;
    }

    public TalkResponse withPreserveHandlers() {
        this.handlers = Talk.preserveHandlers();
        return this;
    }

    public TalkResponse withEmptyHandlers() {
        return withHandlers(emptyList());
    }

    public TalkResponse withLowPriority() {
        this.priority = LOW;
        return this;
    }

    public TalkResponse withFlying(int duration, TimeUnit timeUnit) {
        if (isFlyingEnabled()) {
            this.flyTime = Math.min(TimeUnit.MILLISECONDS.convert(duration, timeUnit), getFlyingMessagesMaxTTL());
        }
        return this;
    }

    public TalkResponse withFlying() {
        if (isFlyingEnabled()) {
            this.flyTime = Math.min(getFlyingMessagesDefaultTTL(), getFlyingMessagesMaxTTL());
        }
        return this;
    }

    public TalkResponse withUnboundedFlying(int duration, TimeUnit timeUnit) {
        if (isFlyingEnabled()) {
            this.flyTime = TimeUnit.MILLISECONDS.convert(duration, timeUnit);
        }
        return this;
    }

    public TalkResponse withUnboundedFlying() {
        if (isFlyingEnabled()) {
            this.flyTime = getFlyingMessagesDefaultTTL();
        }
        return this;
    }

    public TalkResponse withHints() {
        this.showActions = true;
        return this;
    }

    public TalkResponse withNoHints() {
        this.showActions = false;
        return this;
    }

    public TalkResponse withTextSupplier(Supplier<String> textSupplier) {
        this.textSupplier = textSupplier;
        return this;
    }

    public TalkResponse withFileSupplier(Supplier<File> fileSupplier) {
        this.fileSupplier = fileSupplier;
        return this;
    }

    public TalkResponse withExceptionMapper(Function<Exception, TalkResponse> mapper) {
        this.exceptionMapper = mapper;
        return this;
    }

    public TalkResponse withMessageIdGetter() {
        this.messageIdGetterHolder = new SynchronousQueue<>();
        this.messageIdGetter = CompletableFuture.supplyAsync(
            () -> {
                try {
                    return this.messageIdGetterHolder.take();
                } catch (InterruptedException e) {
                    return null;
                }
            }
            //TODO add single thread executor for single thread bots
        );
        messageIdResponsesCache.put(uuid, this);
        return this;
    }

    public String getMessageId() {
        if (messageIdGetter == null) {
            return messageId;
        }
        try {
            return this.messageIdGetter.get();
        } catch (ExecutionException | InterruptedException e) {
            return null;
        }
    }

    public String getMessageId(int timeout, TimeUnit timeUnit) {
        if (messageIdGetter == null) {
            return messageId;
        }
        try {
            return this.messageIdGetter.get(timeout, timeUnit);
        } catch (ExecutionException | TimeoutException | InterruptedException e) {
            return null;
        }
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
        if (messageIdGetterHolder != null) {
            getMessageSender().setTalkResponseMessageId(this, messageId);
        }
    }

    private RatedPersistentMessageSenderV3Facade getMessageSender() {
        if (LibSetting.getInteger(LibSetting.PERSISTENT_MESSAGE_SENDER_VERSION) != 3) {
            throw new UnsupportedOperationException("Persistent message sender must be set to version 3");
        }

        return RatedPersistentMessageSenderV3Facade.getInstance();
    }

    public static TalkResponse getInHeapResponse(String uuid) {
        return messageIdResponsesCache.getIfPresent(uuid);
    }

    public TalkResponse next(TalkResponse response) {
        this.nextResponse = response;
        return this;
    }

    public TalkResponse getLastResponseInChain() {
        TalkResponse lastResponse = this;
        while (lastResponse.nextResponse != null) {
            lastResponse = lastResponse.nextResponse;
        }
        return lastResponse;
    }

    public void addDecoration(Consumer<AbstractSendRequest<?>> decorator) {
        decorators.add(decorator);
    }

    public List<TalkResponse> getFinalResponses() {
        if (mediaType == MediaType.TEXT && text != null && isHtml) {
            TalkResponse response = new TalkResponse(
                uuid,
                callerTalkClassName,
                MediaType.TEXT,
                text,
                true,
                null,
                null,
                null,
                chatId,
                channel,
                null,
                null,
                getHandlers(),
                priority,
                flyTime,
                showActions,
                null,
                null,
                null,
                null,
                null,
                null,
                messageIdGetter,
                messageIdGetterHolder,
                null,
                null
            );
            decorateButtons(response);
            return singletonList(response);
        }

        if (mediaType != MediaType.TEXT || text == null) {
            return singletonList(this);
        }

        List<TalkResponse> finalResponses = new ArrayList<>();
        List<String> chunks = splitText(text); //TODO check if text split should be done for InputAdapters different than Telegram
        for (String chunk : chunks) {
            finalResponses.add(new TalkResponse(
                uuid, // same UUID shared among several TalkResponses, so TalkResponse messageId will be returned for one of them only
                callerTalkClassName,
                MediaType.TEXT,
                chunk,
                false,
                null,
                null,
                null,
                chatId,
                channel,
                null,
                null,
                getHandlers(),
                priority,
                flyTime,
                showActions,
                null,
                null,
                null,
                null,
                null,
                null,
                messageIdGetter,
                messageIdGetterHolder,
                null,
                null
            ));
        }
        for (int i = 0; i < finalResponses.size(); i++) {
            TalkResponse finalResponse = finalResponses.get(i);
            if (i == finalResponses.size() - 1) {
                decorateButtons(finalResponse);
            }
        }

        return finalResponses;
    }

    public AbstractSendRequest<?> getTelegramSendRequest() {
        if (telegramSendRequest == null) {
            telegramSendRequest = buildTelegramSendRequest(null);
        }
        return telegramSendRequest;
    }

    public AbstractSendRequest<?> getTelegramSendRequest(Supplier<String> resourceFileIdSupplier) {
        if (telegramSendRequest == null || resourceFileIdSupplier != null) {
            telegramSendRequest = buildTelegramSendRequest(resourceFileIdSupplier);
        }
        return telegramSendRequest;
    }

    private AbstractSendRequest<?> buildTelegramSendRequest(Supplier<String> resourceFileIdSupplier) {
        AbstractSendRequest<?> telegramSendRequest = switch (mediaType) {
            case TEXT -> {
                String unencodedText = TextEncoder.unencode(getText());
                yield isHtml ?
                    doBuildTelegramTextSendRequest(unencodedText, text -> new SendMessage(chatId, unencodedText).parseMode(ParseMode.HTML)) :
                    doBuildTelegramTextSendRequest(unencodedText, text -> new SendMessage(chatId, unencodedText));
            }
            case CONTACT -> new SendContact(chatId, contactPayload.phoneNumber, contactPayload.firstName);
            case LOCATION -> new SendLocation(chatId, locationPayload.latitude, locationPayload.longitude);
            case ANIMATION, AUDIO, VOICE, PHOTO, VIDEO, VIDEO_NOTE, DOCUMENT ->
                buildTelegramMediaSendRequest(resourceFileIdSupplier);
            default -> null;
        };

        if (telegramSendRequest != null) {
            for (Consumer<AbstractSendRequest<?>> decorator : decorators) {
                decorator.accept(telegramSendRequest);
            }
        }

        return telegramSendRequest;
    }

    private AbstractSendRequest<?> buildTelegramMediaSendRequest(Supplier<String> resourceFileIdSupplier) {
        String resourceFileId = null;
        if (getFileId() == null && resourceFileIdSupplier != null) {
            resourceFileId = resourceFileIdSupplier.get();
        }
        return getFileId() == null && resourceFileId == null ?
            buildTelegramFileMediaSendRequest() :
            buildTelegramFileIdMediaSendRequest(resourceFileId);
    }

    private AbstractSendRequest<?> buildTelegramFileMediaSendRequest() {
        return switch (mediaType) {
            case ANIMATION -> doBuildTelegramMediaSendRequest(getFile(), file -> new SendAnimation(chatId, file));
            case AUDIO -> doBuildTelegramMediaSendRequest(getFile(), file -> new SendAudio(chatId, file));
            case VOICE -> doBuildTelegramMediaSendRequest(getFile(), file -> new SendVoice(chatId, file));
            case PHOTO -> doBuildTelegramMediaSendRequest(getFile(), file -> new SendPhoto(chatId, file));
            case VIDEO -> doBuildTelegramMediaSendRequest(getFile(), file -> new SendVideo(chatId, file));
            case VIDEO_NOTE -> doBuildTelegramMediaSendRequest(getFile(), file -> new SendVideoNote(chatId, file));
            case DOCUMENT -> doBuildTelegramMediaSendRequest(getFile(), file -> new SendDocument(chatId, file));
            default -> null;
        };
    }

    private AbstractSendRequest<?> buildTelegramFileIdMediaSendRequest(String resourceFileId) {
        final String finalFileId = getFileId() != null ? getFileId() : resourceFileId;
        return switch (mediaType) {
            case ANIMATION -> doBuildTelegramMediaSendRequest(finalFileId, fileId -> new SendAnimation(chatId, fileId));
            case AUDIO -> doBuildTelegramMediaSendRequest(finalFileId, fileId -> new SendAudio(chatId, fileId));
            case VOICE -> doBuildTelegramMediaSendRequest(finalFileId, fileId -> new SendVoice(chatId, fileId));
            case PHOTO -> doBuildTelegramMediaSendRequest(finalFileId, fileId -> new SendPhoto(chatId, fileId));
            case VIDEO -> doBuildTelegramMediaSendRequest(finalFileId, fileId -> new SendVideo(chatId, fileId));
            case VIDEO_NOTE -> doBuildTelegramMediaSendRequest(finalFileId, fileId -> new SendVideoNote(chatId, fileId));
            case DOCUMENT -> doBuildTelegramMediaSendRequest(finalFileId, fileId -> new SendDocument(chatId, fileId));
            default -> null;
        };
    }

    private AbstractSendRequest<?> doBuildTelegramTextSendRequest(String text, Function<String, AbstractSendRequest<?>> telegramSendRequestSupplier) {
        return text == null ? null : telegramSendRequestSupplier.apply(text);
    }

    private AbstractSendRequest<?> doBuildTelegramMediaSendRequest(File file, Function<File, AbstractSendRequest<?>> telegramSendRequestSupplier) {
        return file == null ? null : telegramSendRequestSupplier.apply(file);
    }

    private AbstractSendRequest<?> doBuildTelegramMediaSendRequest(String fileId, Function<String, AbstractSendRequest<?>> telegramSendRequestSupplier) {
        return fileId == null ? null : telegramSendRequestSupplier.apply(fileId);
    }

    public DeleteMessage getTelegramDeleteMessageRequest() {
        if (messageId == null) {
            throw new RuntimeException("messageId not set for DeleteMessage action");
        }

        int messageIdToFly = Integer.parseInt(messageId);
        return new DeleteMessage(chatId, messageIdToFly);
    }

    public MatrixResponse.Message getMatrixMessage(TalksConfiguration talksConfig, long dbId) {
        if (matrixMessage == null) {
            matrixMessage = buildMatrixMessage(talksConfig, dbId, null);
        }
        return matrixMessage;
    }

    public MatrixResponse.Message getMatrixMessage(TalksConfiguration talksConfig, long dbId, Supplier<String> resourceFileIdSupplier) {
        if (matrixMessage == null || resourceFileIdSupplier != null) {
            matrixMessage = buildMatrixMessage(talksConfig, dbId, resourceFileIdSupplier);
        }
        return matrixMessage;
    }

    private MatrixResponse.Message buildMatrixMessage(TalksConfiguration talksConfig, long dbId, Supplier<String> resourceFileIdSupplier) {
        switch (mediaType) {
            case TEXT:
                if (isHtml) {
                    return MatrixResponse.Message.ofHtml(chatId, channel, getText(), parseTextCommands(), getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
                } else {
                    return MatrixResponse.Message.ofText(chatId, channel, getText(), parseTextCommands(), getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
                }
            case LOCATION:
                return MatrixResponse.Message.ofLocation(chatId, channel, locationPayload.latitude, locationPayload.longitude, getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
            case AUDIO:
            case VOICE:
            case PHOTO:
            case VIDEO:
            case VIDEO_NOTE:
            case DOCUMENT:
                return buildMatrixMediaMessage(talksConfig, dbId, resourceFileIdSupplier);
            case ACTION_DELETE_MESSAGE:
                return buildMatrixDeleteMessage(dbId);
            default:
                return null;
        }
    }

    private MatrixResponse.Message buildMatrixMediaMessage(TalksConfiguration talksConfig, long dbId, Supplier<String> resourceFileIdSupplier) {
        String resourceFileId = null;
        if (getFileId() == null && resourceFileIdSupplier != null) {
            resourceFileId = resourceFileIdSupplier.get();
        }
        return getFileId() == null && resourceFileId == null ?
            buildMatrixFileMediaMessage(talksConfig, dbId) :
            buildMatrixFileIdMediaMessage(talksConfig, dbId, resourceFileId);
    }

    private MatrixResponse.Message buildMatrixFileMediaMessage(TalksConfiguration talksConfig, long dbId) {
        return switch (mediaType) {
            case AUDIO, VOICE ->
                MatrixResponse.Message.ofAudio(chatId, channel, getFile(), getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
            case PHOTO, ANIMATION ->
                MatrixResponse.Message.ofImage(chatId, channel, getFile(), getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
            case VIDEO, VIDEO_NOTE ->
                MatrixResponse.Message.ofVideo(chatId, channel, getFile(), getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
            case DOCUMENT ->
                MatrixResponse.Message.ofFile(chatId, channel, getFile(), getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
            default -> null;
        };
    }

    private MatrixResponse.Message buildMatrixFileIdMediaMessage(TalksConfiguration talksConfig, long dbId, String resourceFileId) {
        final String finalFileId = getFileId() != null ? getFileId() : resourceFileId;
        return switch (mediaType) {
            case AUDIO, VOICE ->
                MatrixResponse.Message.ofAudioId(chatId, channel, finalFileId, getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
            case PHOTO ->
                MatrixResponse.Message.ofImageId(chatId, channel, finalFileId, getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
            case VIDEO, VIDEO_NOTE ->
                MatrixResponse.Message.ofVideoId(chatId, channel, finalFileId, getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
            case DOCUMENT ->
                MatrixResponse.Message.ofFileId(chatId, channel, finalFileId, getGlobalCommands(talksConfig), getTriggers(talksConfig), showActions, dbId);
            default -> null;
        };
    }

    private MatrixResponse.Message buildMatrixDeleteMessage(long dbId) {
        if (messageId == null) {
            throw new RuntimeException("messageId not set for DeleteMessage action");
        }
        return MatrixResponse.Message.ofDeleteMessage(chatId, channel, messageId, dbId);
    }

    private List<String> parseTextCommands() {
        if (mediaType != MediaType.TEXT) {
            return emptyList();
        }

        List<String> commands = new ArrayList<>();

        Matcher matcher = REGEX_COMMAND.matcher(text);
        while (matcher.find()) {
            commands.add(matcher.group());
        }

        return commands;
    }

    private List<String> getGlobalCommands(TalksConfiguration talksConfig) {
        return TalkManager.getInstance(talksConfig).getGlobalHandlers().stream()
            .flatMap(handler -> handler.getTriggers().stream())
            .filter(trigger -> ! trigger.isRegex())
            .map(InputTrigger::getText)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    private List<String> getTriggers(TalksConfiguration talksConfig) {
        List<InputHandler> effectiveHandlers = Talk.hasPreserveHandlers(handlers) ?
            TalkManager.getInstance(talksConfig).getBoundedHandlers(chatId) : //TODO does not select by the current Talk
            handlers;

        Stream<String> textTriggers = effectiveHandlers.stream()
            .flatMap(handler -> handler.getTriggers().stream())
            .filter(trigger -> ! trigger.isRegex())
            .map(InputTrigger::getText)
            .filter(Objects::nonNull);

        Stream<String> buttons = effectiveHandlers.stream()
            .flatMap(handler -> handler.getTriggers().stream())
            .map(InputTrigger::getButton)
            .filter(Objects::nonNull)
            .map(InputTrigger.Button::getText);

        return Stream.concat(textTriggers, buttons)
            .distinct()
            .collect(Collectors.toList());
    }

    private List<String> splitText(String text) {
        List<String> chunks = splitText("\n\n", singletonList(text));
        chunks = splitText("\n", chunks);
        chunks = splitText(" ", chunks);
        return chunks;
    }

    private List<String> splitText(String separator, List<String> texts) {
        if (allTextsAreShort(texts)) {
            return texts;
        }

        List<String> chunks = new ArrayList<>();
        for (String text : texts) {
            chunks.addAll(splitText(separator, text));
        }
        return chunks;
    }

    private List<String> splitText(String separator, String text) {
        if (text.length() <= MAX_TEXT_MESSAGE_LENGTH) {
            return singletonList(text);
        }

        List<String> chunks = new ArrayList<>();
        String chunk = "";
        for (String part : text.split(separator)) {
            if (! chunk.isEmpty() && chunk.length() + separator.length() + part.length() > MAX_TEXT_MESSAGE_LENGTH) {
                chunks.add(chunk);
                chunk = "";
            }
            chunk += (chunk.isEmpty() ? "" : separator) + part;
        }
        if (! chunk.isEmpty()) {
            chunks.add(chunk);
        }
        return chunks;
    }

    private boolean allTextsAreShort(List<String> texts) {
        for (String text : texts) {
            if (text.length() > MAX_TEXT_MESSAGE_LENGTH) {
                return false;
            }
        }
        return true;
    }

    private void decorateButtons(TalkResponse response) {
        ButtonDecorator.getInstance().decorate(response);
    }

    private static long getFlyingMessagesDefaultTTL() {
        return TimeUnit.MILLISECONDS.convert(LibSetting.getInteger(LibSetting.FLYING_MESSAGES_DURATION_IN_MINUTES), TimeUnit.MINUTES);
    }

    private static long getFlyingMessagesMaxTTL() {
        return TimeUnit.MILLISECONDS.convert(LibSetting.getInteger(LibSetting.FLYING_MESSAGES_MAX_DURATION_IN_MINUTES), TimeUnit.MINUTES);
    }

    private static boolean isFlyingEnabled() {
        return getFlyingMessagesDefaultTTL() != -1;
    }

    private static String newUuid() {
        return UUID.randomUUID().toString();
    }
}
