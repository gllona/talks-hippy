package org.logicas.librerias.talks.localization;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class BaseStrings {

    private static Logger logger = LogManager.getLogger(BaseStrings.class);

    private final Map<Enum<?>, Map<Enum<?>, String>> values = new HashMap<>();
    private final BotLangGetter botLangGetter;
    private Class<? extends Enum<?>> langsEnumClass;
    private Class<? extends Enum<?>> stringsEnumClass;

    protected BaseStrings(BotLangGetter botLangGetter) {
        this.botLangGetter = botLangGetter;
        configureEnumClasses();
        setStrings();
        checkAllStringsAreBuilt();
    }

    protected abstract void configureEnumClasses();

    protected abstract Enum<?> getDefaultLang();

    protected abstract void setStrings();

    protected <T extends Enum<?>> void setLangs(Class<T> langsEnumClass) {
        this.langsEnumClass = langsEnumClass;
    }

    protected <T extends Enum<?>> void setStrings(Class<T> stringsEnumClass) {
        this.stringsEnumClass = stringsEnumClass;
    }

    @FunctionalInterface
    public interface BotLangGetter {
        Enum<?> getBotLang(String session);
    }

    @RequiredArgsConstructor
    protected class Builder {
        final Enum<?> str;
        Map<Enum<?>, String> builderValues = new HashMap<>();
        public Builder with(Enum<?> lang, String value) {
            if (! langsEnumClass.isInstance(lang)) {
                throw new RuntimeException(String.format("Strings: trying to call builder with() with %s which is not a Langs enum value", str.name()));
            }
            builderValues.put(lang, value);
            return this;
        }
        public void build() {
            if (builderValues.size() == 0) {
                throw new RuntimeException(String.format("Strings: builder for %s has no values defined", str.name()));
            }
            values.put(str, builderValues);
        }
    }

    @RequiredArgsConstructor
    public class StringGetter {
        final Enum<?> str;
        Map<String, String> parameters = new HashMap<>();
        public StringGetter with(String parameter, String value) {
            parameters.put(parameter, value);
            return this;
        }
        public String forSession(String session) {
            Map<Enum<?>, String> strings = values.get(str);
            Enum<?> lang = getLangToUse(session, strings);
            String original = strings.get(lang);
            String transformed = applyTransformations(original);
            return transformed;
        }
        private Enum<?> getLangToUse(String session, Map<Enum<?>, String> strings) {
            Enum<?> botLang = botLangGetter.getBotLang(session);
            Enum<?> defaultLang = getDefaultLang();
            if (strings.containsKey(botLang)) {
                return botLang;
            } else if (strings.containsKey(defaultLang)) {
                logger.warn("Strings: using default lang {} for string {} (bot lang {} is not defined for this string)", defaultLang.name(), str.name(), botLang.name());
                return defaultLang;
            } else {
                Enum<?> firstLang = strings.entrySet().iterator().next().getKey();
                logger.warn("Strings: using lang {} for string {} (bot lang {} and default lang {} are not defined for this string)", firstLang.name(), str.name(), botLang.name(), defaultLang.name());
                return firstLang;
            }
        }
        private String applyTransformations(String original) {
            String transformed = original;
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                String regexStr = "\\{" + entry.getKey() + "\\}";
                transformed = transformed.replaceAll(regexStr, entry.getValue() == null ? "" : entry.getValue());
            }
            return transformed;
        }
    }

    protected Builder builder(Enum<?> str) {
        if (! stringsEnumClass.isInstance(str)) {
            throw new RuntimeException(String.format("Strings: trying to instance a builder with %s which is not a Strings enum value", str.name()));
        }
        return new Builder(str);
    }

    private void checkAllStringsAreBuilt() {
        checkAllStringsAreSetInValues();
        checkAllStringsAreSetForDefaultLang();
    }

    private void checkAllStringsAreSetInValues() {
        List<Enum<?>> missingValues = new ArrayList<>();
        for (Enum<?> str : stringsEnumClass.getEnumConstants()) {
            if (! values.containsKey(str)) {
                missingValues.add(str);
            }
        }
        if (missingValues.size() > 0) {
            throw new RuntimeException(String.format("Strings: missing builders for: %s",
                missingValues.stream().map(Enum::name).collect(Collectors.joining(", "))
            ));
        }
    }

    private void checkAllStringsAreSetForDefaultLang() {
        List<Enum<?>> missingValues = new ArrayList<>();
        for (Enum<?> str : stringsEnumClass.getEnumConstants()) {
            if (! values.get(str).containsKey(getDefaultLang())) {
                missingValues.add(str);
            }
        }
        if (missingValues.size() > 0) {
            throw new RuntimeException(String.format("Strings: missing default language %s definition for: %s",
                getDefaultLang(),
                missingValues.stream().map(Enum::name).collect(Collectors.joining(", "))
            ));
        }
    }

    public StringGetter get(Enum<?> str) {
        return new StringGetter(str);
    }
}
