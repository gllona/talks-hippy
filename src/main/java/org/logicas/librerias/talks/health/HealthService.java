package org.logicas.librerias.talks.health;

import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.InputAdapters;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.Objects;
import java.util.Random;

public class HealthService {

    public static final String CHAT_ID = "HEALTH_CHAT_ID";
    private static final String SENDER_ID = "HEALTH_SENDER_ID";
    public static final String REQUEST_TEXT = "HEALTHCHECK_WITH_OBFUSCATION_" + new Random().nextInt();
    public static final String RESPONSE_TEXT = "HEALTHCHECK_RESPONSE";

    private static HealthService instance = null;

    private final TalksConfiguration talksConfig;
    private final HealthInputAdapter healthInputAdapter;

    public static synchronized HealthService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new HealthService(talksConfig);
        }
        return instance;
    }

    public HealthService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        healthInputAdapter = (HealthInputAdapter) talksConfig.getInputAdaptersManager().get(InputAdapters.HEALTH);
    }

    public synchronized void check() {
        checkRouting();
        //TODO check message sender
    }

    private void checkRouting() {
        TalkResponse response = null;

        try {
            TalkRequest request = TalkRequest.ofHealth(
                healthInputAdapter,
                CHAT_ID,
                SENDER_ID,
                REQUEST_TEXT
            );
            healthInputAdapter.receiveRequest(request);
            response = healthInputAdapter.getLastResponse();
        } catch (Exception e) {
            throw new HealthException(e);
        }

        if (response == null || ! Objects.equals(response.getText(), RESPONSE_TEXT)) {
            throw new HealthException("Unexpected TalkResponse content, incoming message routing is failing");
        }
    }
}
