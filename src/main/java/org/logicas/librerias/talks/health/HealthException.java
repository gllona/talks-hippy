package org.logicas.librerias.talks.health;

public class HealthException extends RuntimeException {

    public HealthException(String message) {
        super(message);
    }

    public HealthException(Throwable cause) {
        super(cause);
    }
}
