package org.logicas.librerias.talks.health;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class HealthInternalTalk extends Talk {

    public HealthInternalTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(
            className(this), "/org.logicas.librerias.talks.health.HealthInternalTalk::handle",
            InputTrigger.ofString(HealthService.REQUEST_TEXT)
        ));
    }

    public TalkResponse handle(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            HealthService.RESPONSE_TEXT
        );
    }
}
