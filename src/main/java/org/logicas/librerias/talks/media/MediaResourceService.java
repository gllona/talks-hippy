package org.logicas.librerias.talks.media;

import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.InputAdapters;

import java.time.LocalDateTime;
import java.util.Objects;

public class MediaResourceService {

    private static MediaResourceService instance = null;

    private final TalksConfiguration talksConfig;
    private final MediaResourceRepository repository;

    public static synchronized MediaResourceService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new MediaResourceService(talksConfig);
        }
        return instance;
    }

    public MediaResourceService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        repository = MediaResourceRepository.getInstance(talksConfig.getDao());
    }

    public synchronized void createIfNotExists(String resourceId, InputAdapters inputAdapter) {
        MediaResource resource = repository.readByResourceIdAndInputAdapter(resourceId, inputAdapter.name());
        if (resource == null) {
            resource = new MediaResource(
                null,
                resourceId,
                inputAdapter.name(),
                null,
                LocalDateTime.now()
            );
            repository.save(resource);
        }
    }

    public String findFileId(String resourceId, InputAdapters inputAdapter) {
        if (resourceId == null) {
            return null;
        }
        MediaResource resource = repository.readByResourceIdAndInputAdapter(resourceId, inputAdapter.name());
        return resource == null ? null : resource.getFileId();
    }

    public void update(String resourceId, InputAdapters inputAdapter, String fileId) {
        if (fileId == null) {
            return;
        }
        MediaResource resource = repository.readByResourceIdAndInputAdapter(resourceId, inputAdapter.name());
        if (resource != null && resource.getFileId() == null && ! Objects.equals(resource.getFileId(), fileId)) {
            resource.setFileId(fileId);
            resource.setUpdatedAt(LocalDateTime.now());
            repository.save(resource);
        }
    }
}
