package org.logicas.librerias.talks.media;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.QueryBuilder;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

public class MediaResourceRepository extends SingleKeyEntityRepository<MediaResource> {

    private static MediaResourceRepository instance;

    private Dao dao;

    private MediaResourceRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized MediaResourceRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new MediaResourceRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "talks_media_resources";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return MediaResource.FIELDS;
    }

    @Override
    public String idFieldName() {
        return MediaResource.FIELD_ID;
    }

    public MediaResource readByResourceIdAndInputAdapter(String resourceId, String inputAdapter) {
        return readByCondition(String.format("%s = '%s' AND %s = '%s'",
                MediaResource.FIELD_RESOURCE_ID, QueryBuilder.escapeSql(resourceId),
                MediaResource.FIELD_INPUT_ADAPTER, QueryBuilder.escapeSql(inputAdapter)
            )).stream()
              .findFirst()
              .orElse(null);
    }
}
