package org.logicas.librerias.talks.media;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Setter
@Getter
@ToString
public class MediaResource extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_RESOURCE_ID = "resource_id";
    public static final String FIELD_INPUT_ADAPTER = "input_adapter";
    public static final String FIELD_FILE_ID = "file_id";
    public static final String FIELD_UPDATED_AT = "updated_at";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_RESOURCE_ID,
        FIELD_INPUT_ADAPTER,
        FIELD_FILE_ID,
        FIELD_UPDATED_AT
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private String resourceId;
    private String inputAdapter;
    private String fileId;
    private LocalDateTime updatedAt;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getResourceId() {
        return resourceId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(resourceId, FIELD_RESOURCE_ID, params);
        conditionalAddToParams(inputAdapter, FIELD_INPUT_ADAPTER, params);
        conditionalAddToParams(fileId, FIELD_FILE_ID, params);
        conditionalAddToParams(updatedAt, FIELD_UPDATED_AT, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }
}
