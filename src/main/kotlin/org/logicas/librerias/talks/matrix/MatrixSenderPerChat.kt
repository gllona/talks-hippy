package org.logicas.librerias.talks.matrix

import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.sender.v3.MessageSender
import org.logicas.librerias.talks.sender.v3.TalkResponseDbWrapper
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.math.max

class MatrixSenderPerChat(
    val chatId: String,
    val talksConfig: TalksConfiguration,
    val messageSender: MessageSender
) {
    var processor: Job? = null
    val startMutex = Mutex()
    val started = AtomicBoolean(false)
    val sendQueue = Channel<TalkResponseDbWrapper>(Channel.UNLIMITED)
    val tracer = talksConfig.tracer

    companion object {
        var senders = ConcurrentHashMap<String, MatrixSenderPerChat>()
        val logger = LogManager.getLogger(MatrixSenderPerChat::class.java)

        fun get(
            chatId: String,
            talksConfig: TalksConfiguration,
            messageSender: MessageSender
        ): MatrixSenderPerChat {
            return senders.getOrPut(chatId) { MatrixSenderPerChat(chatId, talksConfig, messageSender) }
        }

        fun shutdownAll() {
            senders.values.forEach { it.shutdown() }
        }
    }

    suspend fun start() {
        if (started.get()) {
            return
        }

        startMutex.withLock {
            if (! started.get()) {
                processor = Coroutines.launchIO(Coroutines.Group.MATRIX_MESSAGE_SENDER) {
                    doStart()
                }
                started.set(true)
            }
        }
    }

    private suspend fun doStart() = coroutineScope {
        var lastSendTimestamp = System.currentTimeMillis() - getInterMessageDelay()

        while (isActive) {
            delayForThisChat(lastSendTimestamp)
            if (isActive) {
                val message = sendQueue.receive()
                delayForAllChats()
                lastSendTimestamp = System.currentTimeMillis()
                doSendMessage(message)
            }
        }
    }

    private suspend fun delayForThisChat(lastSendTimestamp: Long) {
        val now = System.currentTimeMillis()
        val millisSinceLastSend = now - lastSendTimestamp
        val toDelay = max(0, getInterMessageDelay() - millisSinceLastSend)
        if (toDelay > 0) {
            delay((getInterMessageDelay() * 1.01).toLong()) // add 1% to play safe
        }
    }

    private suspend fun delayForAllChats() = coroutineScope {
        while (isActive && messageSender.sendCapacityFull()) { //TODO split Matrix from Telegram
            delay(getInterMessageDelay() / 5) // 200 ms
        }
    }

    suspend fun sendMessage(message: TalkResponseDbWrapper) {
        start()
        sendQueue.send(message)
    }

    private suspend fun doSendMessage(message: TalkResponseDbWrapper) = coroutineScope {
        messageSender.markMessageAsSent(message, false)
    }

    fun shutdown() {
        sendQueue.close()
        processor?.cancel()
    }

    private fun getInterMessageDelay(): Long {
        return LibSetting.getInteger(LibSetting.CHAT_INTER_MESSAGE_DELAY_IN_MILLIS).toLong()
    }
}
