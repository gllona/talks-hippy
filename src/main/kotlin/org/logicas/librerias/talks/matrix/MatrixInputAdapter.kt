package org.logicas.librerias.talks.matrix

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.InputAdapterApi
import org.logicas.librerias.talks.api.ModelApi
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.api.TracerApi
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.engine.MediaType
import org.logicas.librerias.talks.engine.TalkRequest
import org.logicas.librerias.talks.engine.TalkResponse
import org.logicas.librerias.talks.engine.Usernames
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

class MatrixInputAdapter(
    private val talksConfig: TalksConfiguration
) : InputAdapterApi {

    companion object {
        private val logger = LogManager.getLogger(MatrixInputAdapter::class.java)
    }

    private var model: ModelApi? = null
    private val tracer: TracerApi = talksConfig.tracer

    private val duplicatesCache: LoadingCache<String, Boolean> = CacheBuilder.newBuilder()
        .expireAfterWrite(LibSetting.getInteger(LibSetting.DUPLICATES_CACHE_TTL_IN_SECONDS).toLong(), TimeUnit.SECONDS)
        .build(CacheLoader.from { _: String? -> false })

    override fun getTalksConfiguration(): TalksConfiguration {
        return talksConfig
    }

    override suspend fun dispatch(model: ModelApi) {
        this.model = model
    }

    override fun receiveRequest(request: TalkRequest) {
        if (shouldReceive(request)) {
            Coroutines.forceLaunchDefault(Coroutines.Group.MATRIX_INPUT_ADAPTER, ephemeral = true) {
                doReceiveRequest(request)
            }
        }
    }

    private fun shouldReceive(request: TalkRequest): Boolean {
        return ! isDuplicated(request.messageId) &&
            (shouldProcessEchoMessages() || ! isEchoMessage(request.username))
    }

    private suspend fun doReceiveRequest(request: TalkRequest) {
        assertModelIsSet()
        val matrixRequest = request.matrixRequest
        val roomId = request.chatId
        val username = request.username
        val messageId = request.messageId

        try {
            tracer.info(
                roomId,
                username,
                messageId,
                String.format("Entered receiveRequest from Matrix with text [%s]",
                    Optional.ofNullable(matrixRequest.body)
                        .map { text: String? -> text?.replace("\n", "\\n") }
                        .orElse(null)
                ))
            val response = model!!.receiveMatrixMessage(
                this,
                matrixRequest
            )
            if (response.isPresent) {
                sendMessage(response.get(), username, messageId)
            }
        } catch (e: Exception) {
            tracer.error(roomId, username, messageId, "Exception when handling MatrixRequest", e)
        } finally {
            tracer.info(roomId, username, messageId, "To exit receiveRequest for MatrixRequest")
        }
    }

    private fun shouldProcessEchoMessages(): Boolean {
        return Objects.equals(LibSetting.getInteger(LibSetting.PROCESS_ECHO_MESSAGES), 1)
    }

    private fun isEchoMessage(username: String): Boolean {
        return Usernames.isMatrixBot(username)
    }

    private fun isDuplicated(messageId: String): Boolean {
        val cached = duplicatesCache.getIfPresent(messageId)
        return if (cached != null && cached) {
            true
        } else {
            duplicatesCache.put(messageId, true)
            false
        }
    }

    override fun sendResponse(response: TalkResponse) {
        sendMessage(response, null, null)
    }

    private fun sendMessage(response: TalkResponse, username: String?, messageId: String?) {
        enqueueMessage(response, username, messageId)
    }

    private fun enqueueMessage(response: TalkResponse, username: String?, messageId: String?) {
        var nextResponse : TalkResponse? = response
        while (nextResponse != null) {
            try {
                talksConfig.messageSender.send(nextResponse)

            } catch (e: Exception) {
                val roomId = response.chatId
                tracer.error(roomId, username, messageId, "Exception trying to generate response for Matrix message", e)
                val exceptionalResponse = nextResponse.exceptionMapper?.apply(e)
                if (exceptionalResponse != null) {
                    try {
                        talksConfig.messageSender.send(exceptionalResponse)
                    } catch (e2: Exception) {
                        tracer.error(roomId, username, messageId, "Exception trying to generate exceptional response for Matrix message", e2)
                    }
                }
            }
            log(nextResponse, username, messageId)
            nextResponse = nextResponse.nextResponse
        }
    }

    private fun log(response: TalkResponse, username: String?, messageId: String?) {
        tracer.info(
            response.chatId,
            username,
            messageId,
            String.format(
                "Answered to Matrix Event with [%s]",
                if (response.mediaType != MediaType.TEXT) response.mediaType
                else response.fastText?.replace("\n", "\\n")
            )
        )
    }

    private fun assertModelIsSet() {
        if (model == null) {
            throw RuntimeException("Matrix Adapter: Model not set")
        }
    }

    override fun getFile(fileId: String): File {
        throw UnsupportedOperationException("Can not get a File object in Matrix input adapter.")
    }

    override suspend fun shutdown() {
        Coroutines.shutdown(Coroutines.Group.MATRIX_INPUT_ADAPTER)
    }
}
