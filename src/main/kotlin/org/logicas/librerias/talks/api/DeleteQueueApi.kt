package org.logicas.librerias.talks.api

import java.io.File
import java.util.concurrent.TimeUnit

interface DeleteQueueApi {

    fun deleteAfter(file: File, duration: Int, timeUnit: TimeUnit, deleteDirectory: Boolean)
}
