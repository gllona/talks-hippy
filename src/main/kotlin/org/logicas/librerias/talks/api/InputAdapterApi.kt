package org.logicas.librerias.talks.api

import org.logicas.librerias.talks.engine.TalkRequest
import org.logicas.librerias.talks.engine.TalkResponse
import java.io.File

interface InputAdapterApi {

    fun getTalksConfiguration(): TalksConfiguration

    suspend fun dispatch(model: ModelApi)

    fun receiveRequest(request: TalkRequest)

    fun sendResponse(response: TalkResponse)

    fun getFile(fileId: String): File

    suspend fun shutdown()
}
