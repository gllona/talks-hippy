package org.logicas.librerias.talks.api

import com.pengrad.telegrambot.model.Update
import org.logicas.librerias.talks.engine.TalkRequest
import org.logicas.librerias.talks.engine.TalkResponse
import org.logicas.librerias.talks.matrix.MatrixReceiveMessageRequest
import java.util.*

interface ModelApi {

    fun shutdown()

    suspend fun receiveTelegramMessage(
        inputAdapter: InputAdapterApi,
        update: Update?
    ): Optional<TalkResponse> {
        return inputAdapter.getTalksConfiguration().talkManager.dispatch(
            TalkRequest(
                inputAdapter,
                update,
                null,
                null
            )
        )
    }

    suspend fun receiveMatrixMessage(
        inputAdapter: InputAdapterApi,
        matrixRequest: MatrixReceiveMessageRequest?
    ): Optional<TalkResponse> {
        return inputAdapter.getTalksConfiguration().talkManager.dispatch(
            TalkRequest(
                inputAdapter,
                null,
                matrixRequest,
                null
            )
        )
    }

    fun beforeReceiveMessages(
        inputAdapter: InputAdapterApi,
        updates: List<Update>
    ) {
        // nothing to do
    }

    fun afterReceiveMessages(
        inputAdapter: InputAdapterApi,
        updates: List<Update>
    ) {
        // nothing to do
    }
}
