package org.logicas.librerias.talks.api

interface AppApi {

    suspend fun shutdown()
}
