package org.logicas.librerias.talks.api

import org.logicas.librerias.talks.engine.*
import java.util.*

interface TalkManagerApi {

    suspend fun dispatch(call: TalkRequest): Optional<TalkResponse>

    // invocation assumes Talks enum elements are same name as their classes
    fun talkForName(simpleClassName: String): Talk?

    fun <T : Talk> talksForClass(clazz: Class<T>): List<T>

    fun hasTalkContexts(session: String): Boolean

    fun resetTalkContexts(session: String)

    fun flushTalkContexts(session: String)

    fun saveAndFlushHandlers(talk: Talk, chatId: String, talkResponse: TalkResponse)

    fun getGlobalHandlers(): List<InputHandler?>

    fun getBoundedHandlers(session: String): List<InputHandler?>

    fun followSession(session: String): String?

    fun switchSessionInitiate(session: String, switchMainSession: Boolean, nextTalkResponse: TalkResponse): String

    fun switchSessionComplete(session: String, code: String, talkContext: TalkContext): Optional<TalkResponse>?

    fun shutDown()
}
