package org.logicas.librerias.talks.api

import org.logicas.librerias.talks.config.InputAdapters

interface InputAdaptersManagerApi {

    fun get(inputAdapterType: InputAdapters): InputAdapterApi

    suspend fun startInputAdapters()

    suspend fun stopInputAdapters()
}
