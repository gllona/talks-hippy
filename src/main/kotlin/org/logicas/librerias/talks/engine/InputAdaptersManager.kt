package org.logicas.librerias.talks.engine

import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.InputAdapterApi
import org.logicas.librerias.talks.api.InputAdaptersManagerApi
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.InputAdapters
import java.util.concurrent.ConcurrentHashMap

class InputAdaptersManager(
    private val talksConfig: TalksConfiguration
) : InputAdaptersManagerApi {

    companion object {
        private var instance: InputAdaptersManager? = null
        private val logger = LogManager.getLogger(InputAdaptersManager::class.java)

        @JvmStatic
        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration?): InputAdaptersManager? {
            if (talksConfig == null) {
                return null
            }
            if (instance == null) {
                instance = InputAdaptersManager(talksConfig)
            }
            return instance as InputAdaptersManager
        }
    }

    private val inputAdapters: ConcurrentHashMap<InputAdapters, InputAdapterApi> = ConcurrentHashMap<InputAdapters, InputAdapterApi>()

    override fun get(inputAdapterType: InputAdapters): InputAdapterApi {
        ensureInputAdapterStarted(inputAdapterType)
        return inputAdapters[inputAdapterType]!!
    }

    private fun ensureInputAdapterStarted(inputAdapterType: InputAdapters) {
        if (inputAdapterStarted(inputAdapterType)) {
            return
        }
        val job = Coroutines.forceLaunchIO(Coroutines.Group.UTILS, ephemeral = true) {
            startInputAdapter(inputAdapterType)
        }
        job.start()
        while (! job.isCompleted) {
            Thread.sleep(20)
        }
    }

    override suspend fun startInputAdapters() {
        try {
            for (adapterType in InputAdapters.values()) {
                startInputAdapter(adapterType)
            }
        } catch (e: Exception) {
            throw RuntimeException("Exception starting InputAdapters", e)
        }
    }

    private suspend fun startInputAdapter(adapterType: InputAdapters) {
        if (adapterType.isActive && ! inputAdapterStarted(adapterType)) {
            val adapter = adapterType.adapter.getDeclaredConstructor(TalksConfiguration::class.java).newInstance(talksConfig)
            if (! inputAdapterStarted(adapterType)) {
                inputAdapters.putIfAbsent(adapterType, adapter)
                adapter.dispatch(talksConfig.model)
            }
            logger.info(String.format("Started input adapter [%s]", adapterType))
        }
    }

    private fun inputAdapterStarted(adapterType: InputAdapters): Boolean {
        return inputAdapters.containsKey(adapterType)
    }

    override suspend fun stopInputAdapters() {
        for ((adapterType, adapter) in inputAdapters) {
            logger.info(String.format("Stopped input adapter [%s]", adapterType))
            adapter.shutdown()
        }

        talksConfig.interactionsManager.shutdown()
        Coroutines.shutdown(Coroutines.Group.INTERACTIONS_MANAGER)
        Coroutines.shutdown(Coroutines.Group.MESSAGE_SENDER)
        Coroutines.shutdown(Coroutines.Group.TELEGRAM_MESSAGE_SENDER)
        Coroutines.shutdown(Coroutines.Group.MATRIX_MESSAGE_SENDER)
        talksConfig.messageSender.shutdown()
    }
}
