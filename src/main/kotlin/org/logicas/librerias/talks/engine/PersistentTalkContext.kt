package org.logicas.librerias.talks.engine

import kotlinx.coroutines.delay
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.copersistance.Dao
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.Coroutines.forceLaunchIO
import org.logicas.librerias.talks.api.TalkType
import org.logicas.librerias.talks.config.LibSetting
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.math.absoluteValue
import kotlin.math.pow

class PersistentTalkContext(dao: Dao, private val talk: Talk) : TalkContext() {

    companion object {
        private val logger = LogManager.getLogger(
            PersistentTalkContext::class.java
        )
        private const val GLOBAL_SESSION_ID: String = "-1"
    }

    private data class CacheKey(
        val session: String,
        var type: String? = null
    )

    private val globalHandlersContext = InMemoryTalkContext()
    private val personalDataRemover: PersonalDataRemover
    private val repository: PersistentTalkContextRepository
    private val cache: MutableMap<CacheKey, PersistentTalkContextEntry> = ConcurrentHashMap()

    init {
        personalDataRemover = PersonalDataRemover.getInstance(dao)
        repository = PersistentTalkContextRepository.getInstance(dao)
    }

    override fun getTalk(): Talk {
        return talk
    }

    override fun defer(): PersistentTalkContext {
        return if (finalContext == null) this else (finalContext as PersistentTalkContext).defer()
    }

    override fun getGlobalHandlers(): List<InputHandler> {
        return defer().globalHandlersContext.globalHandlers
    }

    override fun getBoundedHandlers(session: String): List<InputHandler> {
        val finalContext = defer()
        return finalContext.repository.readHandlers(getDeferredTalkType(), finalContext.follow(session))
    }

    override fun deleteBoundedHandlers(session: String) {
        val finalContext = defer()
        val handlersEntry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), finalContext.follow(session))
        if (handlersEntry != null) {
            handlersEntry.setInputHandlers(emptyList())
            finalContext.repository.save(handlersEntry)
            finalContext.cache(handlersEntry)
        }
        //finalContext.repository.deleteHandlersEntry(follow(session)) //TODO check
    }

    @Synchronized
    override fun setGlobalHandlers(handlers: List<InputHandler>) {
        defer().globalHandlersContext.globalHandlers = handlers
    }

    override fun setHandlers(session: String, handlers: List<InputHandler>?) {
        if (handlers == null || Talk.preserveHandlers() == handlers) {
            return
        }
        val finalContext = defer()
        val mainSession = finalContext.follow(session)
        var entry = finalContext.cached(mainSession, PersistentTalkContextEntry.TYPE_INPUT_HANDLERS)
        if (entry == null) {
            entry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), mainSession)
            if (entry == null) {
                entry = PersistentTalkContextEntry(
                    mainSession,
                    talk.getType().name,
                    PersistentTalkContextEntry.TYPE_INPUT_HANDLERS
                )
            }
            finalContext.cache(entry)
        }
        entry.setInputHandlers(handlers)
    }

    @Synchronized
    override fun setGlobal(name: String, value: Any?) {
        defer().set(GLOBAL_SESSION_ID, name, value)
    }

    override fun getGlobal(name: String): Any? {
        return defer().get(GLOBAL_SESSION_ID, name)
    }

    override fun set(session: String, name: String, value: Any?) {
        val finalContext = defer()
        val mainSession = finalContext.follow(session)
        val varsEntry = finalContext.getVarsEntry(mainSession)
        val vars = varsEntry.vars
        val finalValue = if (value is String) {
            TextEncoder.normalizeAndEncode(value)
        } else {
            value
        }
        vars[name] = finalValue
        varsEntry.vars = vars
    }

    override fun remove(session: String, name: String) {
        val finalContext = defer()
        val mainSession = finalContext.follow(session)
        var entry = finalContext.cached(mainSession, PersistentTalkContextEntry.TYPE_VARS)
        if (entry == null) {
            entry = finalContext.repository.readVarsEntry(getDeferredTalkType(), mainSession)
            if (entry == null) {
                return
            }
            finalContext.cache(entry)
        }
        val vars = entry.vars
        vars.remove(name)
        entry.vars = vars
    }

    override fun get(session: String, name: String): Any? {
        val finalContext = defer()
        val mainSession = finalContext.follow(session)
        var entry = finalContext.cached(mainSession, PersistentTalkContextEntry.TYPE_VARS)
        if (entry == null) {
            entry = finalContext.repository.readVarsEntry(getDeferredTalkType(), mainSession)
            if (entry == null) {
                return null
            }
            finalContext.cache(entry)
        }
        var value = entry.vars[name]
        if (value is String) {
            value = TextEncoder.unencode(value as String?)
        }
        return value
    }

    private fun setAll(session: String, vars: Map<String, Any?>?) {
        if (vars == null) {
            return
        }
        val finalContext = defer()
        val mainSession = finalContext.follow(session)
        val varsEntry = finalContext.getVarsEntry(mainSession)
        varsEntry.setVars(vars)
    }

    private fun getVarsEntry(mainSession: String): PersistentTalkContextEntry {
        val finalContext = defer()
        var entry = cached(mainSession, PersistentTalkContextEntry.TYPE_VARS)
        if (entry == null) {
            entry = finalContext.repository.readVarsEntry(getDeferredTalkType(), mainSession)
            if (entry == null) {
                entry = PersistentTalkContextEntry(
                    mainSession,
                    talk.getType().name,
                    PersistentTalkContextEntry.TYPE_VARS
                )
            }
            finalContext.cache(entry)
        }
        return entry
    }

    override fun getAll(session: String): Map<String, Any?> {
        val finalContext = defer()
        val mainSession = finalContext.follow(session)
        var entry = finalContext.cached(mainSession, PersistentTalkContextEntry.TYPE_VARS)
        if (entry == null) {
            entry = finalContext.repository.readVarsEntry(getDeferredTalkType(), mainSession)
            if (entry == null) {
                return HashMap()
            }
            finalContext.cache(entry)
        }
        return entry.vars
    }

    override fun reset(session: String) {
        val finalContext = defer()
        finalContext.setAll(session, emptyMap())
        finalContext.setHandlers(session, emptyList())
        flush(session)
    }

    override fun flush(session: String) {
        flush(session, PersistentTalkContextEntry.TYPE_INPUT_HANDLERS)
        flush(session, PersistentTalkContextEntry.TYPE_VARS)
    }

    private fun flush(session: String, entryType: String) {
        val finalContext = defer()
        val mainSession = finalContext.follow(session)
        val cacheKey = CacheKey(mainSession, entryType)
        try {
            val entry = finalContext.cached(mainSession, entryType)
            if (entry != null) {
                finalContext.repository.save(buildDeferredEntry(entry))
                finalContext.cache.remove(cacheKey)
            }
        } catch (e: Exception) {
            logger.warn(
                String.format(
                    "Unable to save PersistentTalkContext for session [%s], main session [%s] and type [%s] (cache flush)",
                    session,
                    mainSession,
                    entryType
                )
            )
            // don't throw the exception, this is mainly a concurrency issue between instances
        }
    }

    private fun buildDeferredEntry(entry: PersistentTalkContextEntry): PersistentTalkContextEntry {
        return entry.withTalkClassName(defer().getTalk().javaClass.simpleName)
    }

    override fun forget(session: String) {
        forceLaunchIO(Coroutines.Group.UTILS, ephemeral = true) {
            delay(LibSetting.getInteger(LibSetting.PERSONAL_DATA_REMOVAL_DELAY_IN_MILLIS).toLong())

            val finalContext = defer()
            finalContext.personalDataRemover.remove(finalContext.follow(session))
        }
    }

    private fun cache(entry: PersistentTalkContextEntry) {
        val cacheKey = CacheKey(entry.sessionId, entry.type)
        defer().cache[cacheKey] = entry
    }

    private fun cached(session: String, entryType: String): PersistentTalkContextEntry? {
        val cacheKey = CacheKey(session, entryType)
        return defer().cache[cacheKey]
    }

    override fun follow(session: String): String {
        val finalContext = defer()
        var handlersEntry = finalContext.cached(session, PersistentTalkContextEntry.TYPE_INPUT_HANDLERS)
        if (handlersEntry == null) {
            handlersEntry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), session)
        }
        var i = 0 // safeguard against infinite loop
        while (handlersEntry?.pointsTo != null && i++ < 10) {
            handlersEntry = finalContext.repository.readById(handlersEntry.pointsTo)
            if (handlersEntry != null && Objects.equals(handlersEntry.sessionId, session)) {
                break
            }
        }
        return if (handlersEntry != null) handlersEntry.sessionId else session
    }

    override fun switchSessionInitiate(
        session: String,
        code: String,
        switchMainSession: Boolean,
        nextTalkResponse: TalkResponse
    ) {
        val finalContext = defer()

        var handlersEntry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), finalContext.follow(session)) ?: return

        if (handlersEntry.isNew) {
            return
        }

        var i = 0
        while (handlersEntry.level == 1 && handlersEntry.pointsTo != null && i++ < 10) { // safeguard against infinite loops
            handlersEntry = finalContext.repository.readById(handlersEntry.pointsTo)
        }

        handlersEntry.level = if (switchMainSession) 1 else 0 // 0 means main session
        handlersEntry.code = code
        handlersEntry.codeSetAt = LocalDateTime.now()
        handlersEntry.nextTalkResponseTyped = nextTalkResponse
        finalContext.repository.save(handlersEntry)
        finalContext.cache(handlersEntry)
    }

    override fun generateSwitchSessionCode(): String {
        var code: String

        cleanCodes()

        do {
            val codeLength = LibSetting.getInteger(LibSetting.SWITCH_SESSION_CODE_LENGTH)
            val bound = 10.0.pow(codeLength.toDouble()).toLong()
            val codeLong = Random().nextLong(bound).absoluteValue
            code = String.format("%0" + codeLength + "d", codeLong)
        } while (defer().repository.codeIsPresent(code))

        return code
    }

    private fun cleanCodes() {
        val finalContext = defer()
        finalContext.repository.getEntriesWithOldCodes()
            .forEach {
                it.code = null
                it.codeSetAt = null
                finalContext.repository.save(it)
            }
    }

    override fun matchesSwitchSessionCode(session: String, code: String): Boolean {
        val handlersEntry = defer().repository.readHandlersEntry(getDeferredTalkType(), session)
        return handlersEntry != null && handlersEntry.code == code
    }

    override fun hasHandlersEntry(session: String): Boolean {
        val handlersEntry = defer().repository.readHandlersEntry(getDeferredTalkType(), session)
        return if (handlersEntry == null) false else ! handlersEntry.isNew
    }

    override fun createHandlersEntry(session: String) {
        val finalContext = defer()
        val handlersEntry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), session)
        if (handlersEntry.isNew) {
            handlersEntry.setInputHandlers(emptyList())
            finalContext.repository.save(handlersEntry)
            finalContext.cache(handlersEntry)
        }
    }

    override fun getSessionIdsMatchingSwitchSessionCode(code: String): List<String> {
        return defer().repository.readSessionIdsBySwitchSessionCode(code)
    }

    override fun isSwitchSessionCodeValid(session: String, timeoutInSeconds: Int): Boolean {
        val handlersEntry = defer().repository.readHandlersEntry(getDeferredTalkType(), session)
        return if (handlersEntry != null) {
            val codeSetAt = handlersEntry.codeSetAt ?: throw RuntimeException(String.format("codeSetAt is not set [session=%s]", session))
            codeSetAt.plusSeconds(timeoutInSeconds.toLong()).isAfter(LocalDateTime.now())
        } else {
            throw RuntimeException(String.format("Talk context not found [session=%s]", session))
        }
    }

    override fun invalidateSwitchSessionCode(session: String) {
        val finalContext = defer()
        val handlersEntry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), session)
        handlersEntry.level = 0
        handlersEntry.code = null
        handlersEntry.codeSetAt = null
        finalContext.repository.save(handlersEntry)
        finalContext.cache(handlersEntry)
    }

    override fun getTalkType(): TalkType {
        return talk.getType()
    }

    private fun getDeferredTalkType(): TalkType {
        return defer().talk.getType()!!
    }

    override fun getNextTalkResponse(session: String): TalkResponse? {
        val handlersEntry = defer().repository.readHandlersEntry(getDeferredTalkType(), session)
        return handlersEntry?.nextTalkResponseTyped
    }

    override fun clearNextTalkResponse(session: String) {
        val finalContext = defer()
        val handlersEntry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), session)
        if (! handlersEntry.isNew) {
            handlersEntry.nextTalkResponseTyped = null
            finalContext.repository.save(handlersEntry)
            finalContext.cache(handlersEntry)
        }
    }

    override fun copyVarsFromSession(session: String, sourceSession: String) {
        val finalContext = defer()
        var targetVarsEntry = finalContext.repository.readVarsEntry(getDeferredTalkType(), session)
        val sourceVarsEntry = finalContext.repository.readVarsEntry(getDeferredTalkType(), sourceSession)

        if (targetVarsEntry == null) {
            targetVarsEntry = PersistentTalkContextEntry(session, sourceVarsEntry.talkName, sourceVarsEntry.type)
        }

        targetVarsEntry.vars = sourceVarsEntry.vars
        finalContext.repository.save(targetVarsEntry)
        finalContext.cache(targetVarsEntry)
    }

    override fun isMainSession(session: String): Boolean {
        val handlersEntry = defer().repository.readHandlersEntry(getDeferredTalkType(), session)
        return handlersEntry.level == 0
    }

    override fun pointTo(session: String, targetSession: String) {
        val finalContext = defer()
        val sourceHandlersEntry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), session)
        val targetHandlersEntry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), targetSession)

        sourceHandlersEntry.pointsTo = targetHandlersEntry.id
        sourceHandlersEntry.level = 1
        finalContext.repository.save(sourceHandlersEntry)
        finalContext.cache(sourceHandlersEntry)

        finalContext.setHandlers(session, emptyList())

        if (Objects.equals(finalContext.follow(targetSession), targetSession)) {
            setAsMainSession(targetSession)
        }
    }

    private fun setAsMainSession(session: String) {
        val finalContext = defer()
        val handlersEntry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), session)
        handlersEntry.level = 0
        handlersEntry.pointsTo = null
        finalContext.repository.save(handlersEntry)
        finalContext.cache(handlersEntry)
    }

    override fun deleteCode(session: String) {
        val finalContext = defer()
        val handlersEntry = finalContext.repository.readHandlersEntry(getDeferredTalkType(), session)
        handlersEntry.code = null
        handlersEntry.codeSetAt = null
        finalContext.repository.save(handlersEntry)
        finalContext.cache(handlersEntry)
    }
}
