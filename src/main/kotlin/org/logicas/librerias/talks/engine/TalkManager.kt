package org.logicas.librerias.talks.engine

import com.google.common.cache.CacheBuilder
import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.model.User
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines.cancelRemaining
import org.logicas.librerias.talks.api.TalkManagerApi
import org.logicas.librerias.talks.api.TalkType
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.health.HealthInternalTalk
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.function.Consumer
import java.util.stream.Collectors

class TalkManager private constructor(val talksConfig: TalksConfiguration) : TalkManagerApi {
    private val talks: MutableMap<Talk, Optional<InputHandler>> = TreeMap()

    companion object {
        private val locks = CacheBuilder.newBuilder().expireAfterWrite(LibSetting.getInteger(LibSetting.CONCURRENT_REQUESTS_CACHE_TTL_IN_SECONDS)?.toLong()!!, TimeUnit.SECONDS).build<String, Mutex>()
        private val logger = LogManager.getLogger(TalkManager::class.java)
        private var instance: TalkManagerApi? = null

        @JvmStatic
        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration): TalkManagerApi {
            if (instance == null) {
                instance = TalkManager(talksConfig)
            }
            return instance as TalkManagerApi
        }
    }

    init {
        logger.info("Starting TalkManager....")
        createTalks(talksConfig.getTalkTypes())
        setupGlobalHandlers()
        logger.info("TalkManager started.")
    }

    override fun getGlobalHandlers(): List<InputHandler> {
        return talks.keys.stream()
            .map { talk: Talk -> talk.getContext().getGlobalHandlers() }
            .findFirst()
            .orElse(emptyList())
    }

    override fun getBoundedHandlers(session: String): List<InputHandler> {
        return talks.keys.stream()
            .flatMap { talk: Talk -> talk.getContext().getBoundedHandlers(session).stream() }
            .distinct()
            .collect(Collectors.toList())
    }

    private fun createTalks(talkTypes: List<TalkType>) {
        val healthTalk = createHealthTalkType()
        val allTalkTypes: MutableList<TalkType> = ArrayList(talkTypes)
        allTalkTypes.addAll(talkTypes)
        allTalkTypes.add(healthTalk)
        for (talkType in allTalkTypes) {
            val talk = instanceTalk(talkType)
            val handler = talk.getDefaultHandler()
            talks[talk] = handler
        }
    }

    private fun createHealthTalkType(): TalkType {
        return object : TalkType {
            override fun getTalkClass(): Class<out Talk> {
                return HealthInternalTalk::class.java
            }

            override fun getName(): String {
                return HealthInternalTalk::class.java.getSimpleName()
            }
        }
    }

    private fun instanceTalk(talkType: TalkType): Talk {
        return try {
            talkType.getTalkClass()
                .getDeclaredConstructor(TalksConfiguration::class.java, TalkType::class.java)
                .newInstance(talksConfig, talkType)
        } catch (e: Exception) {
            throw RuntimeException(String.format("Can not instance class [%s]", talkType.getTalkClass().getSimpleName()), e)
        }
    }

    private fun setupGlobalHandlers() {
        val allHandlers = ArrayList(talks.values).stream()
            .filter { obj: Optional<InputHandler> -> obj.isPresent }
            .map { obj: Optional<InputHandler> -> obj.get() }
            .collect(Collectors.toList())
        for (entry in talks.keys) {
            entry.getContext().setGlobalHandlers(allHandlers)
        }
    }

    override fun hasTalkContexts(session: String): Boolean {
        val handlersOfSomeTalk = getTalks().stream()
            .map { obj: Talk -> obj.getContext() }
            .map { talkContext: TalkContext -> talkContext.getBoundedHandlers(session) }
            .filter { inputHandlers: List<InputHandler>? -> !inputHandlers!!.isEmpty() }
            .findAny()
            .orElse(null)
        return handlersOfSomeTalk != null
    }

    override fun resetTalkContexts(session: String) {
        for (talk in getTalks()) {
            talk.getContext().reset(session)
        }
    }

    override fun flushTalkContexts(session: String) {
        getTalks().stream()
            .map { obj: Talk -> obj.getContext() }
            .distinct()
            .forEach { talkContext: TalkContext -> talkContext.flush(session) }
    }

    override suspend fun dispatch(call: TalkRequest): Optional<TalkResponse> {
        updateCallFields(call)
        var finalResponse = Optional.empty<TalkResponse>()
        for (talk in getTalks()) {
            var response: Optional<TalkResponse> = Optional.empty()
            val shouldLockDispatch = talk.shouldLockDispatch(call)
            if (shouldLockDispatch != null && shouldLockDispatch) {
                getMutex(call).withLock {
                    response = doDispatch(talk, call)
                }
            } else if (shouldLockDispatch != null) {
                response = doDispatch(talk, call)
            }
            if (response.isPresent) {
                finalResponse = response
                break
            }
        }
        if (finalResponse.isEmpty) {
            finalResponse = buildFallbackResponse(call)
        } else if (finalResponse.get() === TalkResponse.EMPTY) {
            finalResponse = Optional.empty()
        }
        talksConfig.interactionsManager.handle(call)
        return finalResponse
    }

    private fun getMutex(request: TalkRequest): Mutex {
        return locks.get(request.chatId) { Mutex() }
    }

    private fun doDispatch(talk: Talk, call: TalkRequest): Optional<TalkResponse> {
        val response = talk.dispatch(call)
        if (response.isPresent) {
            saveAndFlushHandlers(talk, call.chatId, call.username, call.messageId, response.get())
        }
        return response
    }

    override fun saveAndFlushHandlers(talk: Talk, chatId: String, talkResponse: TalkResponse) {
        saveAndFlushHandlers(talk, chatId, null, null, talkResponse)
    }

    private fun saveAndFlushHandlers(
        talk: Talk,
        chatId: String,
        username: String?,
        messageId: String?,
        talkResponse: TalkResponse
    ) {
        talk.saveHandlers(chatId, username, messageId, talkResponse.getLastResponseInChain())
        talksConfig.talkManager.flushTalkContexts(chatId)
    }

    private fun buildFallbackResponse(call: TalkRequest): Optional<TalkResponse> {
        if (Usernames.isBot(call.username)) {
            return Optional.empty()
        } else if (LibSetting.getInteger(LibSetting.PROCESS_ECHO_MESSAGES) == 1) {
            return Optional.empty() //TODO find a better way so fallback responses are produced even if echo messages are processed
        }
        val fallbackResponse = getFallbackResponse().replace("\\n", "\n")
        if (fallbackResponse.isEmpty()) {
            logger.info("Returning an empty TalkResponse for Update [{}]", call.update.updateId())
            return Optional.empty()
        }
        return Optional.of(
            TalkResponse.ofText(
                null, call.chatId,
                fallbackResponse
            ).withPreserveHandlers()
        )
    }

    private fun updateCallFields(call: TalkRequest) {
        if (isJoinGroupMessage(call.message)) {
            call.setJoinGroup(true)
        }
        if (isLeaveGroupMessage(call.message)) {
            call.setLeaveGroup(true)
        }
    }

    private fun isJoinGroupMessage(message: Any): Boolean {
        return if (message is Message) {
            isTelegramJoinGroupMessage(message)
        } else {
            false
        }
    }

    private fun isTelegramJoinGroupMessage(message: Message?): Boolean {
        if (message?.newChatMembers() == null) {
            return false
        }
        val botName = getBotName()
        return Arrays.stream(message.newChatMembers())
            .anyMatch { user: User -> botName.equals(user.username(), ignoreCase = true) }
    }

    private fun isLeaveGroupMessage(message: Any): Boolean {
        return if (message is Message) {
            isTelegramLeaveGroupMessage(message)
        } else {
            false
        }
    }

    private fun isTelegramLeaveGroupMessage(message: Message?): Boolean {
        if (message?.leftChatMember() == null) {
            return false
        }
        val botName = getBotName()
        return botName.equals(message.leftChatMember().username(), ignoreCase = true)
    }

    private fun getTalks(): List<Talk> {
        return ArrayList(talks.keys)
    }

    // invocation assumes Talks enum elements are same name as their classes //TODO
    override fun talkForName(simpleClassName: String): Talk? {
        for (talk in talks.keys) {
            if (talk.javaClass.getSimpleName() == simpleClassName) {
                return talk
            }
        }
        return null
    }

    override fun <T : Talk> talksForClass(clazz: Class<T>): List<T> {
        return talks.keys.stream()
            .filter { talk: Talk -> clazz.isAssignableFrom(talk.javaClass) }
            .map { talk: Talk -> talk as T }
            .toList()
    }

    override fun followSession(session: String): String {
        return talks.keys.stream()
            .map { talk: Talk -> talk.getContext().follow(session) }
            .filter { obj: String? -> Objects.nonNull(obj) }
            .findFirst()
            .orElse(session)
    }

    override fun switchSessionInitiate(
        session: String,
        switchMainSession: Boolean,
        nextTalkResponse: TalkResponse
    ): String {
        val code = generateSwitchSessionCode()
        talks.keys
            .forEach(Consumer { talk: Talk ->
                talk.getContext().switchSessionInitiate(session, code, switchMainSession, nextTalkResponse)
            })
        return code
    }

    private fun generateSwitchSessionCode(): String {
        return talks.keys.stream().findFirst()
            .map { talk: Talk -> talk.getContext().generateSwitchSessionCode() }
            .orElseThrow { RuntimeException("No Talks defined") }
    }

    override fun switchSessionComplete(
        session: String,
        code: String,
        talkContext: TalkContext
    ): Optional<TalkResponse>? {
        val sessionIdsMatchingCode = talks.keys.stream()
            .map { obj: Talk -> obj.getContext() }
            .flatMap { context: TalkContext -> context.getSessionIdsMatchingSwitchSessionCode(code).stream() }
            .distinct()
            .toList()
        val originalTalkContextsBySessionIds = sessionIdsMatchingCode.stream()
            .flatMap { sessionId: String ->
                talks.keys.stream()
                    .map { talk: Talk ->
                        if (!talk.getContext().hasHandlersEntry(sessionId)) null else talk.getContext()
                    }
                    .filter { obj: TalkContext? -> Objects.nonNull(obj) }
                    .distinct()
                    .map { context: TalkContext? -> invalidateInvalidSwitchSessionCode(context, sessionId) }
                    .filter { obj: TalkContext? -> Objects.nonNull(obj) }
                    .map { context: TalkContext? -> Pair(sessionId, context) }
                    .toList().stream()
            }.distinct().toList()
        if (originalTalkContextsBySessionIds.isEmpty()) {
            return null // null signals invalid or expired switch session code
        }
        if (!talkContext.hasHandlersEntry(session)) {
            talkContext.createHandlersEntry(session)
        }
        val currentTalkContexts = talks.keys.stream()
            .map { talk: Talk -> if (talk.getContext().hasHandlersEntry(session)) talk else null }
            .filter { obj: Talk? -> Objects.nonNull(obj) }
            .collect(Collectors.toMap(
                { obj: Talk? -> obj!!.getType() },
                { obj: Talk? -> obj!!.getContext() }
            ))
        var nextTalkResponse: TalkResponse? = null
        for ((sessionId, context) in originalTalkContextsBySessionIds) {
            val talkResponse = doSwitchSessionComplete(session, sessionId, talkContext, context, currentTalkContexts)
            if (talkResponse != null) {
                nextTalkResponse = talkResponse
            }
        }
        return Optional.ofNullable(nextTalkResponse)
    }

    private fun invalidateInvalidSwitchSessionCode(context: TalkContext?, sessionId: String): TalkContext? {
        return if (!context!!.isSwitchSessionCodeValid(sessionId, LibSetting.getInteger(LibSetting.SWITCH_SESSION_TIMEOUT_IN_SECONDS))) {
            context.invalidateSwitchSessionCode(sessionId)
            null
        } else {
            context
        }
    }

    private fun doSwitchSessionComplete(
        currentSession: String,
        originalSession: String,
        currentTalkContext: TalkContext,
        originalTalkContext: TalkContext?,
        currentTalkContexts: Map<TalkType, TalkContext>
    ): TalkResponse? {
        val originalTalkType = originalTalkContext!!.getTalkType()
        val currentTalkContextOfOriginalType = currentTalkContexts[originalTalkType] ?: return null
        currentTalkContextOfOriginalType.copyVarsFromSession(currentSession, originalSession)
        //TODO check if vars of originalSession should be cleared
        val nextTalkResponse = originalTalkContext.getNextTalkResponse(originalSession)
        if (nextTalkResponse != null) {
            originalTalkContext.clearNextTalkResponse(originalSession)
            nextTalkResponse.chatId = currentSession
        }
        originalTalkContext.deleteCode(originalSession)
        if (originalTalkContext.isMainSession(originalSession)) {
            currentTalkContext.pointTo(currentSession, originalSession)
        } else {
            originalTalkContext.pointTo(originalSession, currentSession)
        }
        return nextTalkResponse
    }

    private fun getBotName(): String {
        return LibSetting.get(LibSetting.BOT_NAME)
    }

    private fun getFallbackResponse(): String {
        return LibSetting.get(LibSetting.FALLBACK_RESPONSE)
    }

    override fun shutDown() {
        logger.info("Stopping TalkManager....")
        cancelRemaining()
        talksConfig.getDao().shutdown()
        logger.info("TalkManager stopped.")
    }
}
