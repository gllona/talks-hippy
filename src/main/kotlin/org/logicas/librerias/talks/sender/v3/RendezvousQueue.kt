package org.logicas.librerias.talks.sender.v3

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.engine.TalkResponse
import org.logicas.librerias.talks.sender.PersistentMessage
import org.logicas.librerias.talks.sender.PersistentMessageRepository
import org.logicas.librerias.talks.sender.v3.RatedPersistentMessageSenderV3Facade.Companion.DEEP_LOG
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean
import java.util.stream.Collectors

class RendezvousQueue(
    val talksConfig: TalksConfiguration
) {
    var initProcessor: Job? = null
    val mutex = Mutex()
    val sendQueue = Channel<TalkResponseDbWrapper>(Channel.UNLIMITED)
    var lastNormalPriorityAcceptAt = ConcurrentHashMap<String, Long>()
    var lowPriorityProcessor: Job? = null
    val lowPriorityProcessorMutex = Mutex()
    val lowPriorityProcessorStarted = AtomicBoolean(false)
    val lowPriorityMessages = Channel<TalkResponse>(Channel.UNLIMITED)
    val messageRepository: PersistentMessageRepository = PersistentMessageRepository.getInstance(talksConfig.dao)

    companion object {
        val logger: Logger = LogManager.getLogger(RendezvousQueue::class.java)
        var instance: RendezvousQueue? = null

        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration): RendezvousQueue {
            if (instance == null) {
                instance = RendezvousQueue(talksConfig)
            }
            return instance!!
        }
    }

    init {
        queuePendingMessages()
    }

    private fun queuePendingMessages() {
        initProcessor = Coroutines.forceLaunchDefault(Coroutines.Group.MESSAGE_SENDER) {
            val pendingPersistentMessages = readSortedPendingMessages()
            for (persistentMessage in pendingPersistentMessages) {
                val response = persistentMessage.unencodedTalkResponse
                val message = buildMessage(response, persistentMessage.id)
                sendQueue.send(message)
                log("QUEUED MESSAGE DBID=" + persistentMessage.id + " (" + response.priority + ")")
            }
        }
    }

    private fun readSortedPendingMessages(): List<PersistentMessage> {
        return messageRepository.readPendingMessages().stream()
            .sorted(Comparator.comparing { obj -> obj.id })
            .collect(Collectors.toList())
    }

    suspend fun acceptNormalPriority(response: TalkResponse) {
        mutex.withLock {
            val now = System.currentTimeMillis()
            lastNormalPriorityAcceptAt[response.chatId] = now
            accept(response)
        }
    }

    @Suppress("EXPERIMENTAL_IS_NOT_ENABLED")
    @OptIn(ExperimentalCoroutinesApi::class)
    suspend fun acceptLowPriority(response: TalkResponse) {
        startLowPriorityProcessor()
        mutex.withLock {
            val now = System.currentTimeMillis()
            val lastAcceptAt = lastNormalPriorityAcceptAt[response.chatId] ?: (now - getChatInterMessageDelayInMillis())
            val elapsedSinceLastAccept = now - lastAcceptAt
            if (sendQueue.isEmpty && elapsedSinceLastAccept > getChatInterMessageDelayInMillis()) {
                accept(response)
            } else {
                lowPriorityMessages.send(response)
            }
        }
    }

    private suspend fun accept(response: TalkResponse) {
        for (finalResponse in response.finalResponses) {
            val persistentMessage = PersistentMessage()
            persistentMessage.unencodedTalkResponse = finalResponse
            messageRepository.save(persistentMessage)
            val message = buildMessage(finalResponse, persistentMessage.id)
            sendQueue.send(message)
        }
    }

    private fun buildMessage(response: TalkResponse, persistentMessageId: Long): TalkResponseDbWrapper {
        return TalkResponseDbWrapper(
            response,
            persistentMessageId
        )
    }

    suspend fun getOneMessage(): TalkResponseDbWrapper {
        return sendQueue.receive()
    }

    private suspend fun startLowPriorityProcessor() {
        if (lowPriorityProcessorStarted.get()) {
            return
        }

        lowPriorityProcessorMutex.withLock {
            if (! lowPriorityProcessorStarted.get()) {
                lowPriorityProcessor = Coroutines.launchDefault(Coroutines.Group.MESSAGE_SENDER) {
                    doStartLowPriorityProcessor()
                }
                lowPriorityProcessorStarted.set(true)
            }
        }
    }

    private suspend fun doStartLowPriorityProcessor() = coroutineScope {
        Coroutines.launchDefault(Coroutines.Group.MESSAGE_SENDER) {
            while (isActive) {
                delay(getChatInterMessageDelayInMillis() / 10)
                val response = lowPriorityMessages.receive()
                acceptLowPriority(response)
            }
        }
    }

    fun shutdown() {
        sendQueue.close()
        lowPriorityMessages.close()
        initProcessor?.cancel()
        lowPriorityProcessor?.cancel()
    }

    private fun log(s: String) {
        if (DEEP_LOG) {
            logger.info(s)
        }
    }

    private fun getChatInterMessageDelayInMillis(): Long {
        return LibSetting.getInteger(LibSetting.CHAT_INTER_MESSAGE_DELAY_IN_MILLIS).toLong()
    }
}
