package org.logicas.librerias.talks.sender.v3

import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.engine.TalkResponse

class LowPrioritySender(
    val talksConfig: TalksConfiguration
) {
    val rendezvousQueue = RendezvousQueue.getInstance(talksConfig)

    companion object {
        var instance: LowPrioritySender? = null

        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration): LowPrioritySender {
            if (instance == null) {
                instance = LowPrioritySender(talksConfig)
            }
            return instance!!
        }
    }

    suspend fun send(response: TalkResponse) {
        rendezvousQueue.acceptLowPriority(response)
    }

    fun shutdown() {
        // nothing to do
    }
}
