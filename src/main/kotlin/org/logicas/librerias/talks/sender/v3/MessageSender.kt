package org.logicas.librerias.talks.sender.v3

import com.google.common.cache.CacheBuilder
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.isActive
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.InputAdapters
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.engine.TalkResponse
import org.logicas.librerias.talks.matrix.MatrixResponse
import org.logicas.librerias.talks.matrix.MatrixSenderPerChat
import org.logicas.librerias.talks.media.MediaResourceService
import org.logicas.librerias.talks.sender.PersistentMessageRepository
import org.logicas.librerias.talks.telegram.TelegramSenderPerChat
import java.time.LocalDateTime
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

class MessageSender(
    val talksConfig: TalksConfiguration
) {
    var processor: Job? = null
    val startMutex = Mutex()
    val started = AtomicBoolean(false)
    val rendezvousQueue = RendezvousQueue.getInstance(talksConfig)
    val failedMessageQueue = Channel<TalkResponseDbWrapper>(Channel.UNLIMITED)
    val messagesSentInLastSecondCache = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.SECONDS).build<TalkResponseDbWrapper, TalkResponseDbWrapper>()
    val mediaResourceService = MediaResourceService.getInstance(talksConfig)
    val messageRepository: PersistentMessageRepository = PersistentMessageRepository.getInstance(talksConfig.dao)

    companion object {
        val logger = LogManager.getLogger(MessageSender::class.java)
        var instance: MessageSender? = null

        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration): MessageSender {
            if (instance == null) {
                instance = MessageSender(talksConfig)
            }
            return instance!!
        }
    }

    suspend fun start() {
        if (started.get()) {
            return
        }

        startMutex.withLock {
            if (! started.get()) {
                processor = Coroutines.launchDefault(Coroutines.Group.MESSAGE_SENDER) {
                    doStart()
                }
                started.set(true)
            }
        }
    }

    private suspend fun doStart() = coroutineScope {
        while (isActive) {
            val sent = sendFailedMessage()
            if (! sent) {
                val message = rendezvousQueue.getOneMessage()
                sendMessage(message)
            }
        }
    }

    private suspend fun sendMessage(message: TalkResponseDbWrapper) {
        val chatId = message.response.chatId

        when (message.response.type) {
            InputAdapters.TELEGRAM -> TelegramSenderPerChat.get(chatId.toLong(), talksConfig, this).sendMessage(message)
            InputAdapters.MATRIX -> MatrixSenderPerChat.get(chatId, talksConfig, this).sendMessage(message)
            InputAdapters.HEALTH -> {}
            null -> {}
        }
    }

    suspend fun enqueueFailedMessage(message: TalkResponseDbWrapper) {
        failedMessageQueue.send(message)
    }

    private suspend fun sendFailedMessage(): Boolean {
        val message = failedMessageQueue.tryReceive().getOrNull()
        return if (message != null) {
            sendMessage(message)
            true
        } else {
            false
        }
    }

    fun markMessageAsSent(message: TalkResponseDbWrapper, deleteImmediately: Boolean) {
        messagesSentInLastSecondCache.put(message, message)
        val persistentMessage = messageRepository.readById(message.sendQueueDbId)
        if (persistentMessage != null) {
            if (deleteImmediately) {
                messageRepository.delete(persistentMessage)
            } else {
                persistentMessage.sentAt = LocalDateTime.now()
                messageRepository.save(persistentMessage)
            }
        }
    }

    fun confirmSentMessagesReception(messageIdsAndMediaFileIds: List<Triple<Long, String, String?>>, inputAdapter: InputAdapters) {
        messageIdsAndMediaFileIds.forEach {
            confirmSentMessageReception(it, inputAdapter)
        }
    }

    fun confirmSentMessageReception(messageIdAndMediaFileId: Triple<Long, String, String?>, inputAdapter: InputAdapters) {
        val persistentMessageId = messageIdAndMediaFileId.first
        val channelMessageId = messageIdAndMediaFileId.second
        val mediaFileId = messageIdAndMediaFileId.third
        val message = messageRepository.readById(persistentMessageId)
        if (message != null) {
            messageRepository.delete(message)
            val talkResponse = message.unencodedTalkResponse
            TalkResponse.getInHeapResponse(talkResponse.uuid)?.setMessageId(channelMessageId)
            if (mediaFileId != null && talkResponse != null && talkResponse.mediaResourceId != null) {
                mediaResourceService.update(talkResponse.mediaResourceId, inputAdapter, mediaFileId)
            }
            handle(talkResponse, channelMessageId)
        }
    }

    fun setTalkResponseMessageId(response: TalkResponse, messageId: String) {
        Coroutines.forceLaunchDefault(Coroutines.Group.MESSAGE_SENDER, ephemeral = true) {
            response.messageIdGetterHolder.put(messageId)
        }
    }

    private fun handle(talkResponse: TalkResponse, messageId: String) {
        talksConfig.interactionsManager.handle(talkResponse, messageId)
    }

    fun sendCapacityFull(): Boolean {
        return messagesSentInLastSecondCache.size() >= getAllChatsMaxMessagesPerSecond()
    }

    fun shutdown() {
        failedMessageQueue.close()
        processor?.cancel()
        TelegramSenderPerChat.shutdownAll()
        MatrixSenderPerChat.shutdownAll()
    }

    private fun getAllChatsMaxMessagesPerSecond(): Int {
        return LibSetting.getInteger(LibSetting.ALL_CHATS_MAX_MESSAGES_PER_SECOND) - 1
    }

    fun getMatrixMessages(): List<MatrixResponse.Message> {
        val messages = messageRepository.readMatrixMessages()
            .map { TalkResponseDbWrapper(it.unencodedTalkResponse, it.id) }

        val resourceFileIdSuppliers = messages
            .filter { it.response.mediaResourceId != null }
            .map {
                mediaResourceService.createIfNotExists(it.response.mediaResourceId, InputAdapters.MATRIX)
                it
            }
            .associateBy(
                { it.sendQueueDbId },
                { { mediaResourceService.findFileId(it.response.mediaResourceId, InputAdapters.MATRIX) } }
            )

        val matrixMessages = messages
            .mapNotNull { message: TalkResponseDbWrapper -> message.response.getMatrixMessage(
                talksConfig,
                message.sendQueueDbId,
                if (message.response.mediaResourceId == null) null else resourceFileIdSuppliers[message.sendQueueDbId]
            ) }
            .sortedBy { o: MatrixResponse.Message -> o.id }

        return matrixMessages
    }
}
