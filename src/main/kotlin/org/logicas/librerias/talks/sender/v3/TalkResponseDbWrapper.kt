package org.logicas.librerias.talks.sender.v3

import org.logicas.librerias.talks.engine.TalkResponse

class TalkResponseDbWrapper(
    val response: TalkResponse,
    val sendQueueDbId: Long
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TalkResponseDbWrapper

        if (sendQueueDbId != other.sendQueueDbId) return false

        return true
    }

    override fun hashCode(): Int {
        return sendQueueDbId.hashCode()
    }
}
