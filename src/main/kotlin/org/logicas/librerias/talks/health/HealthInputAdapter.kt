package org.logicas.librerias.talks.health

import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.InputAdapterApi
import org.logicas.librerias.talks.api.ModelApi
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.api.TracerApi
import org.logicas.librerias.talks.engine.TalkRequest
import org.logicas.librerias.talks.engine.TalkResponse
import java.io.File
import java.util.concurrent.CountDownLatch

class HealthInputAdapter(
    private val talksConfig: TalksConfiguration
) : InputAdapterApi {

    companion object {
        private val logger = LogManager.getLogger(HealthInputAdapter::class.java)
    }

    private var model: ModelApi? = null
    private val tracer: TracerApi = talksConfig.tracer

    private var lastResponse: TalkResponse? = null

    override fun getTalksConfiguration(): TalksConfiguration {
        return talksConfig
    }

    override suspend fun dispatch(model: ModelApi) {
        this.model = model
    }

    override fun receiveRequest(request: TalkRequest) {
        val latch = CountDownLatch(1)
        Coroutines.forceLaunchDefault(Coroutines.Group.UTILS, ephemeral = true) {
            doReceiveRequest(request)
            latch.countDown()
        }
        latch.await()
    }

    private suspend fun doReceiveRequest(request: TalkRequest) {
        assertModelIsSet()
        val matrixRequest = request.matrixRequest
        lastResponse = null

        try {
            val response = model!!.receiveMatrixMessage(
                this,
                matrixRequest
            )
            if (response.isPresent) {
                lastResponse = response.get()
            }
        } catch (e: Exception) {
            throw HealthException(e)
        }
    }

    fun getLastResponse(): TalkResponse? {
        return lastResponse
    }

    private fun assertModelIsSet() {
        if (model == null) {
            throw RuntimeException("Telegram Adapter: Model not set")
        }
    }

    override fun sendResponse(response: TalkResponse) {
        // do nothing
    }

    override fun getFile(fileId: String): File {
        throw UnsupportedOperationException("Can not get a File object in Matrix input adapter.")
    }

    override suspend fun shutdown() {
        // nothing to do
    }
}
