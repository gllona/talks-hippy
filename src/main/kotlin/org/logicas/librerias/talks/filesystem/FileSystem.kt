package org.logicas.librerias.talks.filesystem

import org.logicas.librerias.talks.api.FileSystemApi
import org.logicas.librerias.talks.api.TalksConfiguration
import java.io.File
import java.util.concurrent.TimeUnit

class FileSystem(private val talksConfig: TalksConfiguration) : FileSystemApi {

    companion object {
        private var instance: FileSystemApi? = null
        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration): FileSystemApi? {
            if (instance == null) {
                instance = FileSystem(talksConfig)
            }
            return instance
        }
    }

    override fun deleteAfter(file: File?, count: Int, timeUnit: TimeUnit) {
        if (file == null || ! file.exists()) {
            return
        }

        talksConfig.fileSystemDeleteQueue.deleteAfter(file, count, timeUnit, false)
    }

    override fun deleteDownloadedAfter(file: File?, count: Int, timeUnit: TimeUnit) {
        if (file == null || ! file.exists()) {
            return
        }

        talksConfig.fileSystemDeleteQueue.deleteAfter(file, count, timeUnit, true)
    }
}
