package org.logicas.librerias.talks.telegram

import com.pengrad.telegrambot.BotUtils
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.model.Update
import org.logicas.librerias.talks.Coroutines
import spark.Request
import spark.Response
import spark.Route

abstract class BotHandler : Route {

    @Throws(Exception::class)
    override fun handle(request: Request, response: Response): Any {
        val update = BotUtils.parseUpdate(request.body())
        val message = update.message()
        if (isStartMessage(message) && onStart(message)) {
            return "ok"
        } else {
            Coroutines.forceLaunchDefault(Coroutines.Group.TELEGRAM_INPUT_ADAPTER, ephemeral = true) {
                onWebhookUpdate(update)
            }
        }
        return "ok"
    }

    private fun isStartMessage(message: Message?): Boolean {
        return message?.text() != null && message.text().startsWith("/start")
    }

    private fun onStart(message: Message?): Boolean {
        return false
    }

    abstract fun onWebhookUpdate(update: Update)

    abstract fun getToken(): String

    abstract fun getBot(): TelegramBot
}
