package org.logicas.librerias.talks.telegram

import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.request.BaseRequest
import com.pengrad.telegrambot.response.SendResponse
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.config.InputAdapters
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.engine.MediaType
import org.logicas.librerias.talks.media.MediaResourceService
import org.logicas.librerias.talks.sender.v3.MessageSender
import org.logicas.librerias.talks.sender.v3.TalkResponseDbWrapper
import java.io.FileNotFoundException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.math.max

class TelegramSenderPerChat(
    val chatId: Long,
    val talksConfig: TalksConfiguration,
    val messageSender: MessageSender,
    val mediaResourceService: MediaResourceService = MediaResourceService.getInstance(talksConfig)
) {
    var processor: Job? = null
    val startMutex = Mutex()
    val started = AtomicBoolean(false)
    val sendQueue = Channel<TalkResponseDbWrapper>(Channel.UNLIMITED)
    val tracer = talksConfig.tracer

    companion object {
        var senders = ConcurrentHashMap<Long, TelegramSenderPerChat>()
        val logger = LogManager.getLogger(TelegramSenderPerChat::class.java)

        fun get(
            chatId: Long,
            talksConfig: TalksConfiguration,
            messageSender: MessageSender
        ): TelegramSenderPerChat {
            return senders.getOrPut(chatId) { TelegramSenderPerChat(chatId, talksConfig, messageSender) }
        }

        fun shutdownAll() {
            senders.values.forEach { it.shutdown() }
        }
    }

    suspend fun start() {
        if (started.get()) {
            return
        }

        startMutex.withLock {
            if (! started.get()) {
                processor = Coroutines.launchIO(Coroutines.Group.TELEGRAM_MESSAGE_SENDER) {
                    doStart()
                }
                started.set(true)
            }
        }
    }

    private suspend fun doStart() = coroutineScope {
        var lastSendTimestamp = System.currentTimeMillis() - getInterMessageDelay()

        while (isActive) {
            delayForThisChat(lastSendTimestamp)
            if (isActive) {
                val message = sendQueue.receive()
                delayForAllChats()
                lastSendTimestamp = System.currentTimeMillis()
                doSendMessage(message)
            }
        }
    }

    private suspend fun delayForThisChat(lastSendTimestamp: Long) {
        val now = System.currentTimeMillis()
        val millisSinceLastSend = now - lastSendTimestamp
        val toDelay = max(0, getInterMessageDelay() - millisSinceLastSend)
        if (toDelay > 0) {
            delay((getInterMessageDelay() * 1.01).toLong()) // add 1% to play safe
        }
    }

    private suspend fun delayForAllChats() = coroutineScope {
        while (isActive && messageSender.sendCapacityFull()) {
            delay(getInterMessageDelay() / 5) // 200 ms
        }
    }

    suspend fun sendMessage(message: TalkResponseDbWrapper) {
        start()
        sendQueue.send(message)
    }

    private suspend fun doSendMessage(message: TalkResponseDbWrapper) = coroutineScope {
        Coroutines.launchIO(Coroutines.Group.TELEGRAM_MESSAGE_SENDER, ephemeral = true) {
            try {
                val telegramRequest = buildTelegramRequest(message)
                if (telegramRequest == null) {
                    tracer.error(
                        message.response.chatId,
                        null,
                        null,
                        "Exception evaluating building a Telegram request"
                    )
                    messageSender.markMessageAsSent(message, true)
                } else {
                    val telegramResponse = talksConfig.telegramBot.execute(telegramRequest)
                    if (telegramResponse is SendResponse) {
                        val telegramMessageId = telegramResponse.message()?.messageId()
                        if (telegramMessageId != null) {
                            messageSender.confirmSentMessageReception(
                                Triple(
                                    message.sendQueueDbId,
                                    telegramMessageId.toString(),
                                    getFileId(telegramResponse.message())
                                ),
                                InputAdapters.TELEGRAM
                            )
                        }
                    }
                    messageSender.markMessageAsSent(message, false)
                }
            } catch (e: Exception) {
                when (e.cause) {
                    is FileNotFoundException -> {
                        messageSender.markMessageAsSent(message, true) // do not retry the send operation
                        logger.error("Exception trying sending message to Telegram because file was not found.", e)
                    }
                    else -> {
                        messageSender.enqueueFailedMessage(message)
                        logger.error("Exception trying sending message to Telegram, will retry...", e)
                    }
                }
            }
        }
    }

    private fun buildTelegramRequest(message: TalkResponseDbWrapper): BaseRequest<*, *>? {
        val response = message.response
        return when (response.mediaType) {
            MediaType.ACTION_DELETE_MESSAGE -> response.telegramDeleteMessageRequest
            else -> {
                if (response.mediaResourceId == null)
                    response.telegramSendRequest
                else {
                    mediaResourceService.createIfNotExists(response.mediaResourceId, InputAdapters.TELEGRAM)
                    response.getTelegramSendRequest { mediaResourceService.findFileId(response.mediaResourceId, InputAdapters.TELEGRAM) }
                }
            }
        }
    }

    private fun getFileId(message: Message): String? {
        return listOfNotNull(
            message.animation()?.fileId(),
            message.audio()?.fileId(),
            message.voice()?.fileId(),
            if (message.photo() == null) null else message.photo()[message.photo().size - 1]?.fileId(),
            message.video()?.fileId(),
            message.videoNote()?.fileId(),
            message.document()?.fileId()
        ).firstOrNull()
    }

    fun shutdown() {
        sendQueue.close()
        processor?.cancel()
    }

    private fun getInterMessageDelay(): Long {
        return LibSetting.getInteger(LibSetting.CHAT_INTER_MESSAGE_DELAY_IN_MILLIS).toLong()
    }
}
