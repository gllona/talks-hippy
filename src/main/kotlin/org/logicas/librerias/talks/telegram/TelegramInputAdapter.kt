package org.logicas.librerias.talks.telegram

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.TelegramException
import com.pengrad.telegrambot.UpdatesListener
import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.model.Update
import com.pengrad.telegrambot.request.GetFile
import com.pengrad.telegrambot.request.GetUpdates
import org.apache.logging.log4j.LogManager
import org.logicas.librerias.talks.Coroutines
import org.logicas.librerias.talks.api.InputAdapterApi
import org.logicas.librerias.talks.api.ModelApi
import org.logicas.librerias.talks.api.TalksConfiguration
import org.logicas.librerias.talks.api.TracerApi
import org.logicas.librerias.talks.config.LibSetting
import org.logicas.librerias.talks.engine.MediaType
import org.logicas.librerias.talks.engine.TalkRequest
import org.logicas.librerias.talks.engine.TalkResponse
import org.logicas.librerias.talks.sender.v3.MessageSender
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.nio.channels.Channels
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.concurrent.CountDownLatch

class TelegramInputAdapter(
    private val talksConfig: TalksConfiguration
) : BotHandler(), InputAdapterApi {

    companion object {
        private val logger = LogManager.getLogger(TelegramInputAdapter::class.java)
    }

    private val token: String = getTelegramApiKey()
    private val bot: TelegramBot = TelegramBot.Builder(token).build() // TelegramBot.Builder(token).debug().build();
    private var model: ModelApi? = null
    private val messageSender: MessageSender = MessageSender.getInstance(talksConfig)
    private val tracer: TracerApi = talksConfig.tracer

    init {
        talksConfig.telegramBot = bot
        talksConfig.messageSender // start sender
        talksConfig.interactionsManager // start interactions manager
    }

    override fun getTalksConfiguration(): TalksConfiguration {
        return talksConfig
    }

    override fun getToken(): String {
        return token
    }

    override fun getBot(): TelegramBot {
        return bot
    }

    override fun receiveRequest(request: TalkRequest) {
        onWebhookUpdate(request.update)
    }

    override fun onWebhookUpdate(update: Update) {
        Coroutines.forceLaunchDefault(Coroutines.Group.TELEGRAM_INPUT_ADAPTER, ephemeral = true) {
            doOnWebhookUpdate(update)
        }
    }

    private suspend fun doOnWebhookUpdate(update: Update) {
        assertModelIsSet()
        val chatId = getChatId(update)
        val username = getUsername(update)
        val messageId = getMessageId(update)
        try {
            tracer.info(
                chatId,
                username,
                messageId,
                String.format("Entered onWebhookUpdate from Telegram for Update with text [%s]",
                    Optional.ofNullable(getMessage(update))
                        .map { obj: Message -> obj.text() }
                        .map { text: String? -> text?.replace("\n", "\\n") }
                        .orElse(null)
                ))
            val response = model!!.receiveTelegramMessage(
                this,
                update
            )
            if (response.isPresent) {
                sendMessage(response.get(), username, messageId)
            }
        } catch (e: Exception) {
            tracer.error(chatId, username, messageId, "Exception when handling Update", e)
        } finally {
            tracer.info(chatId, username, messageId, "To exit onWebhookUpdate for Update")
        }
    }

    private fun sendMessage(
        response: TalkResponse,
        username: String?,
        messageId: String?
    ) {
        var nextResponse : TalkResponse? = response
        do {
            val chatId = getChatId(nextResponse)
            try {
                talksConfig.messageSender.send(nextResponse)

            } catch (e: Exception) {
                tracer.error(chatId, username, messageId, "Exception trying to generate response for Update", e)
                val exceptionalResponse = nextResponse?.exceptionMapper?.apply(e)
                if (exceptionalResponse != null) {
                    try {
                        talksConfig.messageSender.send(exceptionalResponse)
                    } catch (e2: Exception) {
                        tracer.error(chatId, username, messageId, "Exception trying to generate exceptional response for Update", e2)
                    }
                }
            }
            tracer.info(
                chatId,
                username,
                messageId,
                String.format(
                    "Answered to Telegram Update with [%s]",
                    if (nextResponse?.mediaType != MediaType.TEXT) nextResponse?.mediaType
                    else nextResponse.fastText?.replace("\n", "\\n")
                )
            )
            nextResponse = nextResponse?.nextResponse
        } while (nextResponse != null)
    }

    private fun getChatId(response: TalkResponse?): String? {
        return response?.chatId
    }

    private fun getChatId(update: Update): String? {
        val message = getMessage(update)
        return message?.chat()?.id()?.toString()
    }

    private fun getUsername(update: Update): String? {
        val message = getMessage(update)
        val username = message?.chat()?.username()
        return if (username == null) null else "@$username"
    }

    private fun getMessageId(update: Update): String? {
        val message = getMessage(update)
        return message?.messageId()?.toString()
    }

    private fun getMessage(update: Update): Message? {
        if (update.message() != null) {
            return update.message()
        } else if (update.editedMessage() != null) {
            return update.editedMessage()
        } else if (update.channelPost() != null) {
            return update.channelPost()
        } else if (update.editedChannelPost() != null) {
            return update.editedChannelPost()
        }
        return null
    }

    private fun assertModelIsSet() {
        if (model == null) {
            throw RuntimeException("Telegram Adapter: Model not set")
        }
    }

    // suspend not needed but stays here for future implementation not based on Coroutines.forceLaunchDefault
    override suspend fun dispatch(model: ModelApi) {
        this.model = model
        setupBot()
    }

    private fun setupBot() {
        bot.setUpdatesListener(
            ::updatesListener,
            ::exceptionHandler,
            buildGetUpdatesRequest()
        )
    }

    private fun exceptionHandler(telegramException: TelegramException) {
        tracer.error("-1", null, null, "Exception in Telegram updates listener", telegramException)
    }

    private fun buildGetUpdatesRequest(): GetUpdates {
        val getUpdates = GetUpdates()
        getUpdates.limit(getTelegramGetUpdatesLimit())
        return getUpdates
    }

    private fun updatesListener(updates: List<Update>): Int {
        model!!.beforeReceiveMessages(this, updates)

        val latch = CountDownLatch(updates.size)
        updates.forEach {
            Coroutines.forceLaunchDefault(Coroutines.Group.TELEGRAM_INPUT_ADAPTER, ephemeral = true) {
                onWebhookUpdate(it)
                latch.countDown()
            }
        }
        latch.await()

        model!!.afterReceiveMessages(this, updates)

        return UpdatesListener.CONFIRMED_UPDATES_ALL
    }

    override fun sendResponse(response: TalkResponse) {
        sendMessage(response, null, null)
    }

    override fun getFile(fileId: String): File {
        return try {
            // telegram
            val request = GetFile(fileId)
            val response = bot.execute(request)
            val originalFile = response.file()
            val originalFilePathStr = originalFile.filePath()
            val originalFilePath = if (originalFilePathStr == null) null else Paths.get(originalFilePathStr)
            val originalFileName = originalFilePath?.fileName?.toString()
            val link = bot.getFullFilePath(originalFile)
            // filesystem
            val baseDirectory = getFileDownloadPath()
            val subdirectory = UUID.randomUUID().toString()
            val directoryPath = Paths.get(baseDirectory, subdirectory)
            Files.createDirectories(directoryPath)
            val targetFileName = originalFileName?.replace("/", "-")?.replace("\\", "-") ?: originalFile.fileUniqueId()
            val targetFilePathStr = Paths.get(baseDirectory, subdirectory, targetFileName).toString()
            // download
            download(link, targetFilePathStr)
        } catch (e: IOException) {
            logger.error(String.format("Unable to download file [fileId=%s]", fileId))
            throw e
        }
    }

    private fun download(url: String, filename: String): File {
        return try {
            val `in` = URL(url).openStream()
            val readableByteChannel = Channels.newChannel(`in`)
            val fileOutputStream = FileOutputStream(filename)
            val fileChannel = fileOutputStream.channel
            fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE)
            File(filename)
        } catch (e: IOException) {
            logger.error(String.format("Error downloading [%s] to [%s]", url, filename), e)
            throw e
        }
    }

    override suspend fun shutdown() {
        bot.removeGetUpdatesListener()
        Coroutines.shutdown(Coroutines.Group.TELEGRAM_INPUT_ADAPTER)
    }

    private fun getFileDownloadPath(): String {
        return LibSetting.get(LibSetting.FILE_DOWNLOAD_PATH)
    }

    private fun getTelegramApiKey(): String {
        return LibSetting.get(LibSetting.TELEGRAM_API_KEY)
    }

    private fun getTelegramGetUpdatesLimit(): Int {
        return LibSetting.getInteger(LibSetting.TELEGRAM_GET_UPDATES_LIMIT)
    }
}
