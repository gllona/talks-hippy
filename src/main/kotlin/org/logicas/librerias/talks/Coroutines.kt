package org.logicas.librerias.talks

import kotlinx.coroutines.*
import org.logicas.librerias.talks.config.LibSetting
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors

object Coroutines {

    enum class Group {
        TELEGRAM_INPUT_ADAPTER,
        MATRIX_INPUT_ADAPTER,
        MESSAGE_SENDER,
        TELEGRAM_MESSAGE_SENDER,
        MATRIX_MESSAGE_SENDER,
        INTERACTIONS_MANAGER,
        UTILS,
        APP
    }

    private val jobs = ConcurrentHashMap<Group, ConcurrentHashMap<Job, Int>>()

    val useSingleThread = shouldUseSingleThread()
    val singleThreadDispatcher = if (useSingleThread) Executors.newSingleThreadExecutor().asCoroutineDispatcher() else null
    val defaultDispatcher = if (useSingleThread) singleThreadDispatcher!! else Dispatchers.Default
    val ioDispatcher = if (useSingleThread) singleThreadDispatcher!! else Dispatchers.IO

    @OptIn(DelicateCoroutinesApi::class)
    val globalScope = GlobalScope

    private suspend fun launch(
        context: CoroutineDispatcher,
        group: Group,
        ephemeral: Boolean = false,
        block: suspend CoroutineScope.() -> Unit
    ): Job = coroutineScope {
        //val job = launch(context) {   // this causes coroutine blocking with channels usage
        val job = globalScope.launch(context) {   // this should not be used
            block()
        }
        if (! ephemeral) {
            register(group, job)
        }
        job
    }

    suspend fun launchDefault(
        group: Group,
        ephemeral: Boolean = false,
        block: suspend CoroutineScope.() -> Unit
    ): Job {
        return launch(defaultDispatcher, group, ephemeral, block)
    }

    suspend fun launchIO(
        group: Group,
        ephemeral: Boolean = false,
        block: suspend CoroutineScope.() -> Unit
    ): Job {
        return launch(ioDispatcher, group, ephemeral, block)
    }

    private fun forceLaunch(
        context: CoroutineDispatcher,
        group: Group,
        ephemeral: Boolean = false,
        block: suspend CoroutineScope.() -> Unit
    ): Job {
        val job = globalScope.launch(context) {
            block()
        }
        if (! ephemeral) {
            register(group, job)
        }
        return job
    }

    fun forceLaunchDefault(
        group: Group,
        ephemeral: Boolean = false,
        block: suspend CoroutineScope.() -> Unit
    ): Job {
        return forceLaunch(defaultDispatcher, group, ephemeral, block)
    }

    fun forceLaunchIO(
        group: Group,
        ephemeral: Boolean = false,
        block: suspend CoroutineScope.() -> Unit
    ): Job {
        return forceLaunch(ioDispatcher, group, ephemeral, block)
    }

    suspend fun join(job: Job) = coroutineScope {
        job.join()
    }

    suspend fun joinAll(jobs: Collection<Job>) = coroutineScope {
        jobs.joinAll()
    }

    private fun register(group: Group, job: Job) {
        val map = getMap(group)
        map[job] = 14
    }

    suspend fun terminate(group: Group, job: Job) = coroutineScope {
        val map = getMap(group)
        if (map.containsKey(job)) {
            job.cancelAndJoin()
            map.remove(job)
        }
    }

    suspend fun shutdown(group: Group) {
        val map = getMap(group)
        val jobs = map.keys().toList()
        jobs.forEach { it.cancel() }
        jobs.joinAll()
        map.clear()
    }

    @JvmStatic
    fun cancel(group: Group) {
        val map = getMap(group)
        val jobs = map.keys().toList()
        jobs.forEach { it.cancel() }
    }

    @JvmStatic
    fun cancelRemaining() {
        jobs.forEach { map ->
            map.value.forEach {
                val job = it.key
                job.cancel()
            }
        }
    }

    private fun getMap(group: Group): ConcurrentHashMap<Job, Int> {
        if (! jobs.containsKey(group)) {
            jobs[group] = ConcurrentHashMap()
        }
        return jobs[group]!!
    }

    private fun shouldUseSingleThread(): Boolean {
        return LibSetting.getInteger(LibSetting.USE_SINGLE_THREAD) != 0
    }
}
